package com.example.leuks.roulette.commons.entities;

import java.util.ArrayList;

/**
 * Created by Leuks on 10/12/2016.
 */

public class InfiniteTuple<S> {
    private ArrayList<S> data;

    public InfiniteTuple() {
        this.data = new ArrayList<>();
    }

    public InfiniteTuple(S... arguments) {
        this.data = new ArrayList<>();
        for (S item : arguments) {
            add(item);
        }
    }

    public void add(S item) {
        this.data.add(item);
    }

    public void addLast(S item) {
        data.add(0, item);
        data.remove(data.size() - 1);
    }

    public int size() {
        return this.data.size();
    }

    public S get(int index) {
        return this.data.get(index);
    }

    public boolean isEmpty() {
        if (this.data.isEmpty()) return true;
        else return false;
    }

    public void removeAll() {
        data.clear();
    }

    public String toString(boolean reverse) {
        StringBuilder sb = new StringBuilder();
        int c = 0;
        if (reverse) {
            for (int i = this.data.size() - 1; i >= 0; i--) {
                sb.append(this.data.get(i));
                c++;
                if (c == 3) break;
                sb.append(" - ");
            }
        } else {
            int min = (this.data.size() - 4);
            if (min < 0) min = 0;
            for (int i = min; i < this.data.size(); i++) {
                sb.append(this.data.get(i));
                c++;
                if (c == 3) break;
                sb.append(" - ");
            }
        }
        return sb.toString();
    }
}
