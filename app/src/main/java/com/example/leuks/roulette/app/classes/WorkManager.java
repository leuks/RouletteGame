package com.example.leuks.roulette.app.classes;

import android.app.ProgressDialog;
import android.support.design.widget.Snackbar;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.commons.bdd.Parameters;
import com.example.leuks.roulette.commons.entities.Tools;
import com.example.leuks.roulette.commons.entities.Tuple;

import java.util.ArrayList;

/**
 * Created by leuks on 02/01/2017.
 */

public class WorkManager {
    private static int ID = 0;
    private MainActivity mainActivity;
    private int max;
    private int count;
    private int lastcount;
    private int littleCount;
    private ArrayList<Tuple<Integer, Integer>> testList;
    private Parameters parameters;
    private int id;

    public WorkManager(MainActivity mainActivity, int max, ArrayList<Tuple<Integer, Integer>> testList) {
        this.mainActivity = mainActivity;
        this.max = max;
        this.testList = testList;
        this.parameters = mainActivity.getHelper().getParameters();
        id = ID++;
    }

    public void start() {
        ProgressDialog progressDialog = mainActivity.getProgressDialog();
        if (!(testList == null || testList.isEmpty()))
            progressDialog.setTitle("Remise à 0 en cours");
        else progressDialog.setTitle("Génération en cours");
        progressDialog.show();
        count = 0;
        littleCount = 0;
        lastcount = Integer.MAX_VALUE;
        startWork();
        mainActivity.getWork(this).init();
    }

    public synchronized void addWorkResult() {
        ProgressDialog progressDialog = mainActivity.getProgressDialog();
        count++;

        if (count == max) {
            Snackbar snackbar1 = Snackbar.make(mainActivity.findViewById(android.R.id.content), "Génération terminée", Snackbar.LENGTH_LONG);
            snackbar1.show();
            progressDialog.dismiss();
            mainActivity.getEntryFragment().onResume();
            mainActivity.reset();
            return;
        }

        if (lastcount != count) {
            mainActivity.reset();
            startWork();
        }
    }

    private void startWork() {
        ProgressDialog progressDialog = mainActivity.getProgressDialog();
        progressDialog.setMessage((count + 1) + "/" + max);
        lastcount = count;
        Work work = mainActivity.getWork(this);
        if (testList == null || testList.isEmpty()) {
            work.start(Tools.randomIntWithInclusiveBounds(0, 36), null);
        } else {
            if (count >= testList.size()) {
                work.start(Tools.randomIntWithInclusiveBounds(0, 36), null);
            } else {
                Tuple<Integer, Integer> actualTest = testList.get(count);
                int shot = actualTest.getArg1();
                int simpleChance = actualTest.getArg2();
                work.start(shot, simpleChance);
            }
        }
    }

    public boolean equals(WorkManager wm) {
        if (wm == null) return false;
        return id == wm.id;
    }

    //GS
    public int getId() {
        return id;
    }
}
