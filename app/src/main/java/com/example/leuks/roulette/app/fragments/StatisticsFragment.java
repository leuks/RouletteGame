package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.app.classes.DataWork;
import com.example.leuks.roulette.commons.bdd.BestTargetItem;
import com.example.leuks.roulette.commons.bdd.ScoreOneItem;
import com.example.leuks.roulette.commons.bdd.ScoreTwoItem;
import com.example.leuks.roulette.commons.bdd.ValorisationItem;
import com.example.leuks.roulette.commons.entities.Tuple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

/**
 * Created by leuks on 30/12/2016.
 */

public class StatisticsFragment extends Fragment {
    private MainActivity mainActivity;
    private LinearLayout valorisationLayout;
    private LinearLayout scoreOneScrollLayout;
    private LinearLayout scoreTwoScrollLayout;
    private LinearLayout targetingScrollLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.fragment_statistics_layout, container, false);

        //components
        TextView targetingTextViewShow = (TextView) thisView.findViewById(R.id.textView_targeting_show);
        TextView scoreOneTextViewShow = (TextView) thisView.findViewById(R.id.textView_score_one_show);
        TextView scoreTwoTextViewShow = (TextView) thisView.findViewById(R.id.textView_score_two_show);
        TextView valorisationTextViewShow = (TextView) thisView.findViewById(R.id.textView_valorisation_show);
        targetingScrollLayout = (LinearLayout) thisView.findViewById(R.id.scroll_targeting_layout);
        scoreOneScrollLayout = (LinearLayout) thisView.findViewById(R.id.scroll_score_one_layout);
        scoreTwoScrollLayout = (LinearLayout) thisView.findViewById(R.id.scroll_score_two_layout);
        valorisationLayout = (LinearLayout) thisView.findViewById(R.id.layout_valorisation);

        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        targetingTextViewShow.setTypeface(fontButton);
        scoreOneTextViewShow.setTypeface(fontButton);
        scoreTwoTextViewShow.setTypeface(fontButton);
        valorisationTextViewShow.setTypeface(fontButton);

        return thisView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DataWork dataWork = mainActivity.getDataWork(false);
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");

        //targeting
        HashMap<Integer, ArrayList<BestTargetItem>> lastExitOccurence = dataWork.getStatItems();
        //50items
        for (int key : lastExitOccurence.keySet()) {
            if (lastExitOccurence.get(key).size() < 50)
                lastExitOccurence.put(key, new ArrayList<BestTargetItem>());
            else {
                for (int i = 0; i < 50; i++) {
                    lastExitOccurence.get(key).remove(0);
                }
            }
        }
        HashMap<Integer, Tuple<Integer, Integer>> countAllElementsTargeting = new HashMap<>();
        for (int i : new TreeSet<>(lastExitOccurence.keySet())) {
            ArrayList<BestTargetItem> bestTargetItemsOfOccurence = lastExitOccurence.get(i);

            TableLayout tableLayout = new TableLayout(mainActivity);
            TableRow header = new TableRow(mainActivity);
            TextView occurenceNumberTextViewShow = new TextView(mainActivity);

            header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));
            TextView headerDnb = new TextView(mainActivity);
            headerDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            headerDnb.setText("Dénombrement");
            headerDnb.setGravity(Gravity.CENTER);
            headerDnb.setPadding(10, 10, 10, 10);

            TextView headerFreqTargetChoiceTextView = new TextView(mainActivity);
            headerFreqTargetChoiceTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            headerFreqTargetChoiceTextView.setText("Fréq Cible");
            headerFreqTargetChoiceTextView.setGravity(Gravity.CENTER);
            headerFreqTargetChoiceTextView.setPadding(10, 10, 10, 10);

            TextView headerFreqSuccessTextView = new TextView(mainActivity);
            headerFreqSuccessTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            headerFreqSuccessTextView.setText("Freq Succès");
            headerFreqSuccessTextView.setGravity(Gravity.CENTER);
            headerFreqSuccessTextView.setPadding(10, 10, 10, 10);

            header.addView(headerDnb);
            header.addView(headerFreqTargetChoiceTextView);
            header.addView(headerFreqSuccessTextView);

            occurenceNumberTextViewShow.setTypeface(fontButton);
            occurenceNumberTextViewShow.setText("Occurence " + (i + 1));
            occurenceNumberTextViewShow.setGravity(Gravity.CENTER);

            tableLayout.addView(header);


            HashMap<Integer, Tuple<Integer, Integer>> countElements = new HashMap<>();
            for (BestTargetItem bestTargetItem : bestTargetItemsOfOccurence) {
                if (countElements.get(bestTargetItem.getDenombrement()) == null) {
                    countElements.put(bestTargetItem.getDenombrement(), new Tuple(1, 0));

                    if (bestTargetItem.isSuccess()) {
                        int oldCountSuccess = countElements.get(bestTargetItem.getDenombrement()).getArg2();
                        countElements.get(bestTargetItem.getDenombrement()).setArg2(oldCountSuccess + 1);
                    }
                } else {
                    int oldCount = countElements.get(bestTargetItem.getDenombrement()).getArg1();
                    countElements.get(bestTargetItem.getDenombrement()).setArg1(oldCount + 1);

                    if (bestTargetItem.isSuccess()) {
                        int oldCountSuccess = countElements.get(bestTargetItem.getDenombrement()).getArg2();
                        countElements.get(bestTargetItem.getDenombrement()).setArg2(oldCountSuccess + 1);
                    }
                }

                if (countAllElementsTargeting.get(bestTargetItem.getDenombrement()) == null) {
                    countAllElementsTargeting.put(bestTargetItem.getDenombrement(), new Tuple(1, 0));

                    if (bestTargetItem.isSuccess()) {
                        int oldCountSuccess = countAllElementsTargeting.get(bestTargetItem.getDenombrement()).getArg2();
                        countAllElementsTargeting.get(bestTargetItem.getDenombrement()).setArg2(oldCountSuccess + 1);
                    }
                } else {
                    int oldCount = countAllElementsTargeting.get(bestTargetItem.getDenombrement()).getArg1();
                    countAllElementsTargeting.get(bestTargetItem.getDenombrement()).setArg1(oldCount + 1);

                    if (bestTargetItem.isSuccess()) {
                        int oldCountSuccess = countAllElementsTargeting.get(bestTargetItem.getDenombrement()).getArg2();
                        countAllElementsTargeting.get(bestTargetItem.getDenombrement()).setArg2(oldCountSuccess + 1);
                    }
                }
            }

            for (int k : new TreeSet<>(countElements.keySet())) {
                TableRow rowOccurence = new TableRow(mainActivity);
                rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

                TextView twDnb = new TextView(mainActivity);
                twDnb.setPadding(10, 10, 10, 10);
                twDnb.setGravity(Gravity.CENTER);
                twDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                TextView twFreqTarget = new TextView(mainActivity);
                twFreqTarget.setPadding(10, 10, 10, 10);
                twFreqTarget.setGravity(Gravity.CENTER);
                twFreqTarget.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                TextView twFreqSuccess = new TextView(mainActivity);
                twFreqSuccess.setPadding(10, 10, 10, 10);
                twFreqSuccess.setGravity(Gravity.CENTER);
                twFreqSuccess.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                twDnb.setText(String.valueOf(k));
                twFreqTarget.setText(String.valueOf(countElements.get(k).getArg1()));
                twFreqSuccess.setText(String.valueOf(countElements.get(k).getArg2()));

                rowOccurence.addView(twDnb);
                rowOccurence.addView(twFreqTarget);
                rowOccurence.addView(twFreqSuccess);
                tableLayout.addView(rowOccurence);
            }


            LinearLayout linearLayoutOccurence = new LinearLayout(mainActivity);
            linearLayoutOccurence.setPadding(10, 50, 50, 10);
            linearLayoutOccurence.setOrientation(LinearLayout.VERTICAL);
            linearLayoutOccurence.addView(occurenceNumberTextViewShow);
            linearLayoutOccurence.addView(tableLayout);
            targetingScrollLayout.addView(linearLayoutOccurence);

        }

        TableLayout tableLayoutTotTargeting = new TableLayout(mainActivity);
        TableRow headerTotTargeting = new TableRow(mainActivity);

        headerTotTargeting.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));
        TextView totTargetingheaderDnb = new TextView(mainActivity);
        totTargetingheaderDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        totTargetingheaderDnb.setText("Dénombrement");
        totTargetingheaderDnb.setGravity(Gravity.CENTER);
        totTargetingheaderDnb.setPadding(10, 10, 10, 10);

        TextView totTargetingheaderFreqTargetChoiceTextView = new TextView(mainActivity);
        totTargetingheaderFreqTargetChoiceTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        totTargetingheaderFreqTargetChoiceTextView.setText("Fréq Cible");
        totTargetingheaderFreqTargetChoiceTextView.setGravity(Gravity.CENTER);
        totTargetingheaderFreqTargetChoiceTextView.setPadding(10, 10, 10, 10);

        TextView totTargetingheaderFreqSuccessTextView = new TextView(mainActivity);
        totTargetingheaderFreqSuccessTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        totTargetingheaderFreqSuccessTextView.setText("Freq Succès");
        totTargetingheaderFreqSuccessTextView.setGravity(Gravity.CENTER);
        totTargetingheaderFreqSuccessTextView.setPadding(10, 10, 10, 10);

        headerTotTargeting.addView(totTargetingheaderDnb);
        headerTotTargeting.addView(totTargetingheaderFreqTargetChoiceTextView);
        headerTotTargeting.addView(totTargetingheaderFreqSuccessTextView);

        tableLayoutTotTargeting.addView(headerTotTargeting);

        TextView totTargetingNumberTextViewShow = new TextView(mainActivity);
        totTargetingNumberTextViewShow.setTypeface(fontButton);
        totTargetingNumberTextViewShow.setText("Total");
        totTargetingNumberTextViewShow.setGravity(Gravity.CENTER);

        for (int k : new TreeSet<>(countAllElementsTargeting.keySet())) {
            TableRow rowOccurence = new TableRow(mainActivity);
            rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

            TextView twDnb = new TextView(mainActivity);
            twDnb.setPadding(10, 10, 10, 10);
            twDnb.setGravity(Gravity.CENTER);
            twDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

            TextView twFreqTarget = new TextView(mainActivity);
            twFreqTarget.setPadding(10, 10, 10, 10);
            twFreqTarget.setGravity(Gravity.CENTER);
            twFreqTarget.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

            TextView twFreqSuccess = new TextView(mainActivity);
            twFreqSuccess.setPadding(10, 10, 10, 10);
            twFreqSuccess.setGravity(Gravity.CENTER);
            twFreqSuccess.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

            twDnb.setText(String.valueOf(k));
            twFreqTarget.setText(String.valueOf(countAllElementsTargeting.get(k).getArg1()));
            twFreqSuccess.setText(String.valueOf(countAllElementsTargeting.get(k).getArg2()));

            rowOccurence.addView(twDnb);
            rowOccurence.addView(twFreqTarget);
            rowOccurence.addView(twFreqSuccess);
            tableLayoutTotTargeting.addView(rowOccurence);
        }


        LinearLayout linearLayoutTotTargeting = new LinearLayout(mainActivity);
        linearLayoutTotTargeting.setPadding(10, 50, 50, 10);
        linearLayoutTotTargeting.setOrientation(LinearLayout.VERTICAL);
        linearLayoutTotTargeting.addView(totTargetingNumberTextViewShow);
        linearLayoutTotTargeting.addView(tableLayoutTotTargeting);
        targetingScrollLayout.addView(linearLayoutTotTargeting);

        //score one

        HashMap<Integer, ArrayList<ScoreOneItem>> scoreOnes = dataWork.getScoreOnes();
        //50 item
        for (int key : scoreOnes.keySet()) {
            if (scoreOnes.get(key).size() < 50) scoreOnes.put(key, new ArrayList<ScoreOneItem>());
            else {
                for (int i = 0; i < 50; i++) {
                    scoreOnes.get(key).remove(0);
                }
            }
        }
        HashMap<String, Integer> countAllElementsScoreOne = new HashMap<>();
        for (int i : new TreeSet<>(scoreOnes.keySet())) {
            ArrayList<ScoreOneItem> scoreOnesOfOccurence = scoreOnes.get(i);

            TableLayout tableLayout = new TableLayout(mainActivity);
            TableRow header = new TableRow(mainActivity);
            TextView occurenceNumberTextViewShow = new TextView(mainActivity);

            header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

            TextView headerDnb = new TextView(mainActivity);
            headerDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            headerDnb.setText("Dénombrement");
            headerDnb.setGravity(Gravity.CENTER);
            headerDnb.setPadding(10, 10, 10, 10);

            TextView headerFreqTextView = new TextView(mainActivity);
            headerFreqTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            headerFreqTextView.setText("Fréquence");
            headerFreqTextView.setGravity(Gravity.CENTER);
            headerFreqTextView.setPadding(10, 10, 10, 10);

            header.addView(headerDnb);
            header.addView(headerFreqTextView);

            occurenceNumberTextViewShow.setTypeface(fontButton);
            occurenceNumberTextViewShow.setText("Occurence " + (i + 1));
            occurenceNumberTextViewShow.setGravity(Gravity.CENTER);

            tableLayout.addView(header);


            HashMap<String, Integer> countElements = new HashMap<>();
            for (ScoreOneItem scoreOneItem : scoreOnesOfOccurence) {

                Tuple<Integer, Integer> f1 = scoreOneItem.getFunctions().get(ScoreOneItem.F1);
                String sF1 = "";
                if (f1.getArg1() != null) sF1 += f1.getArg1();
                if (f1.getArg2() != null) {
                    sF1 += "." + f1.getArg2();
                    if (countElements.get(sF1) == null) {
                        countElements.put(sF1, 1);
                    } else {
                        countElements.put(sF1, countElements.get(sF1) + 1);
                    }

                    if (countAllElementsScoreOne.get(sF1) == null) {
                        countAllElementsScoreOne.put(sF1, 1);
                    } else {
                        countAllElementsScoreOne.put(sF1, countAllElementsScoreOne.get(sF1) + 1);
                    }
                }


                Tuple<Integer, Integer> f21 = scoreOneItem.getFunctions().get(ScoreOneItem.F2_1);
                String sF21 = "";
                if (f21 != null) {
                    if (f21.getArg1() != null) sF21 += f21.getArg1();
                    if (f21.getArg2() != null) {
                        sF21 += "." + f21.getArg2();
                        if (countElements.get(sF21) == null) {
                            countElements.put(sF21, 1);
                        } else {
                            countElements.put(sF21, countElements.get(sF21) + 1);
                        }

                        if (countAllElementsScoreOne.get(sF21) == null) {
                            countAllElementsScoreOne.put(sF21, 1);
                        } else {
                            countAllElementsScoreOne.put(sF21, countAllElementsScoreOne.get(sF21) + 1);
                        }
                    }
                }


                Tuple<Integer, Integer> f22 = scoreOneItem.getFunctions().get(ScoreOneItem.F2_2);
                String sF22 = "";
                if (f22 != null) {
                    if (f22.getArg1() != null) sF22 += f22.getArg1();
                    if (f22.getArg2() != null) {
                        sF22 += "." + f22.getArg2();
                        if (countElements.get(sF22) == null) {
                            countElements.put(sF22, 1);
                        } else {
                            countElements.put(sF22, countElements.get(sF22) + 1);
                        }

                        if (countAllElementsScoreOne.get(sF22) == null) {
                            countAllElementsScoreOne.put(sF22, 1);
                        } else {
                            countAllElementsScoreOne.put(sF22, countAllElementsScoreOne.get(sF22) + 1);
                        }
                    }
                }

                Tuple<Integer, Integer> f31 = scoreOneItem.getFunctions().get(ScoreOneItem.F3_1);
                String sF31 = "";
                if (f31 != null) {
                    if (f31.getArg1() != null) sF31 += f31.getArg1();
                    if (f31.getArg2() != null) {
                        sF31 += "." + f31.getArg2();
                        if (countElements.get(sF31) == null) {
                            countElements.put(sF31, 1);
                        } else {
                            countElements.put(sF31, countElements.get(sF31) + 1);
                        }

                        if (countAllElementsScoreOne.get(sF31) == null) {
                            countAllElementsScoreOne.put(sF31, 1);
                        } else {
                            countAllElementsScoreOne.put(sF31, countAllElementsScoreOne.get(sF31) + 1);
                        }
                    }
                }


                Tuple<Integer, Integer> f32 = scoreOneItem.getFunctions().get(ScoreOneItem.F3_2);
                String sF32 = "";
                if (f32 != null) {
                    if (f32.getArg1() != null) sF32 += f32.getArg1();
                    if (f32.getArg2() != null) {
                        sF32 += "." + f32.getArg2();
                        if (countElements.get(sF32) == null) {
                            countElements.put(sF32, 1);
                        } else {
                            countElements.put(sF32, countElements.get(sF32) + 1);
                        }

                        if (countAllElementsScoreOne.get(sF32) == null) {
                            countAllElementsScoreOne.put(sF32, 1);
                        } else {
                            countAllElementsScoreOne.put(sF32, countAllElementsScoreOne.get(sF32) + 1);
                        }
                    }
                }

                Tuple<Integer, Integer> f41 = scoreOneItem.getFunctions().get(ScoreOneItem.F4_1);
                String sF41 = "";
                if (f41 != null) {
                    if (f41.getArg1() != null) sF41 += f41.getArg1();
                    if (f41.getArg2() != null) {
                        sF41 += "." + f41.getArg2();
                        if (countElements.get(sF41) == null) {
                            countElements.put(sF41, 1);
                        } else {
                            countElements.put(sF41, countElements.get(sF41) + 1);
                        }

                        if (countAllElementsScoreOne.get(sF41) == null) {
                            countAllElementsScoreOne.put(sF41, 1);
                        } else {
                            countAllElementsScoreOne.put(sF41, countAllElementsScoreOne.get(sF41) + 1);
                        }
                    }
                }

                Tuple<Integer, Integer> f42 = scoreOneItem.getFunctions().get(ScoreOneItem.F4_2);
                String sF42 = "";
                if (f42 != null) {
                    if (f42.getArg1() != null) sF42 += f42.getArg1();
                    if (f42.getArg2() != null) {
                        sF42 += "." + f42.getArg2();
                        if (countElements.get(sF42) == null) {
                            countElements.put(sF42, 1);
                        } else {
                            countElements.put(sF42, countElements.get(sF42) + 1);
                        }

                        if (countAllElementsScoreOne.get(sF42) == null) {
                            countAllElementsScoreOne.put(sF42, 1);
                        } else {
                            countAllElementsScoreOne.put(sF42, countAllElementsScoreOne.get(sF42) + 1);
                        }
                    }
                }
            }

            for (String k : new TreeSet<>(countElements.keySet())) {
                TableRow rowOccurence = new TableRow(mainActivity);
                rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

                TextView twDnb = new TextView(mainActivity);
                twDnb.setPadding(10, 10, 10, 10);
                twDnb.setGravity(Gravity.CENTER);
                twDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                TextView twFreq = new TextView(mainActivity);
                twFreq.setPadding(10, 10, 10, 10);
                twFreq.setGravity(Gravity.CENTER);
                twFreq.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                twDnb.setText(k);
                twFreq.setText(String.valueOf(countElements.get(k)));

                rowOccurence.addView(twDnb);
                rowOccurence.addView(twFreq);
                tableLayout.addView(rowOccurence);

            }

            LinearLayout linearLayoutOccurence = new LinearLayout(mainActivity);
            linearLayoutOccurence.setPadding(10, 50, 50, 10);
            linearLayoutOccurence.setOrientation(LinearLayout.VERTICAL);
            linearLayoutOccurence.addView(occurenceNumberTextViewShow);
            linearLayoutOccurence.addView(tableLayout);
            scoreOneScrollLayout.addView(linearLayoutOccurence);

        }

        TableLayout tableLayoutTotScoreOne = new TableLayout(mainActivity);
        TableRow headerscoreOne = new TableRow(mainActivity);
        TextView totScoreOneNumberTextViewShow = new TextView(mainActivity);

        headerscoreOne.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

        TextView headerDnbScoreOne = new TextView(mainActivity);
        headerDnbScoreOne.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerDnbScoreOne.setText("Dénombrement");
        headerDnbScoreOne.setGravity(Gravity.CENTER);
        headerDnbScoreOne.setPadding(10, 10, 10, 10);

        TextView headerFreqTextViewScoreOne = new TextView(mainActivity);
        headerFreqTextViewScoreOne.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerFreqTextViewScoreOne.setText("Fréquence");
        headerFreqTextViewScoreOne.setGravity(Gravity.CENTER);
        headerFreqTextViewScoreOne.setPadding(10, 10, 10, 10);

        headerscoreOne.addView(headerDnbScoreOne);
        headerscoreOne.addView(headerFreqTextViewScoreOne);

        totScoreOneNumberTextViewShow.setTypeface(fontButton);
        totScoreOneNumberTextViewShow.setText("Total");
        totScoreOneNumberTextViewShow.setGravity(Gravity.CENTER);

        tableLayoutTotScoreOne.addView(headerscoreOne);

        for (String k : new TreeSet<>(countAllElementsScoreOne.keySet())) {
            TableRow rowOccurence = new TableRow(mainActivity);
            rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

            TextView twDnb = new TextView(mainActivity);
            twDnb.setPadding(10, 10, 10, 10);
            twDnb.setGravity(Gravity.CENTER);
            twDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

            TextView twFreq = new TextView(mainActivity);
            twFreq.setPadding(10, 10, 10, 10);
            twFreq.setGravity(Gravity.CENTER);
            twFreq.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

            twDnb.setText(k);
            twFreq.setText(String.valueOf(countAllElementsScoreOne.get(k)));

            rowOccurence.addView(twDnb);
            rowOccurence.addView(twFreq);
            tableLayoutTotScoreOne.addView(rowOccurence);

        }


        LinearLayout linearLayoutTotScoreOne = new LinearLayout(mainActivity);
        linearLayoutTotScoreOne.setPadding(10, 50, 50, 10);
        linearLayoutTotScoreOne.setOrientation(LinearLayout.VERTICAL);
        linearLayoutTotScoreOne.addView(totScoreOneNumberTextViewShow);
        linearLayoutTotScoreOne.addView(tableLayoutTotScoreOne);
        scoreOneScrollLayout.addView(linearLayoutTotScoreOne);

        //score two
        HashMap<Integer, ArrayList<ScoreTwoItem>> scoreTwos = dataWork.getScoreTwos();
        //50 item
        for (int key : scoreTwos.keySet()) {
            if (scoreTwos.get(key).size() < 50) scoreTwos.put(key, new ArrayList<ScoreTwoItem>());
            else {
                for (int i = 0; i < 50; i++) {
                    scoreTwos.get(key).remove(0);
                }
            }
        }
        HashMap<String, Integer> countAllElementsScoreTwo = new HashMap<>();
        for (int i : new TreeSet<>(scoreTwos.keySet())) {
            ArrayList<ScoreTwoItem> scoreTwosOfOccurence = scoreTwos.get(i);

            TableLayout tableLayout = new TableLayout(mainActivity);
            TableRow header = new TableRow(mainActivity);
            TextView occurenceNumberTextViewShow = new TextView(mainActivity);

            header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

            TextView headerDnb = new TextView(mainActivity);
            headerDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            headerDnb.setText("Dénombrement");
            headerDnb.setGravity(Gravity.CENTER);
            headerDnb.setPadding(10, 10, 10, 10);

            TextView headerFreqTextView = new TextView(mainActivity);
            headerFreqTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            headerFreqTextView.setText("Fréquence");
            headerFreqTextView.setGravity(Gravity.CENTER);
            headerFreqTextView.setPadding(10, 10, 10, 10);

            header.addView(headerDnb);
            header.addView(headerFreqTextView);

            occurenceNumberTextViewShow.setTypeface(fontButton);
            occurenceNumberTextViewShow.setText("Occurence " + (i + 1));
            occurenceNumberTextViewShow.setGravity(Gravity.CENTER);

            tableLayout.addView(header);


            HashMap<String, Integer> countElements = new HashMap<>();
            for (ScoreTwoItem scoreTwoItem : scoreTwosOfOccurence) {

                Tuple<Boolean, Tuple> F = scoreTwoItem.getFunctions().get(ScoreTwoItem.F);
                Tuple<Integer, Integer> infosF = F.getArg2();
                String sF = "";
                if (infosF.getArg1() != null) sF += infosF.getArg1();
                if ((infosF.getArg2() != null) && (infosF.getArg2() != 0)) {
                    sF += "." + infosF.getArg2();
                    if (countElements.get(sF) == null) {
                        countElements.put(sF, 1);
                    } else {
                        countElements.put(sF, countElements.get(sF) + 1);
                    }

                    if (countAllElementsScoreTwo.get(sF) == null) {
                        countAllElementsScoreTwo.put(sF, 1);
                    } else {
                        countAllElementsScoreTwo.put(sF, countAllElementsScoreTwo.get(sF) + 1);
                    }
                }
            }

            for (String k : new TreeSet<>(countElements.keySet())) {
                TableRow rowOccurence = new TableRow(mainActivity);
                rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

                TextView twDnb = new TextView(mainActivity);
                twDnb.setPadding(10, 10, 10, 10);
                twDnb.setGravity(Gravity.CENTER);
                twDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                TextView twFreq = new TextView(mainActivity);
                twFreq.setPadding(10, 10, 10, 10);
                twFreq.setGravity(Gravity.CENTER);
                twFreq.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                twDnb.setText(k);
                twFreq.setText(String.valueOf(countElements.get(k)));

                rowOccurence.addView(twDnb);
                rowOccurence.addView(twFreq);
                tableLayout.addView(rowOccurence);

            }


            LinearLayout linearLayoutOccurence = new LinearLayout(mainActivity);
            linearLayoutOccurence.setPadding(10, 50, 50, 10);
            linearLayoutOccurence.setOrientation(LinearLayout.VERTICAL);
            linearLayoutOccurence.addView(occurenceNumberTextViewShow);
            linearLayoutOccurence.addView(tableLayout);
            scoreTwoScrollLayout.addView(linearLayoutOccurence);
        }

        TableLayout tableLayoutTotScoreTwo = new TableLayout(mainActivity);
        TableRow headerscoreTwo = new TableRow(mainActivity);
        TextView totScoreTwoNumberTextViewShow = new TextView(mainActivity);

        headerscoreOne.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

        TextView headerDnbScoreTwo = new TextView(mainActivity);
        headerDnbScoreTwo.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerDnbScoreTwo.setText("Dénombrement");
        headerDnbScoreTwo.setGravity(Gravity.CENTER);
        headerDnbScoreTwo.setPadding(10, 10, 10, 10);

        TextView headerFreqTextViewScoreTwo = new TextView(mainActivity);
        headerFreqTextViewScoreTwo.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerFreqTextViewScoreTwo.setText("Fréquence");
        headerFreqTextViewScoreTwo.setGravity(Gravity.CENTER);
        headerFreqTextViewScoreTwo.setPadding(10, 10, 10, 10);

        headerscoreTwo.addView(headerDnbScoreTwo);
        headerscoreTwo.addView(headerFreqTextViewScoreTwo);

        totScoreTwoNumberTextViewShow.setTypeface(fontButton);
        totScoreTwoNumberTextViewShow.setText("Total");
        totScoreTwoNumberTextViewShow.setGravity(Gravity.CENTER);

        tableLayoutTotScoreTwo.addView(headerscoreTwo);

        for (String k : new TreeSet<>(countAllElementsScoreTwo.keySet())) {
            TableRow rowOccurence = new TableRow(mainActivity);
            rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

            TextView twDnb = new TextView(mainActivity);
            twDnb.setPadding(10, 10, 10, 10);
            twDnb.setGravity(Gravity.CENTER);
            twDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

            TextView twFreq = new TextView(mainActivity);
            twFreq.setPadding(10, 10, 10, 10);
            twFreq.setGravity(Gravity.CENTER);
            twFreq.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

            twDnb.setText(k);
            twFreq.setText(String.valueOf(countAllElementsScoreTwo.get(k)));

            rowOccurence.addView(twDnb);
            rowOccurence.addView(twFreq);
            tableLayoutTotScoreTwo.addView(rowOccurence);

        }


        LinearLayout linearLayoutTotScoreTwo = new LinearLayout(mainActivity);
        linearLayoutTotScoreTwo.setPadding(10, 50, 50, 10);
        linearLayoutTotScoreTwo.setOrientation(LinearLayout.VERTICAL);
        linearLayoutTotScoreTwo.addView(totScoreTwoNumberTextViewShow);
        linearLayoutTotScoreTwo.addView(tableLayoutTotScoreTwo);
        scoreTwoScrollLayout.addView(linearLayoutTotScoreTwo);

        //valorisation
        ArrayList<ValorisationItem> valorisations = dataWork.getValorisations();

        //50 item
        if (valorisations.size() < 50) valorisations = new ArrayList<>();
        else {
            for (int i = 0; i < 50; i++) {
                valorisations.remove(0);
            }
        }

        TableLayout tableLayout = new TableLayout(mainActivity);
        TableRow header = new TableRow(mainActivity);

        header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 5f));
        TextView headerValorisationTextView = new TextView(mainActivity);
        headerValorisationTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerValorisationTextView.setText("Valorisation");
        headerValorisationTextView.setGravity(Gravity.CENTER);
        headerValorisationTextView.setPadding(10, 10, 10, 10);

        TextView headerCountLoseTextView = new TextView(mainActivity);
        headerCountLoseTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerCountLoseTextView.setText("Freq Echec");
        headerCountLoseTextView.setGravity(Gravity.CENTER);
        headerCountLoseTextView.setPadding(10, 10, 10, 10);

        TextView headerCountSuccessTextView = new TextView(mainActivity);
        headerCountSuccessTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerCountSuccessTextView.setText("Freq Succès");
        headerCountSuccessTextView.setGravity(Gravity.CENTER);
        headerCountSuccessTextView.setPadding(10, 10, 10, 10);

        header.addView(headerValorisationTextView);
        header.addView(headerCountLoseTextView);
        header.addView(headerCountSuccessTextView);

        tableLayout.addView(header);

        HashMap<Integer, Tuple<Integer, Integer>> countElements = new HashMap<>();
        for (ValorisationItem valorisationItem : valorisations) {
            for (int o : valorisationItem.getValorisation().keySet()) {
                if (countElements.get(valorisationItem.getValorisation().get(o)) == null) {
                    Tuple<Integer, Integer> tupleToput = new Tuple<>();

                    if (valorisationItem.getBestTargetItem().get(o).isSuccess()) {
                        tupleToput.setArg1(0);
                        tupleToput.setArg2(1);
                    } else {
                        tupleToput.setArg1(1);
                        tupleToput.setArg2(0);
                    }

                    countElements.put(valorisationItem.getValorisation().get(o), tupleToput);
                } else {
                    Tuple<Integer, Integer> tupleToput = countElements.get(valorisationItem.getValorisation().get(o));

                    if (valorisationItem.getBestTargetItem().get(o).isSuccess()) {
                        tupleToput.setArg2(tupleToput.getArg2() + 1);
                    } else {
                        tupleToput.setArg1(tupleToput.getArg1() + 1);
                    }
                }
            }
        }

        int countLose = 0;
        int countSuccess = 0;
        for (int key : new TreeSet<>(countElements.keySet())) {
            if (countElements.get(key) != null) {
                TableRow rowOccurence = new TableRow(mainActivity);
                rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

                TextView twValorisation = new TextView(mainActivity);
                twValorisation.setPadding(10, 10, 10, 10);
                twValorisation.setGravity(Gravity.CENTER);
                twValorisation.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                TextView twFreqLose = new TextView(mainActivity);
                twFreqLose.setPadding(10, 10, 10, 10);
                twFreqLose.setGravity(Gravity.CENTER);
                twFreqLose.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                TextView twFreqSuccess = new TextView(mainActivity);
                twFreqSuccess.setPadding(10, 10, 10, 10);
                twFreqSuccess.setGravity(Gravity.CENTER);
                twFreqSuccess.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                countLose += (countElements.get(key).getArg1() * key);
                countSuccess += (countElements.get(key).getArg2() * key);

                twValorisation.setText(String.valueOf(key));
                twFreqLose.setText(String.valueOf(countElements.get(key).getArg1()));
                twFreqSuccess.setText(String.valueOf(countElements.get(key).getArg2()));

                rowOccurence.addView(twValorisation);
                rowOccurence.addView(twFreqLose);
                rowOccurence.addView(twFreqSuccess);
                tableLayout.addView(rowOccurence);
            }
        }


        TableRow rowOccurence = new TableRow(mainActivity);
        rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

        TextView twValorisation = new TextView(mainActivity);
        twValorisation.setPadding(10, 10, 10, 10);
        twValorisation.setGravity(Gravity.CENTER);
        twValorisation.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

        TextView twFreqLose = new TextView(mainActivity);
        twFreqLose.setPadding(10, 10, 10, 10);
        twFreqLose.setGravity(Gravity.CENTER);
        twFreqLose.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

        TextView twFreqSuccess = new TextView(mainActivity);
        twFreqSuccess.setPadding(10, 10, 10, 10);
        twFreqSuccess.setGravity(Gravity.CENTER);
        twFreqSuccess.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

        twValorisation.setText("Total");
        twFreqLose.setText(String.valueOf(countLose));
        twFreqSuccess.setText(String.valueOf(countSuccess));

        rowOccurence.addView(twValorisation);
        rowOccurence.addView(twFreqLose);
        rowOccurence.addView(twFreqSuccess);
        tableLayout.addView(rowOccurence);
        valorisationLayout.addView(tableLayout);
    }
}
