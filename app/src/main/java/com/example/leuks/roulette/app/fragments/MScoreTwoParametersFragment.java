package com.example.leuks.roulette.app.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.commons.bdd.Parameters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

/**
 * Created by leuks on 02/01/2017.
 */

public class MScoreTwoParametersFragment extends Fragment {
    private MainActivity mainActivity;
    private TableLayout dataTable;
    private HashMap<Integer, ArrayList<String>> denombrement;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.fragment_parameters_m_score_one_data_layout, container, false);

        //font
        final Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");

        //childs
        dataTable = (TableLayout) thisView.findViewById(R.id.table_data);
        Button validateButton = (Button) thisView.findViewById(R.id.button_validate_data_parameters);
        Button addButton = (Button) thisView.findViewById(R.id.button_more);

        //listeners
        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Parameters parameters = mainActivity.getHelper().getParameters();
                final Parameters oldParameters = mainActivity.getHelper().getParameters();
                HashMap<String, Integer> mScoreTwo = parameters.getmScoreTwo();

                ArrayList<String> buttonTexts = new ArrayList<>();
                //add
                for (int i = 1; i < dataTable.getChildCount(); i++) {
                    TableRow indexedRow = (TableRow) dataTable.getChildAt(i);

                    int value = Integer.parseInt(((TextView) indexedRow.getChildAt(1)).getText().toString());
                    LinearLayout buttonLayout = (LinearLayout) indexedRow.getChildAt(0);

                    for (int b = 0; b < buttonLayout.getChildCount(); b++) {
                        Button button = (Button) buttonLayout.getChildAt(b);
                        String buttonText = button.getText().toString();
                        buttonTexts.add(buttonText);

                        if (buttonText.equals(">")) {
                            if (!parameters.existsMScoreTwoKey(">")) {
                                mScoreTwo.put(">", value);
                            } else {
                                if (parameters.getValuemScoreTwoOf(">") != value) {
                                    mScoreTwo.remove(">");
                                    mScoreTwo.put(">", value);
                                }
                            }
                        } else if (buttonText.equals("<")) {
                            if (!parameters.existsMScoreTwoKey("<")) {
                                mScoreTwo.put("<", value);
                            } else {
                                if (parameters.getValuemScoreTwoOf("<") != value) {
                                    mScoreTwo.remove("<");
                                    mScoreTwo.put("<", value);
                                }
                            }
                        } else {

                            if (!parameters.existsMScoreTwoKey(buttonText)) {
                                mScoreTwo.put(buttonText, value);
                            } else {
                                if (parameters.getValuemScoreTwoOf(buttonText) != value) {
                                    mScoreTwo.remove(buttonText);
                                    mScoreTwo.put(buttonText, value);
                                }
                            }
                        }
                    }
                }

                ArrayList<String> toRemove = new ArrayList<>();
                //delete
                for (String key : mScoreTwo.keySet()) {
                    if (!buttonTexts.contains(key)) {
                        toRemove.add(key);
                    }
                }

                for (String key : toRemove) {
                    mScoreTwo.remove(key);
                }

                mainActivity.getHelper().updateParameters(parameters);

                Snackbar snackbar = Snackbar
                        .make(mainActivity.findViewById(android.R.id.content), "Paramètres mis à jour!", Snackbar.LENGTH_LONG)
                        .setAction("ANNULER", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mainActivity.getHelper().updateParameters(oldParameters);
                                refreshView();
                                Snackbar snackbar1 = Snackbar.make(mainActivity.findViewById(android.R.id.content), "Paramètres rétablis!", Snackbar.LENGTH_SHORT);
                                snackbar1.show();
                            }
                        });
                snackbar.show();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(mainActivity);
                dialog.setTitle("Ajouter un dénombrement");
                dialog.setContentView(R.layout.dialog_new_dnb_layout);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;

                //components
                final EditText editText = (EditText) dialog.findViewById(R.id.editText_dnb);
                final EditText editTextValue = (EditText) dialog.findViewById(R.id.editText_value);
                Button buttonMore = (Button) dialog.findViewById(R.id.button_more);
                Button buttonLess = (Button) dialog.findViewById(R.id.button_less);
                Button validate_button = (Button) dialog.findViewById(R.id.button_validate);
                TextView twDnbShow = (TextView) dialog.findViewById(R.id.textView_denombrement_show);
                TextView twDValueShow = (TextView) dialog.findViewById(R.id.textView_value_show);

                //parameters
                editText.setInputType(InputType.TYPE_CLASS_PHONE);
                editTextValue.setInputType(InputType.TYPE_CLASS_PHONE);

                //font
                twDnbShow.setTypeface(fontButton);
                twDValueShow.setTypeface(fontButton);

                //listener
                buttonMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editText.setText(">");
                    }
                });

                buttonLess.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editText.setText("<");
                    }
                });

                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (editText.getText().toString().equals(">") || editText.getText().toString().equals("<")) {
                            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});
                        } else {
                            editText.setFilters(new InputFilter[]{});
                        }
                        editText.setSelection(editText.getText().length());
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });

                validate_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String textDnb = editText.getText().toString().trim();
                        String textValue = editTextValue.getText().toString().trim();

                        if ((Pattern.compile("\\d+").matcher(textValue).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textValue).find())) {
                            if (Pattern.compile("^([0-9]|n|N)+[.]([0-9]|n|N)+?$").matcher(textDnb).find() || textDnb.equals(">") || textDnb.equals("<")) {
                                if (denombrement.get(Integer.parseInt(textValue)) == null) {
                                    denombrement.put(Integer.parseInt(textValue), new ArrayList<String>());
                                }

                                if (textDnb.equals(">")) {
                                    if (!valueExists(">")) {
                                        denombrement.get(Integer.parseInt(textValue)).add(">");
                                    } else {
                                        Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                    }
                                } else if (textDnb.equals("<")) {
                                    if (!valueExists("<")) {
                                        denombrement.get(Integer.parseInt(textValue)).add("<");
                                    } else {
                                        Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    if (!valueExists(textDnb)) {
                                        denombrement.get(Integer.parseInt(textValue)).add(textDnb);
                                    } else {
                                        Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                refreshViewFromDenombrement();
                                dialog.dismiss();
                                return;
                            } else {
                                Toast.makeText(mainActivity, "Décimal uniquement", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        } else {
                            Toast.makeText(mainActivity, "Décimal uniquement", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                });


                dialog.getWindow().setAttributes(lp);
                dialog.show();
            }
        });

        return thisView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshView();
    }

    private void refreshView() {
        Parameters parameters = mainActivity.getHelper().getParameters();
        HashMap<String, Integer> mScoreTwo = parameters.getmScoreTwo();
        denombrement = new HashMap<>();
        Set<String> keys = mScoreTwo.keySet();
        for (String key : keys) {
            int value = mScoreTwo.get(key);
            if (denombrement.get(value) == null) {
                ArrayList<String> arrayToPut = new ArrayList<>();
                arrayToPut.add(key);
                denombrement.put(value, arrayToPut);
            } else {
                denombrement.get(value).add(key);
            }
        }

        refreshViewFromDenombrement();
    }

    private void refreshViewFromDenombrement() {
        Parameters parameters = mainActivity.getHelper().getParameters();
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        dataTable.removeAllViews();

        //header
        TableRow header = new TableRow(mainActivity);
        header.setGravity(Gravity.CENTER);
        header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 2f));

        TextView headerDnb = new TextView(mainActivity);
        headerDnb.setGravity(Gravity.CENTER);
        headerDnb.setPadding(10, 10, 10, 10);
        headerDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerDnb.setText("Dnb Score 2");
        headerDnb.setTypeface(fontButton);

        TextView headerValue = new TextView(mainActivity);
        headerValue.setPadding(10, 10, 10, 10);
        headerValue.setGravity(Gravity.CENTER);
        headerValue.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerValue.setText("Valeur Score 2");
        headerValue.setTypeface(fontButton);


        header.addView(headerDnb);
        header.addView(headerValue);

        dataTable.addView(header);

        ArrayList<Integer> toRemove = new ArrayList<>();
        for (Integer key : denombrement.keySet()) {
            if (denombrement.get(key).size() == 0) toRemove.add(key);
        }
        for (Integer key : toRemove) {
            denombrement.remove(key);
        }


        for (final Integer key : new TreeSet<>(denombrement.keySet())) {
            final ArrayList<String> numberList = denombrement.get(key);
            TableRow row = new TableRow(mainActivity);
            row.setGravity(Gravity.CENTER);
            row.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            LinearLayout numberLayout = new LinearLayout(mainActivity);
            numberLayout.setGravity(Gravity.CENTER);
            numberLayout.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


            TextView twValue = new TextView(mainActivity);
            twValue.setPadding(10, 10, 10, 10);
            twValue.setGravity(Gravity.CENTER);
            twValue.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            twValue.setTypeface(fontButton);


            for (int s = 0; s < numberList.size(); s++) {
                Button button = new Button(mainActivity);
                button.setGravity(Gravity.CENTER);
                button.setText(numberList.get(s));
                button.setTypeface(fontButton);
                final int ind = s;
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                        builder.setTitle("Action : " + numberList.get(ind));
                        builder.setPositiveButton("Modifier", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                final AlertDialog.Builder editBuilder = new AlertDialog.Builder(mainActivity);
                                editBuilder.setTitle("Modification : " + numberList.get(ind));
                                final EditText newValueED = new EditText(mainActivity);
                                newValueED.setGravity(Gravity.CENTER);
                                newValueED.setInputType(InputType.TYPE_CLASS_PHONE);

                                editBuilder.setView(newValueED);
                                editBuilder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        String newValue = newValueED.getText().toString().trim();
                                        if (Pattern.compile("\\d+").matcher(newValue).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(newValue).find()) {
                                            int intNewValue = Integer.parseInt(newValue);
                                            String elemToRemove = denombrement.get(key).get(ind);
                                            denombrement.get(key).remove(elemToRemove);
                                            if (denombrement.get(intNewValue) == null) {
                                                ArrayList<String> denombrementList = new ArrayList<>();
                                                denombrementList.add(elemToRemove);
                                                denombrement.put(intNewValue, denombrementList);
                                            } else {
                                                denombrement.get(intNewValue).add(elemToRemove);
                                            }

                                            refreshViewFromDenombrement();
                                        } else {
                                            Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    }
                                });
                                editBuilder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        return;
                                    }
                                });
                                editBuilder.show();
                            }
                        });
                        builder.setNegativeButton("Supprimer", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                final AlertDialog.Builder editBuilder = new AlertDialog.Builder(mainActivity);
                                editBuilder.setTitle("Suppresion : " + numberList.get(ind));
                                editBuilder.setMessage("Êtes-vous sûr ?");
                                editBuilder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        denombrement.get(key).remove(ind);
                                        refreshViewFromDenombrement();
                                        return;
                                    }
                                });
                                editBuilder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        return;
                                    }
                                });
                                editBuilder.show();
                            }
                        });
                        builder.show();
                    }
                });
                numberLayout.addView(button);
            }

            twValue.setText(String.valueOf(key));

            row.addView(numberLayout);
            row.addView(twValue);
            dataTable.addView(row);
        }
    }

    private boolean valueExists(String value) {
        boolean exists = false;
        for (int key : denombrement.keySet()) {
            for (String child : denombrement.get(key)) {
                if (child.equals(value)) exists = true;
            }
        }
        return exists;
    }
}
