package com.example.leuks.roulette.app.classes.tasks.action;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.app.classes.UtilObj;
import com.example.leuks.roulette.app.classes.UtilObj.AddHandlerObject;
import com.example.leuks.roulette.app.classes.Work;
import com.example.leuks.roulette.commons.bdd.BestTargetItem;
import com.example.leuks.roulette.commons.bdd.Parameters;
import com.example.leuks.roulette.commons.bdd.Target;
import com.example.leuks.roulette.commons.entities.Tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Leuks on 22/11/2016.
 */

public class AddMatchingTask extends Thread {
    public Handler tHandler;
    private MainActivity mainActivity;
    private int shot;
    private int indTab;
    private HashMap<Integer, ArrayList<Target>> occurences;
    private Work work;
    private Integer simpleChance;
    private int lastId;
    private int lastIdBest;
    private Parameters parameters;

    public AddMatchingTask(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        parameters = mainActivity.getHelper().getParameters();
    }

    @Override
    public void run() {
        Looper.prepare();
        tHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                AddHandlerObject obj = (AddHandlerObject) msg.obj;
                shot = obj.getShot();
                indTab = obj.getIndTab();
                occurences = obj.getOccurences();
                work = obj.getWork();
                simpleChance = obj.getSimpleChance();
                lastId = obj.getLastId();
                lastIdBest = obj.getLastBestId();

                MainActivity.dep = System.currentTimeMillis();

                int startInd;

                //Begin with the given simple chance in the object or a random one otherwise
                if (simpleChance == null) startInd = Tools.randomIntWithInclusiveBounds(0, 5);
                else startInd = simpleChance;

                ArrayList<Integer> matches = parameters.getMatchesOf(shot);

                for (int i = 0; i < parameters.getOccurencesNumber(); i++) {
                    int match = matches.get(i);
                    boolean run = true;
                    int incChanceTesting = 0;

                    //If first line in the tab, set an new initialisation target
                    if (occurences.get(i) == null) {
                        Integer[] firstOccurence = {1, 1, 1, 1, 1, 1};
                        int newID = lastId + 1 + ((indTab + 1) * parameters.getOccurencesNumber()) + i;
                        Target firstTarget = new Target(shot, match, i, indTab, startInd, new ArrayList<>(Arrays.asList(firstOccurence)), newID);

                        ArrayList<Target> arrayToPut = new ArrayList<>();
                        arrayToPut.add(firstTarget);
                        occurences.put(i, arrayToPut);

                    } else {
                        ArrayList<Target> targetList = occurences.get(i);
                        Target lastTarget = targetList.get(targetList.size() - 1);
                        ArrayList<Integer> lastOccurence = lastTarget.getOccurence();
                        ArrayList<Integer> occurence = new ArrayList<>();
                        int currentInd = lastTarget.getFirstChance();

                        //Interpret the shot with the current simlple chance
                        while (run) {
                            if (shot == 0) {
                                occurence.add(lastOccurence.get(incChanceTesting) + 1);
                            } else {
                                boolean success = false;
                                switch (currentInd) {
                                    case MainActivity.NR_SIMPLE_CHANCE:
                                        if (!parameters.isRed(match)) success = true;
                                        break;
                                    case MainActivity.RE_SIMPLE_CHANCE:
                                        if (parameters.isRed(match)) success = true;
                                        break;
                                    case MainActivity.PR_SIMPLE_CHANCE:
                                        if (match % 2 == 0) success = true;
                                        break;
                                    case MainActivity.IR_SIMPLE_CHANCE:
                                        if (!(match % 2 == 0)) success = true;
                                        break;
                                    case MainActivity.ME_SIMPLE_CHANCE:
                                        if ((match > 0) && (match <= 18)) success = true;
                                        break;
                                    case MainActivity.PE_SIMPLE_CHANCE:
                                        if ((match > 18) && (match <= 36)) success = true;
                                        break;
                                }

                                //Set 1 if success, increment otherwise
                                if (success) occurence.add(1);
                                else occurence.add(lastOccurence.get(incChanceTesting) + 1);
                            }

                            currentInd++;
                            incChanceTesting++;
                            if (currentInd > 5) currentInd = 0;
                            if (incChanceTesting > 5) run = false;
                        }

                        //Add the new target with new Id for database primary key
                        int newID = lastId + 1 + ((indTab + 1) * parameters.getOccurencesNumber()) + i;
                        Target newTarget = new Target(shot, match, i, indTab, startInd, occurence, newID);
                        occurences.get(i).add(newTarget);
                    }
                }

                HashMap<Integer, ArrayList<BestTargetItem>> exitOccurence = new HashMap<>();
                UtilObj.ReturnHandlerObject returnHandlerObject = new UtilObj.ReturnHandlerObject(indTab);
                for (int i = 0; i < parameters.getOccurencesNumber(); i++) {

                    //Choose a line to start for all tabs (all tabs havn't the same number of lines )
                    exitOccurence.put(i, new ArrayList<BestTargetItem>());
                    ArrayList<Target> targetsOfOccurence = occurences.get(i);
                    int ind;
                    if (targetsOfOccurence.size() - 2 < 0) ind = 0;
                    else ind = targetsOfOccurence.size() - 2;
                    for (int j = ind; j < targetsOfOccurence.size(); j++) {
                        //Compare actual target with the next target
                        Target currentTarget = targetsOfOccurence.get(j);
                        boolean success = false;
                        Target nextTarget = null;
                        if (j != targetsOfOccurence.size() - 1)
                            nextTarget = targetsOfOccurence.get(j + 1);

                        //Choose the max value between all occurences
                        int indMax = 0;
                        int max = 0;
                        ArrayList<Integer> currentOccurence = currentTarget.getOccurence();
                        for (int k = 0; k < currentOccurence.size(); k++) {
                            int actualValue = currentOccurence.get(k);
                            if (actualValue > max) {
                                max = actualValue;
                                indMax = k;
                            }
                        }


                        //if this isn't the end
                        if (j != targetsOfOccurence.size() - 1) {
                            //set success if next is 0
                            if (nextTarget.getOccurence().get(indMax) == 1 && (nextTarget.getMatch() != 0)) {
                                success = true;
                                returnHandlerObject.getTArgetsToUpdate().add(new TargetToUpdate(currentTarget, i, indTab));
                            }
                        } else {
                            //Create new best target item with the max value of all occurences
                            int newID = lastIdBest + 1 + ((indTab + 1) * parameters.getOccurencesNumber()) + i;
                            BestTargetItem bestTargetItem = new BestTargetItem(currentTarget, success, indMax, newID);
                            exitOccurence.get(i).add(bestTargetItem);
                        }
                    }
                }

                //push the new array
                returnHandlerObject.setExitOccurence(exitOccurence);
                returnHandlerObject.setOccurences(occurences);
                Message m = mainActivity.addTargetHandler.obtainMessage();
                m.obj = returnHandlerObject;
                mainActivity.addTargetHandler.dispatchMessage(m);
            }
        };
        Looper.loop();
    }
}
