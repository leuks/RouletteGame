package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.commons.bdd.Parameters;
import com.example.leuks.roulette.commons.entities.Tuple;

/**
 * Created by Leuks on 12/12/2016.
 */

public class ParametersAlgorithmFragment extends Fragment {
    private MainActivity mainActivity;
    private CheckBox scoreOneCheckBox;
    private CheckBox scoreTwoCheckBox;
    private TextView occurencesNumberTextView;
    private TextView tabNumberTextView;
    private SeekBar occurencesNumberSeekBar;
    private SeekBar tabNumberSeekBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.fragment_parameters_algorithm, container, false);

        //childs
        occurencesNumberTextView = (TextView) thisView.findViewById(R.id.textView_occurences_number);
        tabNumberTextView = (TextView) thisView.findViewById(R.id.textView_tab_number);
        occurencesNumberSeekBar = (SeekBar) thisView.findViewById(R.id.seekBar_occurences);
        tabNumberSeekBar = (SeekBar) thisView.findViewById(R.id.seekBar_tab);
        scoreOneCheckBox = (CheckBox) thisView.findViewById(R.id.checkbox_score1);
        scoreTwoCheckBox = (CheckBox) thisView.findViewById(R.id.checkbox_score2);
        Button validateButton = (Button) thisView.findViewById(R.id.button_validate_algorithm_parameters);
        TextView occurencesNumberTextViewShow = (TextView) thisView.findViewById(R.id.textView_occurences_number_show);
        TextView scoresTextViewShow = (TextView) thisView.findViewById(R.id.textView_scores_show);
        TextView tabsTextViewShow = (TextView) thisView.findViewById(R.id.textView_tab_number_show);

        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        occurencesNumberTextView.setTypeface(fontButton);
        tabNumberTextView.setTypeface(fontButton);
        scoreOneCheckBox.setTypeface(fontButton);
        scoreTwoCheckBox.setTypeface(fontButton);
        occurencesNumberTextViewShow.setTypeface(fontButton);
        scoresTextViewShow.setTypeface(fontButton);
        tabsTextViewShow.setTypeface(fontButton);

        //listeners
        occurencesNumberSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                occurencesNumberTextView.setText(String.valueOf(progress + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        tabNumberSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tabNumberTextView.setText(String.valueOf(progress + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        scoreOneCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if ((!isChecked) && (!scoreTwoCheckBox.isChecked()))
                    scoreTwoCheckBox.setChecked(true);
            }
        });

        scoreTwoCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if ((!isChecked) && (!scoreOneCheckBox.isChecked()))
                    scoreOneCheckBox.setChecked(true);
            }
        });

        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Parameters parameters = mainActivity.getHelper().getParameters();
                final Parameters oldparameters = mainActivity.getHelper().getParameters();
                int wantedOccurencesNumber = Integer.parseInt(occurencesNumberTextView.getText().toString());
                int wantedTabNumber = Integer.parseInt(tabNumberTextView.getText().toString());

                if (wantedOccurencesNumber != parameters.getOccurencesNumber())
                    parameters.setOccurencesNumber(wantedOccurencesNumber);
                if (wantedTabNumber != parameters.getTabsNumber())
                    parameters.setTabsNumber(wantedTabNumber);

                if (scoreOneCheckBox.isChecked()) parameters.setScoreOne(true);
                else parameters.setScoreOne(false);

                if (scoreTwoCheckBox.isChecked()) parameters.setScoreTwo(true);
                else parameters.setScoreTwo(false);

                mainActivity.getHelper().updateParameters(parameters);

                Snackbar snackbar = Snackbar
                        .make(mainActivity.findViewById(android.R.id.content), "Paramètres mis à jour!", Snackbar.LENGTH_LONG)
                        .setAction("ANNULER", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mainActivity.getHelper().updateParameters(oldparameters);
                                refreshView();
                                Snackbar snackbar1 = Snackbar.make(mainActivity.findViewById(android.R.id.content), "Paramètres rétablis!", Snackbar.LENGTH_SHORT);
                                snackbar1.show();
                            }
                        });
                snackbar.show();
            }
        });

        return thisView;
    }

    private void refreshView() {
        Parameters parameters = mainActivity.getHelper().getParameters();
        int occurrencesNumber = parameters.getOccurencesNumber();
        int tabsNumber = parameters.getTabsNumber();
        Tuple<Boolean, Boolean> scores = parameters.getScores();

        if (scores.getArg1()) scoreOneCheckBox.setChecked(true);
        else scoreOneCheckBox.setChecked(false);
        if (scores.getArg2()) scoreTwoCheckBox.setChecked(true);
        else scoreTwoCheckBox.setChecked(false);

        occurencesNumberTextView.setText(String.valueOf(occurrencesNumber));
        tabNumberTextView.setText(String.valueOf(tabsNumber));
        occurencesNumberSeekBar.setProgress(occurrencesNumber - 1);
        tabNumberSeekBar.setProgress(tabsNumber - 1);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshView();
    }
}
