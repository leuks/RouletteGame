package com.example.leuks.roulette.commons.bdd;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by Leuks on 22/12/2016.
 */

@DatabaseTable(tableName = "BestTargetItem")
public class BestTargetItem implements Serializable {
    @DatabaseField(id = true)
    int id;
    @DatabaseField(foreign = true, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 5, columnName = "target")
    private Target target;
    @DatabaseField
    private boolean success;
    @DatabaseField
    private int indOfMaxOccurence;

    public BestTargetItem(Target target, boolean success, int indOfMaxOccurence, int id) {
        this.target = target;
        this.success = success;
        this.indOfMaxOccurence = indOfMaxOccurence;
        this.id = id;
    }

    public BestTargetItem() {
    }

    @Override
    public boolean equals(Object obj) {
        BestTargetItem bestTargetItem = (BestTargetItem) obj;
        return bestTargetItem.getTarget().equals(target);
    }

    //GS

    public Target getTarget() {
        return target;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getIndOfMaxOccurence() {
        return indOfMaxOccurence;
    }

    public int getSimpleChance() {
        return ((getTarget().getFirstChance() + getIndOfMaxOccurence()) % 6);
    }

    public int getId() {
        return id;
    }

    public int getDenombrement() {
        return target.getOccurence().get(indOfMaxOccurence);
    }
}
