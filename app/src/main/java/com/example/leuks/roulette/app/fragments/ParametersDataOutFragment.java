package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Space;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.commons.bdd.Parameters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by leuks on 30/12/2016.
 */

public class ParametersDataOutFragment extends Fragment {
    private MainActivity mainActivity;
    private TableLayout dataTable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.fragment_parameters_out_data_layout, container, false);

        //childs
        dataTable = (TableLayout) thisView.findViewById(R.id.table_data);
        Button validateButton = (Button) thisView.findViewById(R.id.button_validate_data_parameters);
        TextView twShow1 = (TextView) thisView.findViewById(R.id.show1);
        TextView twShow2 = (TextView) thisView.findViewById(R.id.show2);
        TextView twShow3 = (TextView) thisView.findViewById(R.id.show3);
        TextView twShow4 = (TextView) thisView.findViewById(R.id.show4);
        TextView twShow5 = (TextView) thisView.findViewById(R.id.show5);
        TextView twShow6 = (TextView) thisView.findViewById(R.id.show6);

        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        twShow1.setTypeface(fontButton);
        twShow2.setTypeface(fontButton);
        twShow3.setTypeface(fontButton);
        twShow4.setTypeface(fontButton);
        twShow5.setTypeface(fontButton);
        twShow6.setTypeface(fontButton);

        //listeners
        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Parameters parameters = mainActivity.getHelper().getParameters();
                final Parameters oldParameters = mainActivity.getHelper().getParameters();
                for (int i = 1; i < dataTable.getChildCount(); i++) {
                    TableRow indexedRow = (TableRow) dataTable.getChildAt(i);
                    ArrayList<Double> matches = parameters.getExitMatching().get(i - 1);
                    for (int j = 1; j < indexedRow.getChildCount(); j++) {
                        String stringValue = ((EditText) indexedRow.getChildAt(j)).getText().toString().trim();
                        if ((Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(stringValue).find()
                                || stringValue.equals("noir")
                                || stringValue.equals("rouge")
                                || stringValue.equals("pair")
                                || stringValue.equals("impair")
                                || stringValue.equals("manque")
                                || stringValue.equals("passe")
                                && (!(stringValue.contains(".."))
                                && (!(stringValue.contains(",")))))) {

                            int simpleChance = ResultFragment.findExitFromSimpleChance(stringValue);
                            Double finalValue = null;
                            if (simpleChance != -1) {
                                finalValue = (double) simpleChance;
                            } else {
                                finalValue = Double.parseDouble(stringValue);
                            }

                            if (matches.get(j - 1) != finalValue) matches.set(j - 1, finalValue);
                        } else {
                            Toast.makeText(mainActivity, "Entiers ou décimaux uniquement", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                }
                mainActivity.getHelper().updateParameters(parameters);

                Snackbar snackbar = Snackbar
                        .make(mainActivity.findViewById(android.R.id.content), "Paramètres mis à jour!", Snackbar.LENGTH_LONG)
                        .setAction("ANNULER", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mainActivity.getHelper().updateParameters(oldParameters);
                                refreshView();
                                Snackbar snackbar1 = Snackbar.make(mainActivity.findViewById(android.R.id.content), "Paramètres rétablis!", Snackbar.LENGTH_SHORT);
                                snackbar1.show();
                            }
                        });
                snackbar.show();
            }
        });

        return thisView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshView();
    }

    private void refreshView() {
        Parameters parameters = mainActivity.getHelper().getParameters();
        HashMap<Integer, ArrayList<Double>> exitMatching = parameters.getExitMatching();

        dataTable.removeAllViews();
        TableRow header = new TableRow(mainActivity);
        header.setGravity(Gravity.CENTER);
        header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

        Space headerSpace = new Space(mainActivity);
        headerSpace.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        header.addView(headerSpace);
        for (int i = 0; i < exitMatching.get(0).size(); i++) {
            TextView cell = new TextView(mainActivity);
            cell.setGravity(Gravity.CENTER);
            cell.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
            cell.setText(String.valueOf(i + 1));
            cell.setBackgroundColor(getResources().getColor(R.color.colorBlue));
            cell.setPadding(10, 10, 10, 10);
            header.addView(cell);
        }

        dataTable.addView(header);

        for (int i = 0; i < exitMatching.size(); i++) {
            ArrayList<Double> matches = exitMatching.get(i);
            TableRow row = new TableRow(mainActivity);
            TextView indicator = new TextView(mainActivity);

            row.setGravity(Gravity.CENTER);
            row.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

            indicator.setGravity(Gravity.CENTER);
            indicator.setText(ResultFragment.findSimpleChanceFromExit(String.valueOf((double) i)));
            indicator.setBackgroundColor(getResources().getColor(R.color.colorBlue));
            indicator.setPadding(10, 10, 10, 10);
            row.addView(indicator);

            for (double match : matches) {
                final EditText cell = new EditText(mainActivity);
                cell.setGravity(Gravity.CENTER);
                cell.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
                cell.setInputType(InputType.TYPE_CLASS_PHONE);

                if (ResultFragment.findSimpleChanceFromExit(String.valueOf((double) match)) == String.valueOf((double) match)) {
                    cell.setText(String.valueOf(match));
                } else
                    cell.setText(ResultFragment.findSimpleChanceFromExit(String.valueOf((double) match)));


                cell.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        String doubleValue = null;
                        try {
                            doubleValue = String.valueOf(Double.parseDouble(cell.getText().toString()));
                            if (ResultFragment.findSimpleChanceFromExit(doubleValue) != doubleValue) {
                                cell.setText(ResultFragment.findSimpleChanceFromExit(doubleValue));
                            }
                        } catch (Exception e) {

                        }
                    }
                });

                row.addView(cell);
            }

            dataTable.addView(row);
        }
    }
}
