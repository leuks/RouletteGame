package com.example.leuks.roulette.app.classes.tasks.action;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.commons.bdd.BestTargetItem;
import com.example.leuks.roulette.commons.bdd.ScoreOneItem;
import com.example.leuks.roulette.commons.bdd.ScoreTwoItem;
import com.example.leuks.roulette.commons.bdd.Target;
import com.example.leuks.roulette.commons.bdd.TargetingStatItem;
import com.example.leuks.roulette.commons.bdd.ValorisationItem;

/**
 * Created by Leuks on 17/02/2017.
 */

public class DBWorkTask extends Thread {
    public static final int UPDATE = 0;
    public static final int ADD = 1;
    public Handler dbHandler;
    private MainActivity mainActivity;


    public DBWorkTask(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void run() {
        Looper.prepare();
        dbHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                int action = msg.arg1;
                int arg = msg.arg2;
                Object o = msg.obj;

                if (o instanceof Target) {
                    if (action == UPDATE) {
                        mainActivity.getHelper().updateBestTargetItem((Target) o, arg == 1 ? true : false);
                    } else {
                        mainActivity.getHelper().createTarget((Target) o);
                    }
                } else if (o instanceof BestTargetItem) {
                    mainActivity.getHelper().createBestTargetItem((BestTargetItem) o);
                } else if (o instanceof ScoreOneItem) {
                    mainActivity.getHelper().createScoreOne((ScoreOneItem) o);
                } else if (o instanceof ScoreTwoItem) {
                    if (action == UPDATE) {
                        mainActivity.getHelper().updateScoreTwoItem((ScoreTwoItem) o);
                    } else {
                        mainActivity.getHelper().createScoreTwo((ScoreTwoItem) o);
                    }
                } else if (o instanceof TargetingStatItem) {
                    mainActivity.getHelper().createTargetingStatItem((TargetingStatItem) o);
                } else if (o instanceof ValorisationItem) {
                    mainActivity.getHelper().createValorisationItem((ValorisationItem) o);
                }
            }
        };
        Looper.loop();
    }
}
