package com.example.leuks.roulette;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.example.leuks.roulette.app.classes.DataWork;
import com.example.leuks.roulette.app.classes.UtilObj;
import com.example.leuks.roulette.app.classes.Work;
import com.example.leuks.roulette.app.classes.WorkManager;
import com.example.leuks.roulette.app.classes.tasks.action.AddMatchingTask;
import com.example.leuks.roulette.app.classes.tasks.action.DBWorkTask;
import com.example.leuks.roulette.app.classes.tasks.action.TargetToUpdate;
import com.example.leuks.roulette.app.fragments.DataChoiceFragment;
import com.example.leuks.roulette.app.fragments.DataParametersChoiceFragment;
import com.example.leuks.roulette.app.fragments.EntryAutoFragment;
import com.example.leuks.roulette.app.fragments.EntryChoiceFragment;
import com.example.leuks.roulette.app.fragments.EntryManualFragment;
import com.example.leuks.roulette.app.fragments.HistoryChoiceFragment;
import com.example.leuks.roulette.app.fragments.HistoryScoreFraqment;
import com.example.leuks.roulette.app.fragments.HistoryTargetingFragment;
import com.example.leuks.roulette.app.fragments.HistoryValorisationFragment;
import com.example.leuks.roulette.app.fragments.MScoreOneParametersFragment;
import com.example.leuks.roulette.app.fragments.MScoreTwoParametersFragment;
import com.example.leuks.roulette.app.fragments.ParametersAlgorithmFragment;
import com.example.leuks.roulette.app.fragments.ParametersDataEntriesFragment;
import com.example.leuks.roulette.app.fragments.ParametersDataOutFragment;
import com.example.leuks.roulette.app.fragments.ParametersDataTraverseFragment;
import com.example.leuks.roulette.app.fragments.ParametersFragment;
import com.example.leuks.roulette.app.fragments.ResultFragment;
import com.example.leuks.roulette.app.fragments.StartFragment;
import com.example.leuks.roulette.app.fragments.StatisticsFragment;
import com.example.leuks.roulette.commons.bdd.BestTargetItem;
import com.example.leuks.roulette.commons.bdd.DBManager;
import com.example.leuks.roulette.commons.bdd.Target;
import com.example.leuks.roulette.commons.entities.Tuple;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Leuks on 22/11/2016.
 */

public class MainActivity extends OrmLiteBaseActivity<DBManager> {
    public static final int ROAD_START = 0;
    public static final int ROAD_ENTRY_CHOICE = 1;
    public static final int ROAD_PARAMETERS = 2;
    public static final int ROAD_PARAMETERS_ALGORITHM = 3;
    public static final int ROAD_PARAMETERS_ENTRIES = 4;
    public static final int ROAD_ENTRY_MANUAL = 5;
    public static final int ROAD_ENTRY_AUTO = 6;
    public static final int ROAD_RESULT = 7;
    public static final int ROAD_DATA_CHOICE = 8;
    public static final int ROAD_STATISTICS = 10;
    public static final int ROAD_PARAMETERS_CHOICE = 11;
    public static final int ROAD_PARAMETERS_OUT = 12;
    public static final int ROAD_PARAMETERS_TRAVERSE = 13;
    public static final int ROAD_PARAMETERS_SCORE_ONE = 14;
    public static final int ROAD_PARAMETERS_SCORE_TWO = 15;
    public static final int ROAD_HISTORY_CHOICE = 16;
    public static final int ROAD_HISTORY_TARGETING = 17;
    public static final int ROAD_HISTORY_SCORES = 18;
    public static final int ROAD_HISTORY_VALORISATION = 19;

    public static final int NR_SIMPLE_CHANCE = 0;
    public static final int RE_SIMPLE_CHANCE = 1;
    public static final int PR_SIMPLE_CHANCE = 2;
    public static final int IR_SIMPLE_CHANCE = 3;
    public static final int ME_SIMPLE_CHANCE = 4;
    public static final int PE_SIMPLE_CHANCE = 5;
    public static final BlockingQueue<Runnable> sPoolWorkQueueDB =
            new LinkedBlockingQueue<Runnable>(1000000);
    private static final BlockingQueue<Runnable> sPoolWorkQueue =
            new LinkedBlockingQueue<>(100);
    public static ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 128, 150, TimeUnit.SECONDS, sPoolWorkQueue);
    public static ThreadPoolExecutor executorDB = new ThreadPoolExecutor(5, 1000000, 1, TimeUnit.HOURS, sPoolWorkQueueDB);

    public static long dep = 0;
    public static long dep2 = 0;
    public static long dep3 = 0;
    public Handler addTargetHandler;
    public AddMatchingTask addMatchingTask;
    public DBWorkTask addDB;
    private StartFragment startFragment;
    private EntryManualFragment entryFragment;
    private ParametersFragment parametersFragment;
    private ParametersAlgorithmFragment parametersAlgorithmFragment;
    private ParametersDataEntriesFragment parametersDataEntriesFragment;
    private EntryAutoFragment entryAutoFragment;
    private EntryChoiceFragment entryChoiceFragment;
    private ResultFragment resultFragment;
    private DataChoiceFragment dataChoiceFragment;
    private StatisticsFragment statisticsFragment;
    private DataParametersChoiceFragment fragmentDataParametersChoice;
    private ParametersDataOutFragment parametersDataOutFragment;
    private ParametersDataTraverseFragment parametersDataTraverseFragment;
    private MScoreOneParametersFragment mScoreOneParametersFragment;
    private MScoreTwoParametersFragment mScoreTwoParametersFragment;
    private HistoryChoiceFragment historyChoiceFragment;
    private HistoryTargetingFragment historyTargetingFragment;
    private HistoryScoreFraqment historyScoreFraqment;
    private HistoryValorisationFragment historyValorisationFragment;
    private Work work;
    private DataWork dataWork;
    private boolean canBack;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        File f = new File("" + getFilesDir());
        if (!f.exists()) {
            f.mkdirs();
        }
        getHelper().init("" + getFilesDir());

        addMatchingTask = new AddMatchingTask(this);
        addTargetHandler = new Handler() {
            ArrayList<BestTargetItem> btForBd = new ArrayList<>();
            ArrayList<Target> tForDb = new ArrayList<>();
            ArrayList<TargetToUpdate> ttoforBd = new ArrayList<>();
            private int count = 0;

            @Override
            public synchronized void handleMessage(Message msg) {
                UtilObj.ReturnHandlerObject returnHandlerObject = (UtilObj.ReturnHandlerObject) msg.obj;
                int intTab = returnHandlerObject.getIndTab();
                HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> globalTargeting = work.getGlobalTargeting();

                int max = 0;
                for (int o : returnHandlerObject.getExitOccurence().keySet()) {
                    for (BestTargetItem b : returnHandlerObject.getExitOccurence().get(o)) {
                        if (globalTargeting.get(intTab) == null) {
                            ArrayList arrayToPut = new ArrayList();
                            HashMap toPut = new HashMap();
                            toPut.put(o, arrayToPut);
                            globalTargeting.put(intTab, toPut);
                        } else {
                            if (globalTargeting.get(intTab).get(o) == null) {
                                ArrayList arrayToPut = new ArrayList();
                                globalTargeting.get(intTab).put(o, arrayToPut);
                            }
                        }
                        btForBd.add(b);
                        work.getGlobalTargeting().get(intTab).get(o).add(b);
                        if (b.getId() > max) max = b.getId();
                    }
                }
                work.setLastBestId(max);

                max = 0;
                HashMap<Integer, ArrayList<Target>> occurences = returnHandlerObject.getOccurences();

                for (int o : occurences.keySet()) {
                    Target t = occurences.get(o).get(occurences.get(o).size() - 1);
                    if (t.getId() > max) max = t.getId();
                    work.getAllTargetsAsHashMap().get(intTab).get(o).add(t);
                    tForDb.add(t);
                }
                work.setLastId(max);


                final ArrayList<TargetToUpdate> toUpdates = returnHandlerObject.getTArgetsToUpdate();
                ttoforBd.addAll(toUpdates);
                for (TargetToUpdate t : toUpdates) {
                    ArrayList<BestTargetItem> bests = work.getGlobalTargeting().get(t.getIndTab()).get(t.getIndOccu());
                    for (int i = bests.size() - 1; i >= 0; i--) {
                        if (bests.get(i).getTarget().equals(t.getTarget())) {
                            bests.get(i).setSuccess(true);
                        }
                    }
                }

                count++;
                if (count == getHelper().getParameters().getTabsNumber()) {

                    final ArrayList<BestTargetItem> co1 = new ArrayList<>(btForBd);
                    final ArrayList<Target> co2 = new ArrayList<>(tForDb);
                    final ArrayList<TargetToUpdate> co3 = new ArrayList<>(ttoforBd);
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            getHelper().createNewGlobalTargets(co1);
                            getHelper().createNewTargets(co2);
                        }
                    });
                    MainActivity.executorDB.execute(thread);
                    Thread thread2 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            getHelper().updateMassTargets(co3);
                        }
                    });
                    MainActivity.executorDB.execute(thread2);

                    btForBd.clear();
                    tForDb.clear();
                    ttoforBd.clear();
                    count = 0;
                    work.executeTargetingTask();
                }
            }
        };
        addMatchingTask.start();

        addDB = new DBWorkTask(this);
        addDB.start();

        startFragment = new StartFragment();
        entryFragment = new EntryManualFragment();
        parametersFragment = new ParametersFragment();
        parametersAlgorithmFragment = new ParametersAlgorithmFragment();
        parametersDataEntriesFragment = new ParametersDataEntriesFragment();
        entryAutoFragment = new EntryAutoFragment();
        entryChoiceFragment = new EntryChoiceFragment();
        resultFragment = new ResultFragment();
        dataChoiceFragment = new DataChoiceFragment();
        statisticsFragment = new StatisticsFragment();
        fragmentDataParametersChoice = new DataParametersChoiceFragment();
        parametersDataOutFragment = new ParametersDataOutFragment();
        parametersDataTraverseFragment = new ParametersDataTraverseFragment();
        mScoreOneParametersFragment = new MScoreOneParametersFragment();
        mScoreTwoParametersFragment = new MScoreTwoParametersFragment();
        historyChoiceFragment = new HistoryChoiceFragment();
        canBack = true;
        historyTargetingFragment = new HistoryTargetingFragment();
        historyScoreFraqment = new HistoryScoreFraqment();
        historyValorisationFragment = new HistoryValorisationFragment();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        setContentView(R.layout.activity_main_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        show(ROAD_START, false);

        if (getHelper().getAllTargetsAsHashMap().isEmpty()) {
            removeMemory();
        }
    }

    /**
     * Fragments Transaction method
     *
     * @param direction
     * @param back
     */
    public void show(int direction, boolean back) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        switch (direction) {
            case ROAD_START:
                fragmentTransaction.replace(R.id.frameLayoutMain, startFragment);
                break;
            case ROAD_ENTRY_CHOICE:
                fragmentTransaction.replace(R.id.frameLayoutMain, entryChoiceFragment);
                break;
            case ROAD_PARAMETERS:
                fragmentTransaction.replace(R.id.frameLayoutMain, parametersFragment);
                break;
            case ROAD_PARAMETERS_ALGORITHM:
                fragmentTransaction.replace(R.id.frameLayoutMain, parametersAlgorithmFragment);
                break;
            case ROAD_PARAMETERS_ENTRIES:
                fragmentTransaction.replace(R.id.frameLayoutMain, parametersDataEntriesFragment);
                break;
            case ROAD_ENTRY_MANUAL:
                fragmentTransaction.replace(R.id.frameLayoutMain, entryFragment);
                break;
            case ROAD_ENTRY_AUTO:
                fragmentTransaction.replace(R.id.frameLayoutMain, entryAutoFragment);
                break;
            case ROAD_RESULT:
                fragmentTransaction.replace(R.id.frameLayoutMain, resultFragment);
                canBack = false;
                break;
            case ROAD_DATA_CHOICE:
                fragmentTransaction.replace(R.id.frameLayoutMain, dataChoiceFragment);
                progressDialog.dismiss();
                break;
            case ROAD_STATISTICS:
                fragmentTransaction.replace(R.id.frameLayoutMain, statisticsFragment);
                break;
            case ROAD_PARAMETERS_CHOICE:
                fragmentTransaction.replace(R.id.frameLayoutMain, fragmentDataParametersChoice);
                break;
            case ROAD_PARAMETERS_OUT:
                fragmentTransaction.replace(R.id.frameLayoutMain, parametersDataOutFragment);
                break;
            case ROAD_PARAMETERS_TRAVERSE:
                fragmentTransaction.replace(R.id.frameLayoutMain, parametersDataTraverseFragment);
                break;
            case ROAD_PARAMETERS_SCORE_ONE:
                fragmentTransaction.replace(R.id.frameLayoutMain, mScoreOneParametersFragment);
                break;
            case ROAD_PARAMETERS_SCORE_TWO:
                fragmentTransaction.replace(R.id.frameLayoutMain, mScoreTwoParametersFragment);
                break;
            case ROAD_HISTORY_CHOICE:
                fragmentTransaction.replace(R.id.frameLayoutMain, historyChoiceFragment);
                break;
            case ROAD_HISTORY_TARGETING:
                fragmentTransaction.replace(R.id.frameLayoutMain, historyTargetingFragment);
                break;
            case ROAD_HISTORY_SCORES:
                fragmentTransaction.replace(R.id.frameLayoutMain, historyScoreFraqment);
                break;
            case ROAD_HISTORY_VALORISATION:
                fragmentTransaction.replace(R.id.frameLayoutMain, historyValorisationFragment);
                break;
        }

        if (back) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void registerDB(final Object o, final int action, final int arg) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                Message m = addDB.dbHandler.obtainMessage();
                m.obj = o;
                m.arg1 = action;
                m.arg2 = arg;
                addDB.dbHandler.dispatchMessage(m);
            }
        });
        executorDB.execute(t);
    }

    public boolean DBAvalaible() {
        return MainActivity.executorDB.getTaskCount() + MainActivity.sPoolWorkQueueDB.size() - MainActivity.executorDB.getCompletedTaskCount() == 0;
    }

    @Override
    public void onBackPressed() {
        if (canBack) getFragmentManager().popBackStackImmediate();
    }

    public void reset() {
        canBack = true;
        work.init();
    }

    public void removeMemory() {
        getHelper().removeMemory();
        ArrayList<Tuple<Integer, Integer>> datatToPut = new ArrayList<>();
        datatToPut.add(new Tuple<>(13, IR_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(1, ME_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(25, NR_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(18, RE_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(18, PE_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(25, ME_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(15, IR_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(27, IR_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(3, IR_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(1, ME_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(1, IR_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(16, PE_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(28, RE_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(4, PR_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(13, ME_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(10, RE_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(27, RE_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(20, PR_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(19, NR_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(20, ME_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(16, PR_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(35, ME_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(25, PR_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(0, RE_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(3, ME_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(24, PR_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(21, PE_SIMPLE_CHANCE));
        datatToPut.add(new Tuple<>(10, RE_SIMPLE_CHANCE));

        work = null;

        //OLD WorkManager workManager = new WorkManager(this, datatToPut.size(), datatToPut);
        WorkManager workManager = new WorkManager(this, 50, datatToPut);
        workManager.start();
    }

    //GS
    public StartFragment getStartFragment() {
        return startFragment;
    }

    public EntryManualFragment getEntryFragment() {
        return entryFragment;
    }

    public ParametersFragment getParametersFragment() {
        return parametersFragment;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public EntryAutoFragment getEntryAutoFragment() {
        return entryAutoFragment;
    }

    public Work getWork(WorkManager workManager) {
        if (work == null) {
            Work work = new Work(this, workManager);
            this.work = work;
            return work;
        } else {
            if (workManager == null || !workManager.equals(work.getWorkManager())) {
                work.setWorkManager(workManager);
            }
            return work;
        }
    }

    public DataWork getDataWork(boolean isNew) {
        if (dataWork == null || isNew) {
            DataWork work = new DataWork(this);
            this.dataWork = work;
            return work;
        } else return dataWork;
    }
}
