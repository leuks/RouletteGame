package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.app.classes.Work;
import com.example.leuks.roulette.commons.bdd.Parameters;
import com.example.leuks.roulette.commons.bdd.ValorisationItem;
import com.example.leuks.roulette.commons.entities.InfiniteTuple;

/**
 * Created by leuks on 28/12/2016.
 */

public class ResultFragment extends Fragment {
    private MainActivity mainActivity;
    private TableLayout tableLayout;
    private TextView oldEntries;

    public static String findSimpleChanceFromExit(String exit) {
        if (exit.equals(String.valueOf((double) MainActivity.NR_SIMPLE_CHANCE))) {
            return "noir";
        } else if (exit.equals(String.valueOf((double) MainActivity.RE_SIMPLE_CHANCE))) {
            return "rouge";
        } else if (exit.equals(String.valueOf((double) MainActivity.PR_SIMPLE_CHANCE))) {
            return "pair";
        } else if (exit.equals(String.valueOf((double) MainActivity.IR_SIMPLE_CHANCE))) {
            return "impair";
        } else if (exit.equals(String.valueOf((double) MainActivity.ME_SIMPLE_CHANCE))) {
            return "manque";
        } else if (exit.equals(String.valueOf((double) MainActivity.PE_SIMPLE_CHANCE))) {
            return "passe";
        } else return exit;
    }

    public static int findExitFromSimpleChance(String sp) {
        if (sp.equals("noir")) return 0;
        else if (sp.equals("rouge")) return 1;
        else if (sp.equals("pair")) return 2;
        else if (sp.equals("impair")) return 3;
        else if (sp.equals("manque")) return 4;
        else if (sp.equals("passe")) return 5;
        else return -1;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.fragment_result_layout, container, false);

        //components
        Button validateResultButton = (Button) thisView.findViewById(R.id.button_validate_result);
        oldEntries = (TextView) thisView.findViewById(R.id.old_entries);
        tableLayout = (TableLayout) thisView.findViewById(R.id.table_layout_result);

        //listeners
        validateResultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.reset();
                mainActivity.getFragmentManager().popBackStackImmediate();
            }
        });

        return thisView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Work work = mainActivity.getWork(null);
        Parameters parameters = mainActivity.getHelper().getParameters();

        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");

        //components
        InfiniteTuple oldEntrys = mainActivity.getEntryFragment().getOldEntrys();
        if (oldEntrys.isEmpty()) oldEntries.setText("Aucune donnée");
        else oldEntries.setText(String.valueOf(oldEntrys.toString(true)));

        TableRow header = new TableRow(mainActivity);
        header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 5f));

        TextView headerShotTextView = new TextView(mainActivity);
        headerShotTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerShotTextView.setText("Tir");
        headerShotTextView.setGravity(Gravity.CENTER);
        headerShotTextView.setPadding(10, 10, 10, 10);
        headerShotTextView.setTypeface(fontButton);

        TextView headerTargetTextView = new TextView(mainActivity);
        headerTargetTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerTargetTextView.setText("Cible");
        headerTargetTextView.setGravity(Gravity.CENTER);
        headerTargetTextView.setPadding(10, 10, 10, 10);
        headerTargetTextView.setTypeface(fontButton);

        TextView headerValorisationTextView = new TextView(mainActivity);
        headerValorisationTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerValorisationTextView.setText("Valorisation");
        headerValorisationTextView.setGravity(Gravity.CENTER);
        headerValorisationTextView.setPadding(10, 10, 10, 10);
        headerValorisationTextView.setTypeface(fontButton);

        TextView headerScoreTextView = new TextView(mainActivity);
        headerScoreTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerScoreTextView.setText("Score");
        headerScoreTextView.setGravity(Gravity.CENTER);
        headerScoreTextView.setPadding(10, 10, 10, 10);
        headerScoreTextView.setTypeface(fontButton);

        TextView headerDnbTextView = new TextView(mainActivity);
        headerDnbTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        headerDnbTextView.setText("Dénombrement");
        headerDnbTextView.setGravity(Gravity.CENTER);
        headerDnbTextView.setPadding(10, 10, 10, 10);
        headerDnbTextView.setTypeface(fontButton);

        header.addView(headerShotTextView);
        header.addView(headerTargetTextView);
        header.addView(headerValorisationTextView);
        header.addView(headerScoreTextView);
        header.addView(headerDnbTextView);
        headerShotTextView.setTypeface(fontButton);

        tableLayout.addView(header);

        ValorisationItem valorisationItem = work.getValorisations();
        boolean first = true;
        for (int o = 0; o < parameters.getOccurencesNumber(); o++) {
            if (valorisationItem.getValuesScoreOne().get(o) != null && !valorisationItem.getValuesScoreOne().get(o).isEmpty() && valorisationItem.getValorisation().get(o) != 0) {
                TableRow tableRow = new TableRow(mainActivity);
                tableRow.setGravity(Gravity.CENTER);
                tableRow.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 5f));

                TextView shot = new TextView(mainActivity);
                shot.setGravity(Gravity.CENTER);
                shot.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
                if (first) {
                    shot.setText(String.valueOf(valorisationItem.getBestTargetItem().get(o).getTarget().getShot()));
                    first = false;
                }

                TextView target = new TextView(mainActivity);
                target.setText(findSimpleChanceFromExit(valorisationItem.getFinalExitTarget().get(o)));
                target.setGravity(Gravity.CENTER);
                target.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));

                TextView valorisation = new TextView(mainActivity);
                valorisation.setText(String.valueOf(valorisationItem.getValorisation().get(o)));
                valorisation.setGravity(Gravity.CENTER);
                valorisation.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));

                TextView score = new TextView(mainActivity);
                score.setText("1");
                score.setGravity(Gravity.CENTER);
                score.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));

                TextView denombrement = new TextView(mainActivity);
                denombrement.setText(valorisationItem.getStringValueScoreOne(o));
                denombrement.setGravity(Gravity.CENTER);
                denombrement.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));


                tableRow.addView(shot);
                tableRow.addView(target);
                tableRow.addView(valorisation);
                tableRow.addView(score);
                tableRow.addView(denombrement);
                tableLayout.addView(tableRow);
            }
            if (valorisationItem.getValuesScoreTwo().get(o) != null && !valorisationItem.getValuesScoreTwo().get(o).isEmpty() && valorisationItem.getValorisation().get(o) != 0) {
                TableRow tableRow = new TableRow(mainActivity);
                tableRow.setGravity(Gravity.CENTER);
                tableRow.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 5f));

                TextView shot = new TextView(mainActivity);
                shot.setGravity(Gravity.CENTER);
                shot.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));

                TextView target = new TextView(mainActivity);
                target.setGravity(Gravity.CENTER);
                target.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));

                TextView valorisation = new TextView(mainActivity);
                valorisation.setGravity(Gravity.CENTER);
                valorisation.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));

                TextView score = new TextView(mainActivity);
                score.setText("2");
                score.setGravity(Gravity.CENTER);
                score.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));

                TextView denombrement = new TextView(mainActivity);
                denombrement.setText(valorisationItem.getStringValueScoreTwo(o));
                denombrement.setGravity(Gravity.CENTER);
                denombrement.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));

                if (valorisationItem.getValuesScoreOne().get(o) == null || !(valorisationItem.getValuesScoreOne().get(o).size() > 0)) {
                    if (first) {
                        shot.setText(String.valueOf(valorisationItem.getBestTargetItem().get(o).getTarget().getShot()));
                        target.setText(findSimpleChanceFromExit(valorisationItem.getFinalExitTarget().get(o)));
                        valorisation.setText(String.valueOf(valorisationItem.getValorisation().get(o)));
                        first = false;
                    }
                }

                tableRow.addView(shot);
                tableRow.addView(target);
                tableRow.addView(valorisation);
                tableRow.addView(score);
                tableRow.addView(denombrement);
                tableLayout.addView(tableRow);
            }
        }
    }
}
