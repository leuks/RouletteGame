package com.example.leuks.roulette.app.classes.tasks.action;

import android.os.AsyncTask;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.app.classes.Work;
import com.example.leuks.roulette.commons.bdd.Parameters;
import com.example.leuks.roulette.commons.bdd.ScoreOneItem;
import com.example.leuks.roulette.commons.bdd.ScoreTwoItem;
import com.example.leuks.roulette.commons.bdd.ValorisationItem;
import com.example.leuks.roulette.commons.entities.Tuple;

import java.util.HashMap;

/**
 * Created by leuks on 28/12/2016.
 */

public class ValorisationTask extends AsyncTask<Void, Void, ValorisationItem> {
    private MainActivity mainActivity;
    private HashMap<Integer, ScoreOneItem> scoreOnes;
    private HashMap<Integer, ScoreTwoItem> scoreTwos;
    private Work work;
    private Parameters parameters;

    public ValorisationTask(MainActivity mainActivity, Work work, HashMap<Integer, ScoreOneItem> scoreOnes, HashMap<Integer, ScoreTwoItem> scoreTwos) {
        this.mainActivity = mainActivity;
        this.scoreOnes = scoreOnes;
        this.scoreTwos = scoreTwos;
        this.work = work;
        this.parameters = mainActivity.getHelper().getParameters();
    }

    @Override
    protected ValorisationItem doInBackground(Void... voids) {
        if (isCancelled()) return null;
        Tuple<Boolean, Boolean> scores = parameters.getScores();

        ValorisationItem valorisationItem = new ValorisationItem();
        for (int i = 0; i < parameters.getOccurencesNumber(); i++) {

            if (scores.getArg1()) {
                ScoreOneItem scoreOneItem = scoreOnes.get(i);

                for (int f = ScoreOneItem.F1; f <= ScoreOneItem.F4_2; f++) {
                    Tuple<Integer, Integer> function = scoreOneItem.getFunctions().get(f);
                    if (function != null) { // column test
                        if (function.getArg2() != null) {
                            Integer valueMScoreOne = parameters.getValuemScoreOneOf(function.toString("."));
                            if (valueMScoreOne != null && !valueMScoreOne.equals(0)) {
                                if (valorisationItem.getValuesScoreOne().get(i) == null) {
                                    valorisationItem.addBestTargetItem(i, scoreOneItem.getBestTargetItem());
                                    valorisationItem.addFinalTarget(i, String.valueOf(parameters.getExitMatching().get(scoreOneItem.getBestTargetItem().getSimpleChance()).get(i)));
                                }
                                valorisationItem.addValue(ValorisationItem.SCORE1, i, function.toString("."), valueMScoreOne);
                            }
                        }
                    }
                }
            }

            if (scores.getArg2()) {
                ScoreTwoItem scoreTwoItem = scoreTwos.get(i);
                Tuple<Integer, Integer> function = (Tuple<Integer, Integer>) scoreTwoItem.getFunctions().get(ScoreTwoItem.F).getArg2();
                Integer valueMScoreTwo = parameters.getValuemScoreTwoOf(function.toString("."));

                if (valueMScoreTwo != null && !valueMScoreTwo.equals(0)) {
                    if (valorisationItem.getValuesScoreTwo().get(i) == null) {
                        valorisationItem.addBestTargetItem(i, scoreTwoItem.getBestTargetItem());
                        valorisationItem.addFinalTarget(i, String.valueOf(parameters.getExitMatching().get(scoreTwoItem.getE()).get(i)));
//                           //lastversion :  valorisationItem.addFinalTarget(i, String.valueOf(parameters.getExitMatching().get(scoreTwoItem.getBestTargetItem().getSimpleChance()).get(i)));
                    }
                    valorisationItem.addValue(ValorisationItem.SCORE2, i, function.toString("."), valueMScoreTwo);
                }
            }
        }
        return valorisationItem;
    }

    @Override
    protected void onPostExecute(ValorisationItem item) {
        super.onPostExecute(item);
        mainActivity.registerDB(item, 0, 0);
        work.setValorisations(item);
        work.close();
        cancel(true);
    }
}
