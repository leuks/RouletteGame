package com.example.leuks.roulette.commons.entities;

import android.content.Context;
import android.widget.Toast;

import java.util.Random;

/**
 * Created by Leuks on 22/11/2016.
 */

/**
 * Class of utilities
 */
public class Tools {

    /**
     * Return a random int beetween min and max bounds
     *
     * @param min
     * @param max
     * @return a random int beetween min and max bounds
     */
    public static int randomIntWithInclusiveBounds(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }

    public static void makeText(Context context, String text, boolean longDuration) {
        Toast.makeText(context, text, longDuration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
    }
}
