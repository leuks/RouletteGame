package com.example.leuks.roulette.commons.bdd;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by leuks on 10/03/2017.
 */

@DatabaseTable(tableName = "ProfilItem")
public class ProfilItem {
    @DatabaseField(id = true)
    private String name;
    @DatabaseField(foreign = true, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 5)
    private Parameters parameters;
    @DatabaseField
    private boolean activate;

    public ProfilItem() {
    }

    public ProfilItem(String name, Parameters parameters, boolean activate) {
        this.name = name;
        this.parameters = new Parameters(parameters);
        this.activate = activate;
    }

    @Override
    public boolean equals(Object obj) {
        ProfilItem item = (ProfilItem) obj;
        return name.equals(item.name);
    }

    //GS
    public String getName() {
        return name;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public boolean isActivate() {
        return activate;
    }

    public void setActivate(boolean acti) {
        activate = acti;
    }
}
