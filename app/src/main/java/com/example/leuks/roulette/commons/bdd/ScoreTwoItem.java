package com.example.leuks.roulette.commons.bdd;

import com.example.leuks.roulette.commons.entities.Tuple;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.HashMap;

/**
 * Created by Leuks on 23/12/2016.
 */

@DatabaseTable(tableName = "ScoreTwoItem")
public class ScoreTwoItem {
    public static final int A = 0;
    public static final int B = 1;
    public static final int C = 2;
    public static final int D = 3;
    public static final int F = 4;

    @DatabaseField(id = true)
    private int id;
    @DatabaseField(foreign = true, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 5)
    private BestTargetItem bestTargetItem;
    @DatabaseField
    private Integer e;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<Integer, Tuple> functions;

    public ScoreTwoItem(BestTargetItem bestTargetItem) {
        this.bestTargetItem = bestTargetItem;
        functions = new HashMap<>();
        e = null;
    }

    public ScoreTwoItem() {
    }

    //GS
    public BestTargetItem getBestTargetItem() {
        return bestTargetItem;
    }

    public HashMap<Integer, Tuple> getFunctions() {
        return functions;
    }

    public Integer getE() {
        return e;
    }

    public void setE(Integer e) {
        this.e = e;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
