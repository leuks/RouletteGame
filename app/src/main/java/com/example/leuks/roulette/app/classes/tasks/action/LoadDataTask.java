package com.example.leuks.roulette.app.classes.tasks.action;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.app.classes.DataWork;

/**
 * Created by Leuks on 31/01/2017.
 */

public class LoadDataTask extends AsyncTask<Void, Void, Void> {
    private MainActivity mainActivity;
    private DataWork dataWork;

    public LoadDataTask(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        dataWork = mainActivity.getDataWork(false);
    }

    @Override
    protected Void doInBackground(Void... params) {
        //bind all data into the current work
        if (isCancelled()) return null;
        dataWork.setGlobalTargeting(mainActivity.getHelper().getAllBestTargetItemsAsHashMap());
        dataWork.setScoreOnes(mainActivity.getHelper().getAllScoreOnes());
        dataWork.setScoreTwos(mainActivity.getHelper().getAllScoreTwos());
        dataWork.setValorisations(mainActivity.getHelper().getAllValorisations());
        dataWork.setStatItems(mainActivity.getHelper().getAllStatItem());
        return null;
    }

    @Override
    protected void onPreExecute() {
        ProgressDialog progressDialog = mainActivity.getProgressDialog();
        progressDialog.setTitle("Chargement des données");
        progressDialog.setMessage("");
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mainActivity.getDataWork(false).close();
        cancel(true);
    }
}
