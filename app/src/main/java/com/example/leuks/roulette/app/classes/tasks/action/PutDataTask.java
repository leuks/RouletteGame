package com.example.leuks.roulette.app.classes.tasks.action;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.app.classes.DataWork;
import com.example.leuks.roulette.app.fragments.ResultFragment;
import com.example.leuks.roulette.commons.bdd.BestTargetItem;
import com.example.leuks.roulette.commons.bdd.Parameters;
import com.example.leuks.roulette.commons.bdd.ScoreOneItem;
import com.example.leuks.roulette.commons.bdd.ScoreTwoItem;
import com.example.leuks.roulette.commons.bdd.ValorisationItem;
import com.example.leuks.roulette.commons.entities.Tuple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

/**
 * Created by Leuks on 01/02/2017.
 */

/**
 * Build data views, view to build depends of mode variable
 */
public class PutDataTask extends AsyncTask<Void, Object, Void> {
    public static final int MODE_TARGETING = 0;
    public static final int MODE_SCORES = 1;
    public static final int MODE_VALORISATION = 2;

    private MainActivity mainActivity;
    private LinearLayout layoutToPut;
    private int mode;
    private boolean each;

    public PutDataTask(MainActivity mainActivity, LinearLayout layoutToPut, int mode, boolean each) {
        this.mainActivity = mainActivity;
        this.layoutToPut = layoutToPut;
        this.mode = mode;
        this.each = each;
    }

    @Override
    protected Void doInBackground(Void... params) {
        DataWork dataWork = mainActivity.getDataWork(false);
        if (mode == MODE_TARGETING) {
            HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> globalTargeting = dataWork.getGlobalTargeting();
            publishProgress(globalTargeting);
        } else if (mode == MODE_SCORES) {
            if (!each) {
                HashMap<Integer, ArrayList<ScoreOneItem>> scoreOnes = dataWork.getScoreOnes();
                publishProgress(scoreOnes);
            } else {
                HashMap<Integer, ArrayList<ScoreTwoItem>> scoreTwos = dataWork.getScoreTwos();
                publishProgress(scoreTwos);
            }
        } else {
            ArrayList<ValorisationItem> valorisations = dataWork.getValorisations();
            publishProgress(valorisations);
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ProgressDialog progressDialog = mainActivity.getProgressDialog();
        if (mode == MODE_TARGETING) {
            progressDialog.setTitle("Création de l'historique");
            progressDialog.setMessage("Ciblage");
        } else if (mode == MODE_SCORES) {
            progressDialog.setTitle("Création de l'historique");
            progressDialog.setMessage("Scores");
        } else {
            progressDialog.setTitle("Création de l'historique");
            progressDialog.setMessage("Valorisation");
        }
        progressDialog.show();
    }

    @Override
    protected void onProgressUpdate(Object... values) {
        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        if (mode == MODE_TARGETING) {
            HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> globalTargeting = (HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>>) values[0];
            for (int t : new TreeSet<>(globalTargeting.keySet())) {
                HashMap<Integer, ArrayList<BestTargetItem>> tab = globalTargeting.get(t);

                //components
                TextView tabNumberTextViewShow = new TextView(mainActivity);
                LinearLayout linearLayoutTab = new LinearLayout(mainActivity);
                LinearLayout linearLayoutTargeting = new LinearLayout(mainActivity);
                HorizontalScrollView tabScrollView = new HorizontalScrollView(mainActivity);

                linearLayoutTargeting.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                linearLayoutTargeting.setOrientation(LinearLayout.VERTICAL);

                linearLayoutTab.setPadding(0, 0, 0, 150);
                linearLayoutTab.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 0.9f));

                tabNumberTextViewShow.setTypeface(fontButton);
                tabNumberTextViewShow.setGravity(Gravity.CENTER);
                tabNumberTextViewShow.setText("Tableau " + (t + 1));
                tabNumberTextViewShow.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 0.1f));

                for (int o = 0; o < tab.size(); o++) {
                    ArrayList<BestTargetItem> occurence = tab.get(o);

                    //components
                    TextView occurenceNumberTextViewShow = new TextView(mainActivity);
                    TableLayout tableLayout = new TableLayout(mainActivity);
                    TableRow header = new TableRow(mainActivity);
                    ScrollView occurenceScrollView = new ScrollView(mainActivity);

                    header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));
                    TextView headerShotTextView = new TextView(mainActivity);
                    headerShotTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headerShotTextView.setText("Tir");
                    headerShotTextView.setGravity(Gravity.CENTER);
                    headerShotTextView.setPadding(10, 10, 10, 10);

                    TextView headerMatchTextView = new TextView(mainActivity);
                    headerMatchTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headerMatchTextView.setText("Match");
                    headerMatchTextView.setGravity(Gravity.CENTER);
                    headerMatchTextView.setPadding(10, 10, 10, 10);

                    TextView headertargetTextView = new TextView(mainActivity);
                    headertargetTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headertargetTextView.setText("Cible");
                    headertargetTextView.setGravity(Gravity.CENTER);
                    headertargetTextView.setPadding(10, 10, 10, 10);

                    TextView headerDnbTextView = new TextView(mainActivity);
                    headerDnbTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headerDnbTextView.setText("Dénombrement");
                    headerDnbTextView.setGravity(Gravity.CENTER);
                    headerDnbTextView.setPadding(10, 10, 10, 10);

                    header.addView(headerShotTextView);
                    header.addView(headerMatchTextView);
                    header.addView(headertargetTextView);
                    header.addView(headerDnbTextView);

                    tableLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 0.9f));
                    tableLayout.addView(header);

                    occurenceNumberTextViewShow.setTypeface(fontButton);
                    occurenceNumberTextViewShow.setText("Occurence " + (o + 1));
                    occurenceNumberTextViewShow.setGravity(Gravity.CENTER);
                    occurenceNumberTextViewShow.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 0.1f));

                    for (BestTargetItem bestTargetItem : occurence) {
                        TableRow rowOccurence = new TableRow(mainActivity);
                        rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 4f));

                        TextView twShot = new TextView(mainActivity);
                        twShot.setPadding(10, 10, 10, 10);
                        twShot.setGravity(Gravity.CENTER);
                        twShot.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twMatch = new TextView(mainActivity);
                        twMatch.setPadding(10, 10, 10, 10);
                        twMatch.setGravity(Gravity.CENTER);
                        twMatch.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twTarget = new TextView(mainActivity);
                        twTarget.setPadding(10, 10, 10, 10);
                        twTarget.setGravity(Gravity.CENTER);
                        twTarget.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twDnb = new TextView(mainActivity);
                        twDnb.setPadding(10, 10, 10, 10);
                        twDnb.setGravity(Gravity.CENTER);
                        twDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        twShot.setText(String.valueOf(bestTargetItem.getTarget().getShot()));
                        twMatch.setText(String.valueOf(bestTargetItem.getTarget().getMatch()));
                        twTarget.setText(ResultFragment.findSimpleChanceFromExit(String.valueOf((double) bestTargetItem.getSimpleChance())));
                        twDnb.setText(String.valueOf(bestTargetItem.getDenombrement()));

                        rowOccurence.addView(twShot);
                        rowOccurence.addView(twMatch);
                        rowOccurence.addView(twTarget);
                        rowOccurence.addView(twDnb);

                        if (bestTargetItem.isSuccess()) {
                            rowOccurence.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorGreenSuccess));
                        } else {
                            rowOccurence.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorRedLoss));
                        }

                        tableLayout.addView(rowOccurence);
                    }

                    LinearLayout linearLayoutOccurence = new LinearLayout(mainActivity);
                    linearLayoutOccurence.setPadding(10, 50, 50, 10);
                    linearLayoutOccurence.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    linearLayoutOccurence.setOrientation(LinearLayout.VERTICAL);
                    linearLayoutOccurence.addView(occurenceNumberTextViewShow);
                    linearLayoutOccurence.addView(tableLayout);
                    occurenceScrollView.addView(linearLayoutOccurence);
                    linearLayoutTab.addView(occurenceScrollView);
                }

                linearLayoutTargeting.addView(tabNumberTextViewShow);
                tabScrollView.addView(linearLayoutTab);
                linearLayoutTargeting.addView(tabScrollView);
                layoutToPut.addView(linearLayoutTargeting);
            }
        } else if (mode == MODE_SCORES) {
            if (!each) {
                HashMap<Integer, ArrayList<ScoreOneItem>> scoreOnes = (HashMap<Integer, ArrayList<ScoreOneItem>>) values[0];
                for (int i : new TreeSet<>(scoreOnes.keySet())) {
                    ArrayList<ScoreOneItem> scoreOneItemsOfOccurence = scoreOnes.get(i);

                    //components
                    TextView occurenceNumberTextViewShow = new TextView(mainActivity);
                    TableLayout tableLayout = new TableLayout(mainActivity);
                    TableRow header = new TableRow(mainActivity);

                    header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 11f));
                    TextView headerShotTextView = new TextView(mainActivity);
                    headerShotTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headerShotTextView.setText("Tir");
                    headerShotTextView.setGravity(Gravity.CENTER);
                    headerShotTextView.setPadding(10, 10, 10, 10);

                    TextView headerDnbTextView = new TextView(mainActivity);
                    headerDnbTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headerDnbTextView.setText("Match");
                    headerDnbTextView.setGravity(Gravity.CENTER);
                    headerDnbTextView.setPadding(10, 10, 10, 10);

                    TextView headerMatriceTextView = new TextView(mainActivity);
                    headerMatriceTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headerMatriceTextView.setText("Matrice");
                    headerMatriceTextView.setGravity(Gravity.CENTER);
                    headerMatriceTextView.setPadding(10, 10, 10, 10);

                    TextView headereBaseTextView = new TextView(mainActivity);
                    headereBaseTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereBaseTextView.setText("Base");
                    headereBaseTextView.setGravity(Gravity.CENTER);
                    headereBaseTextView.setPadding(10, 10, 10, 10);

                    TextView headereF1TextView = new TextView(mainActivity);
                    headereF1TextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereF1TextView.setText("F1");
                    headereF1TextView.setGravity(Gravity.CENTER);
                    headereF1TextView.setPadding(10, 10, 10, 10);

                    TextView headereF21TextView = new TextView(mainActivity);
                    headereF21TextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereF21TextView.setText("F2");
                    headereF21TextView.setGravity(Gravity.CENTER);
                    headereF21TextView.setPadding(10, 10, 10, 10);

                    TextView headereF22TextView = new TextView(mainActivity);
                    headereF22TextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereF22TextView.setText("F2");
                    headereF22TextView.setGravity(Gravity.CENTER);
                    headereF22TextView.setPadding(10, 10, 10, 10);

                    TextView headereF31TextView = new TextView(mainActivity);
                    headereF31TextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereF31TextView.setText("F3");
                    headereF31TextView.setGravity(Gravity.CENTER);
                    headereF31TextView.setPadding(10, 10, 10, 10);

                    TextView headereF32TextView = new TextView(mainActivity);
                    headereF32TextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereF32TextView.setText("F3");
                    headereF32TextView.setGravity(Gravity.CENTER);
                    headereF32TextView.setPadding(10, 10, 10, 10);

                    TextView headereF41TextView = new TextView(mainActivity);
                    headereF41TextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereF41TextView.setText("F4");
                    headereF41TextView.setGravity(Gravity.CENTER);
                    headereF41TextView.setPadding(10, 10, 10, 10);

                    TextView headereF42TextView = new TextView(mainActivity);
                    headereF42TextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereF42TextView.setText("F4");
                    headereF42TextView.setGravity(Gravity.CENTER);
                    headereF42TextView.setPadding(10, 10, 10, 10);

                    header.addView(headerShotTextView);
                    header.addView(headerDnbTextView);
                    header.addView(headerMatriceTextView);
                    header.addView(headereBaseTextView);
                    header.addView(headereF1TextView);
                    header.addView(headereF21TextView);
                    header.addView(headereF22TextView);
                    header.addView(headereF31TextView);
                    header.addView(headereF32TextView);
                    header.addView(headereF41TextView);
                    header.addView(headereF42TextView);

                    tableLayout.addView(header);

                    occurenceNumberTextViewShow.setTypeface(fontButton);
                    occurenceNumberTextViewShow.setText("Occurence " + (i + 1));
                    occurenceNumberTextViewShow.setGravity(Gravity.CENTER);

                    for (ScoreOneItem scoreOneItem : scoreOneItemsOfOccurence) {
                        TableRow rowOccurence = new TableRow(mainActivity);
                        rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 11f));

                        TextView twShot = new TextView(mainActivity);
                        twShot.setPadding(10, 10, 10, 10);
                        twShot.setGravity(Gravity.CENTER);
                        twShot.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twMatch = new TextView(mainActivity);
                        twMatch.setPadding(10, 10, 10, 10);
                        twMatch.setGravity(Gravity.CENTER);
                        twMatch.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twMatrice = new TextView(mainActivity);
                        twMatrice.setPadding(10, 10, 10, 10);
                        twMatrice.setGravity(Gravity.CENTER);
                        twMatrice.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twBase = new TextView(mainActivity);
                        twBase.setPadding(10, 10, 10, 10);
                        twBase.setGravity(Gravity.CENTER);
                        twBase.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView tw1 = new TextView(mainActivity);
                        tw1.setPadding(10, 10, 10, 10);
                        tw1.setGravity(Gravity.CENTER);
                        tw1.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView tw21 = new TextView(mainActivity);
                        tw21.setPadding(10, 10, 10, 10);
                        tw21.setGravity(Gravity.CENTER);
                        tw21.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView tw22 = new TextView(mainActivity);
                        tw22.setPadding(10, 10, 10, 10);
                        tw22.setGravity(Gravity.CENTER);
                        tw22.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView tw31 = new TextView(mainActivity);
                        tw31.setPadding(10, 10, 10, 10);
                        tw31.setGravity(Gravity.CENTER);
                        tw31.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView tw32 = new TextView(mainActivity);
                        tw32.setPadding(10, 10, 10, 10);
                        tw32.setGravity(Gravity.CENTER);
                        tw32.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView tw41 = new TextView(mainActivity);
                        tw41.setPadding(10, 10, 10, 10);
                        tw41.setGravity(Gravity.CENTER);
                        tw41.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView tw42 = new TextView(mainActivity);
                        tw42.setPadding(10, 10, 10, 10);
                        tw42.setGravity(Gravity.CENTER);
                        tw42.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        twShot.setText(String.valueOf(scoreOneItem.getBestTargetItem().getTarget().getShot()));
                        twMatch.setText(String.valueOf(scoreOneItem.getBestTargetItem().getTarget().getMatch()));
                        if (scoreOneItem.isMatrice()) {
                            String sp = ResultFragment.findSimpleChanceFromExit(String.valueOf((double) scoreOneItem.getBestTargetItem().getSimpleChance()));

                            if (scoreOneItem.getProximity() != null) {
                                if (scoreOneItem.getProximity() == ScoreOneItem.PROCHE) sp += ",";
                                else sp += "'";
                            }

                            twMatrice.setText(sp);
                            twBase.setText("");


                        } else {
                            String sp = ResultFragment.findSimpleChanceFromExit(String.valueOf((double) scoreOneItem.getBestTargetItem().getSimpleChance()));

                            if (scoreOneItem.getProximity() != null) {
                                if (scoreOneItem.getProximity() == ScoreOneItem.PROCHE) sp += ",";
                                else sp += "'";
                            }
                            twMatrice.setText("");
                            twBase.setText(sp);
                        }

                        Tuple<Integer, Integer> f1 = scoreOneItem.getFunctions().get(ScoreOneItem.F1);
                        String sF1 = "";
                        if (f1.getArg1() != null) sF1 += f1.getArg1();
                        if (f1.getArg2() != null) sF1 += "." + f1.getArg2();
                        tw1.setText(sF1);

                        Tuple<Integer, Integer> f21 = scoreOneItem.getFunctions().get(ScoreOneItem.F2_1);
                        String sF21 = "";
                        if (f21 != null) {
                            if (f21.getArg1() != null) sF21 += f21.getArg1();
                            if (f21.getArg2() != null) sF21 += "." + f21.getArg2();
                        }
                        tw21.setText(sF21);

                        Tuple<Integer, Integer> f22 = scoreOneItem.getFunctions().get(ScoreOneItem.F2_2);
                        String sF22 = "";
                        if (f22 != null) {
                            if (f22.getArg1() != null) sF22 += f22.getArg1();
                            if (f22.getArg2() != null) sF22 += "." + f22.getArg2();
                        }
                        tw22.setText(sF22);

                        Tuple<Integer, Integer> f31 = scoreOneItem.getFunctions().get(ScoreOneItem.F3_1);
                        String sF31 = "";
                        if (f31 != null) {
                            if (f31.getArg1() != null) sF31 += f31.getArg1();
                            if (f31.getArg2() != null) sF31 += "." + f31.getArg2();
                        }
                        tw31.setText(sF31);

                        Tuple<Integer, Integer> f32 = scoreOneItem.getFunctions().get(ScoreOneItem.F3_2);
                        String sF32 = "";
                        if (f32 != null) {
                            if (f32.getArg1() != null) sF32 += f32.getArg1();
                            if (f32.getArg2() != null) sF32 += "." + f32.getArg2();
                        }
                        tw32.setText(sF32);

                        Tuple<Integer, Integer> f41 = scoreOneItem.getFunctions().get(ScoreOneItem.F4_1);
                        String sF41 = "";
                        if (f41 != null) {
                            if (f41.getArg1() != null) sF41 += f41.getArg1();
                            if (f41.getArg2() != null) sF41 += "." + f41.getArg2();
                        }
                        tw41.setText(sF41);

                        Tuple<Integer, Integer> f42 = scoreOneItem.getFunctions().get(ScoreOneItem.F4_2);
                        String sF42 = "";
                        if (f42 != null) {
                            if (f42.getArg1() != null) sF42 += f42.getArg1();
                            if (f42.getArg2() != null) sF42 += "." + f42.getArg2();
                        }
                        tw42.setText(sF42);


                        rowOccurence.addView(twShot);
                        rowOccurence.addView(twMatch);
                        rowOccurence.addView(twMatrice);
                        rowOccurence.addView(twBase);
                        rowOccurence.addView(tw1);
                        rowOccurence.addView(tw21);
                        rowOccurence.addView(tw22);
                        rowOccurence.addView(tw31);
                        rowOccurence.addView(tw32);
                        rowOccurence.addView(tw41);
                        rowOccurence.addView(tw42);

                        if (scoreOneItem.getBestTargetItem().isSuccess()) {
                            rowOccurence.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorGreenSuccess));
                        } else {
                            rowOccurence.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorRedLoss));
                        }
                        tableLayout.addView(rowOccurence);
                    }

                    LinearLayout linearLayoutOccurence = new LinearLayout(mainActivity);
                    linearLayoutOccurence.setPadding(10, 50, 50, 10);
                    linearLayoutOccurence.setOrientation(LinearLayout.VERTICAL);
                    linearLayoutOccurence.addView(occurenceNumberTextViewShow);
                    linearLayoutOccurence.addView(tableLayout);
                    layoutToPut.addView(linearLayoutOccurence);
                }
            } else {
                HashMap<Integer, ArrayList<ScoreTwoItem>> scoreTwos = (HashMap<Integer, ArrayList<ScoreTwoItem>>) values[0];
                for (int i : new TreeSet<>(scoreTwos.keySet())) {
                    ArrayList<ScoreTwoItem> scoreTwoItemsOfOccurence = scoreTwos.get(i);

                    //components
                    TextView occurenceNumberTextViewShow = new TextView(mainActivity);
                    TableLayout tableLayout = new TableLayout(mainActivity);
                    TableRow header = new TableRow(mainActivity);

                    header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
                    TextView headerShotTextView = new TextView(mainActivity);
                    headerShotTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headerShotTextView.setText("Tir");
                    headerShotTextView.setGravity(Gravity.CENTER);
                    headerShotTextView.setPadding(10, 10, 10, 10);

                    TextView headerMatchTextView = new TextView(mainActivity);
                    headerMatchTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headerMatchTextView.setText("Match");
                    headerMatchTextView.setGravity(Gravity.CENTER);
                    headerMatchTextView.setPadding(10, 10, 10, 10);

                    TextView headerTargetTextView = new TextView(mainActivity);
                    headerTargetTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headerTargetTextView.setText("Cible");
                    headerTargetTextView.setGravity(Gravity.CENTER);
                    headerTargetTextView.setPadding(10, 10, 10, 10);

                    TextView headereDnbTextView = new TextView(mainActivity);
                    headereDnbTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereDnbTextView.setText("Dénombrement");
                    headereDnbTextView.setGravity(Gravity.CENTER);
                    headereDnbTextView.setPadding(10, 10, 10, 10);

                    TextView headereATextView = new TextView(mainActivity);
                    headereATextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereATextView.setText("A");
                    headereATextView.setGravity(Gravity.CENTER);
                    headereATextView.setPadding(10, 10, 10, 10);

                    TextView headereBTextView = new TextView(mainActivity);
                    headereBTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereBTextView.setText("B");
                    headereBTextView.setGravity(Gravity.CENTER);
                    headereBTextView.setPadding(10, 10, 10, 10);

                    TextView headereCTextView = new TextView(mainActivity);
                    headereCTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereCTextView.setText("C");
                    headereCTextView.setGravity(Gravity.CENTER);
                    headereCTextView.setPadding(10, 10, 10, 10);

                    TextView headereDTextView = new TextView(mainActivity);
                    headereDTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereDTextView.setText("D");
                    headereDTextView.setGravity(Gravity.CENTER);
                    headereDTextView.setPadding(10, 10, 10, 10);

                    TextView headereETextView = new TextView(mainActivity);
                    headereETextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereETextView.setText("E");
                    headereETextView.setGravity(Gravity.CENTER);
                    headereETextView.setPadding(10, 10, 10, 10);

                    TextView headereFTextView = new TextView(mainActivity);
                    headereFTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
                    headereFTextView.setText("F");
                    headereFTextView.setGravity(Gravity.CENTER);
                    headereFTextView.setPadding(10, 10, 10, 10);


                    header.addView(headerShotTextView);
                    header.addView(headerMatchTextView);
                    header.addView(headerTargetTextView);
                    header.addView(headereDnbTextView);
                    header.addView(headereATextView);
                    header.addView(headereBTextView);
                    header.addView(headereCTextView);
                    header.addView(headereDTextView);
                    header.addView(headereETextView);
                    header.addView(headereFTextView);

                    tableLayout.addView(header);

                    occurenceNumberTextViewShow.setTypeface(fontButton);
                    occurenceNumberTextViewShow.setText("Occurence " + (i + 1));
                    occurenceNumberTextViewShow.setGravity(Gravity.CENTER);

                    for (ScoreTwoItem scoreTwoItem : scoreTwoItemsOfOccurence) {
                        TableRow rowOccurence = new TableRow(mainActivity);
                        rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

                        TextView twShot = new TextView(mainActivity);
                        twShot.setPadding(10, 10, 10, 10);
                        twShot.setGravity(Gravity.CENTER);
                        twShot.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twMatch = new TextView(mainActivity);
                        twMatch.setPadding(10, 10, 10, 10);
                        twMatch.setGravity(Gravity.CENTER);
                        twMatch.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twTarget = new TextView(mainActivity);
                        twTarget.setPadding(10, 10, 10, 10);
                        twTarget.setGravity(Gravity.CENTER);
                        twTarget.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twDnb = new TextView(mainActivity);
                        twDnb.setPadding(10, 10, 10, 10);
                        twDnb.setGravity(Gravity.CENTER);
                        twDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twA = new TextView(mainActivity);
                        twA.setPadding(10, 10, 10, 10);
                        twA.setGravity(Gravity.CENTER);
                        twA.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twB = new TextView(mainActivity);
                        twB.setPadding(10, 10, 10, 10);
                        twB.setGravity(Gravity.CENTER);
                        twB.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twC = new TextView(mainActivity);
                        twC.setPadding(10, 10, 10, 10);
                        twC.setGravity(Gravity.CENTER);
                        twC.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twD = new TextView(mainActivity);
                        twD.setPadding(10, 10, 10, 10);
                        twD.setGravity(Gravity.CENTER);
                        twD.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twE = new TextView(mainActivity);
                        twE.setPadding(10, 10, 10, 10);
                        twE.setGravity(Gravity.CENTER);
                        twE.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twF = new TextView(mainActivity);
                        twF.setPadding(10, 10, 10, 10);
                        twF.setGravity(Gravity.CENTER);
                        twF.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));


                        twShot.setText(String.valueOf(scoreTwoItem.getBestTargetItem().getTarget().getShot()));

                        twMatch.setText(String.valueOf(scoreTwoItem.getBestTargetItem().getTarget().getMatch()));

                        twTarget.setText(ResultFragment.findSimpleChanceFromExit(String.valueOf((double) scoreTwoItem.getBestTargetItem().getSimpleChance())));

                        twDnb.setText(String.valueOf(scoreTwoItem.getBestTargetItem().getDenombrement()));

                        Tuple<Integer, Integer> A = scoreTwoItem.getFunctions().get(ScoreTwoItem.A);
                        String sA = "";
                        if (A.getArg1() != null) sA += A.getArg1();
                        if (A.getArg2() != null) sA += "." + A.getArg2();
                        twA.setText(sA);

                        Tuple<Integer, Integer> B = scoreTwoItem.getFunctions().get(ScoreTwoItem.B);
                        String sB = "";
                        if (B.getArg1() != null) sB += B.getArg1();
                        if (B.getArg2() != null) sB += "." + B.getArg2();
                        twB.setText(sB);

                        Tuple<Integer, Integer> C = scoreTwoItem.getFunctions().get(ScoreTwoItem.C);
                        String sC = "";
                        if (C.getArg1() != null) sC += C.getArg1();
                        if (C.getArg2() != null) sC += "." + C.getArg2();
                        twC.setText(sC);

                        Tuple<Integer, Integer> D = scoreTwoItem.getFunctions().get(ScoreTwoItem.D);
                        String sD = "";
                        if (D.getArg1() != null) sD += D.getArg1();
                        if (D.getArg2() != null) sD += "." + D.getArg2();
                        twD.setText(sD);

                        twE.setText(ResultFragment.findSimpleChanceFromExit(String.valueOf((double) scoreTwoItem.getE())));

                        Tuple<Boolean, Tuple> F = scoreTwoItem.getFunctions().get(ScoreTwoItem.F);
                        Tuple<Integer, Integer> infosF = F.getArg2();
                        String sF = "";
                        if (infosF.getArg1() != null) sF += infosF.getArg1();
                        if (infosF.getArg2() != null) sF += "." + infosF.getArg2();
                        twF.setText(sF);

                        if (F.getArg1()) {
                            twF.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorGreenSuccess));
                            twE.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorGreenSuccess));
                        } else {
                            twF.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorRedLoss));
                            twE.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorRedLoss));
                        }

                        rowOccurence.addView(twShot);
                        rowOccurence.addView(twMatch);
                        rowOccurence.addView(twTarget);
                        rowOccurence.addView(twDnb);
                        rowOccurence.addView(twA);
                        rowOccurence.addView(twB);
                        rowOccurence.addView(twC);
                        rowOccurence.addView(twD);
                        rowOccurence.addView(twE);
                        rowOccurence.addView(twF);

                        if (scoreTwoItem.getBestTargetItem().isSuccess()) {
                            rowOccurence.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorGreenSuccess));
                        } else {
                            rowOccurence.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorRedLoss));
                        }
                        tableLayout.addView(rowOccurence);
                    }

                    LinearLayout linearLayoutOccurence = new LinearLayout(mainActivity);
                    linearLayoutOccurence.setPadding(10, 50, 50, 10);
                    linearLayoutOccurence.setOrientation(LinearLayout.VERTICAL);
                    linearLayoutOccurence.addView(occurenceNumberTextViewShow);
                    linearLayoutOccurence.addView(tableLayout);
                    layoutToPut.addView(linearLayoutOccurence);
                }
            }
        } else {
            TableLayout tableLayout = new TableLayout(mainActivity);
            TableRow header = new TableRow(mainActivity);

            header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 5f));
            TextView headerShotTextView = new TextView(mainActivity);
            headerShotTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            headerShotTextView.setText("Tir");
            headerShotTextView.setGravity(Gravity.CENTER);
            headerShotTextView.setPadding(10, 10, 10, 10);

            TextView headerTargetTextView = new TextView(mainActivity);
            headerTargetTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            headerTargetTextView.setText("Cible");
            headerTargetTextView.setGravity(Gravity.CENTER);
            headerTargetTextView.setPadding(10, 10, 10, 10);

            TextView headerValorisationTextView = new TextView(mainActivity);
            headerValorisationTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            headerValorisationTextView.setText("Valorisation");
            headerValorisationTextView.setGravity(Gravity.CENTER);
            headerValorisationTextView.setPadding(10, 10, 10, 10);

            TextView headereScoreTextView = new TextView(mainActivity);
            headereScoreTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            headereScoreTextView.setText("Score");
            headereScoreTextView.setGravity(Gravity.CENTER);
            headereScoreTextView.setPadding(10, 10, 10, 10);

            TextView headereDnbTextView = new TextView(mainActivity);
            headereDnbTextView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            headereDnbTextView.setText("Dénombrements");
            headereDnbTextView.setGravity(Gravity.CENTER);
            headereDnbTextView.setPadding(10, 10, 10, 10);

            header.addView(headerShotTextView);
            header.addView(headerTargetTextView);
            header.addView(headerValorisationTextView);
            header.addView(headereScoreTextView);
            header.addView(headereDnbTextView);

            tableLayout.addView(header);

            Parameters parameters = mainActivity.getHelper().getParameters();
            ArrayList<ValorisationItem> valorisations = (ArrayList<ValorisationItem>) values[0];

            for (int i = 0; i < valorisations.size(); i++) {
                ValorisationItem valorisationItem = valorisations.get(i);
                boolean first = true;
                for (int o = 0; o < parameters.getOccurencesNumber(); o++) {
                    if (valorisationItem.getValuesScoreOne().get(o) != null && valorisationItem.getValuesScoreOne().get(o).size() > 0 && valorisationItem.getValorisation().get(o) != 0) {
                        TableRow rowOccurence = new TableRow(mainActivity);
                        rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 5f));

                        TextView twShot = new TextView(mainActivity);
                        twShot.setPadding(10, 10, 10, 10);
                        twShot.setGravity(Gravity.CENTER);
                        twShot.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twTarget = new TextView(mainActivity);
                        twTarget.setPadding(10, 10, 10, 10);
                        twTarget.setGravity(Gravity.CENTER);
                        twTarget.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView tWValorisation = new TextView(mainActivity);
                        tWValorisation.setPadding(10, 10, 10, 10);
                        tWValorisation.setGravity(Gravity.CENTER);
                        tWValorisation.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twScore = new TextView(mainActivity);
                        twScore.setPadding(10, 10, 10, 10);
                        twScore.setGravity(Gravity.CENTER);
                        twScore.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twDnb = new TextView(mainActivity);
                        twDnb.setPadding(10, 10, 10, 10);
                        twDnb.setGravity(Gravity.CENTER);
                        twDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        if (first) {

                            twShot.setText(String.valueOf(valorisationItem.getBestTargetItem().get(o).getTarget().getShot()));
                            first = false;
                        }

                        twTarget.setText(ResultFragment.findSimpleChanceFromExit(String.valueOf(valorisationItem.getFinalExitTarget().get(o))));

                        tWValorisation.setText(String.valueOf(valorisationItem.getValorisation().get(o)));

                        twScore.setText("1");

                        twDnb.setText(valorisationItem.getStringValueScoreOne(o));

                        rowOccurence.addView(twShot);
                        rowOccurence.addView(twTarget);
                        rowOccurence.addView(tWValorisation);
                        rowOccurence.addView(twScore);
                        rowOccurence.addView(twDnb);

                        if (valorisationItem.getBestTargetItem().get(o).isSuccess()) {
                            rowOccurence.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorGreenSuccess));
                        }
                        else{
                            rowOccurence.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorRedLoss));
                        }


                        tableLayout.addView(rowOccurence);
                    }
                    if (valorisationItem.getValuesScoreTwo().get(o) != null && valorisationItem.getValuesScoreTwo().get(o).size() > 0 && valorisationItem.getValorisation().get(o) != 0) {
                        TableRow rowOccurence = new TableRow(mainActivity);
                        rowOccurence.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 5f));

                        TextView twShot = new TextView(mainActivity);
                        twShot.setPadding(10, 10, 10, 10);
                        twShot.setGravity(Gravity.CENTER);
                        twShot.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twTarget = new TextView(mainActivity);
                        twTarget.setPadding(10, 10, 10, 10);
                        twTarget.setGravity(Gravity.CENTER);
                        twTarget.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView tWValorisation = new TextView(mainActivity);
                        tWValorisation.setPadding(10, 10, 10, 10);
                        tWValorisation.setGravity(Gravity.CENTER);
                        tWValorisation.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twScore = new TextView(mainActivity);
                        twScore.setPadding(10, 10, 10, 10);
                        twScore.setGravity(Gravity.CENTER);
                        twScore.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));

                        TextView twDnb = new TextView(mainActivity);
                        twDnb.setPadding(10, 10, 10, 10);
                        twDnb.setGravity(Gravity.CENTER);
                        twDnb.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));


                        if (valorisationItem.getValuesScoreOne().get(o) == null || !(valorisationItem.getValuesScoreOne().get(o).size() > 0)) {
                            if (first) {
                                twShot.setText(String.valueOf(valorisationItem.getBestTargetItem().get(o).getTarget().getShot()));
                                first = false;
                            }

                            twTarget.setText(ResultFragment.findSimpleChanceFromExit(String.valueOf((double) valorisationItem.getBestTargetItem().get(o).getSimpleChance())));

                            tWValorisation.setText(String.valueOf(valorisationItem.getValorisation().get(o)));
                        }

                        twScore.setText("2");

                        twDnb.setText(valorisationItem.getStringValueScoreTwo(o));

                        rowOccurence.addView(twShot);
                        rowOccurence.addView(twTarget);
                        rowOccurence.addView(tWValorisation);
                        rowOccurence.addView(twScore);
                        rowOccurence.addView(twDnb);

                        if (valorisationItem.getBestTargetItem().get(o).isSuccess()) {
                            rowOccurence.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorGreenSuccess));
                        }
                        else{
                            rowOccurence.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorRedLoss));
                        }


                        tableLayout.addView(rowOccurence);
                    }
                }
            }

            layoutToPut.addView(tableLayout);
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mainActivity.getProgressDialog().dismiss();
        cancel(true);
    }
}
