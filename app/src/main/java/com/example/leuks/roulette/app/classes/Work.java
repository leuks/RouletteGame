package com.example.leuks.roulette.app.classes;

import android.app.ProgressDialog;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.app.classes.tasks.action.BestTargetingTask;
import com.example.leuks.roulette.app.classes.tasks.action.DBWorkTask;
import com.example.leuks.roulette.app.classes.tasks.action.ScoreOneTask;
import com.example.leuks.roulette.app.classes.tasks.action.ScoreTwoTask;
import com.example.leuks.roulette.app.classes.tasks.action.TargetingPoolTask;
import com.example.leuks.roulette.app.classes.tasks.action.ValorisationTask;
import com.example.leuks.roulette.commons.bdd.BestTargetItem;
import com.example.leuks.roulette.commons.bdd.Parameters;
import com.example.leuks.roulette.commons.bdd.ScoreOneItem;
import com.example.leuks.roulette.commons.bdd.ScoreTwoItem;
import com.example.leuks.roulette.commons.bdd.Target;
import com.example.leuks.roulette.commons.bdd.TargetingStatItem;
import com.example.leuks.roulette.commons.bdd.ValorisationItem;
import com.example.leuks.roulette.commons.entities.Tuple;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Leuks on 24/11/2016.
 */

public class Work {
    private int shot;
    private MainActivity mainActivity;
    private HashMap<Integer, HashMap<Integer, ArrayList<Target>>> allTargetsAsHashMap;
    private HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> globalTargeting;
    private HashMap<Integer, ArrayList<BestTargetItem>> lastExitOccurence;
    private ValorisationItem valorisations;
    private boolean scoreOneReady;
    private boolean scoreTwoReady;
    private ProgressDialog progressDialog;
    private WorkManager workManager;

    private HashMap<Integer, ArrayList<ScoreTwoItem>> scoresTwo;
    private HashMap<Integer, ArrayList<ScoreOneItem>> scoresOne;
    private int lastId;
    private int lastBestId;
    private int lastScoreTwoId;


    public Work(MainActivity mainActivity, WorkManager workManager) {
        this.mainActivity = mainActivity;
        progressDialog = mainActivity.getProgressDialog();
        this.workManager = workManager;
        globalTargeting = new HashMap<>();
        scoresTwo = null;
        scoresOne = null;
        allTargetsAsHashMap = null;


        Parameters parameters = mainActivity.getHelper().getParameters();
        init();
    }

    public void start(int shot, Integer simpleChance) {
        Parameters parameters = mainActivity.getHelper().getParameters();
        this.shot = shot;
        if (workManager == null) {
            progressDialog = mainActivity.getProgressDialog();
            progressDialog.setTitle("Module 1 & 2");
            progressDialog.setMessage(parameters.getTabsNumber() + " tableaux et " + parameters.getOccurencesNumber() + " occurences");
            progressDialog.show();
        }

        TargetingPoolTask targetingPoolTask = new TargetingPoolTask(mainActivity, shot, simpleChance, this);
        targetingPoolTask.executeOnExecutor(MainActivity.executor);
    }

    public void executeTargetingTask() {
        if (workManager == null) progressDialog.setTitle("Ciblage Global");
        BestTargetingTask bestTargetingTask = new BestTargetingTask(mainActivity, globalTargeting, this);
        bestTargetingTask.executeOnExecutor(MainActivity.executor);
    }

    public void executeScore(HashMap<Integer, ArrayList<BestTargetItem>> lastExitOccurence) {
        if (workManager == null) progressDialog.setTitle("Scores");
        this.lastExitOccurence = lastExitOccurence;
        Parameters parameters = mainActivity.getHelper().getParameters();
        Tuple<Boolean, Boolean> scores = parameters.getScores();
        if (scoresTwo == null) {
            lastScoreTwoId = mainActivity.getHelper().getLastScoreTwoInd();
            scoresTwo = mainActivity.getHelper().getAllScoreTwos();
        }
        if (scoresOne == null) scoresOne = mainActivity.getHelper().getAllScoreOnes();

        for (ArrayList<BestTargetItem> array : lastExitOccurence.values()) {
            for (BestTargetItem b : array) {
                mainActivity.registerDB(new TargetingStatItem(b), 0, 0);
            }
        }

        if (scores.getArg1()) {
            ScoreOneTask scoreOneTask = new ScoreOneTask(mainActivity, lastExitOccurence, scoresOne, this);
            scoreOneTask.executeOnExecutor(MainActivity.executor);
        }

        if (scores.getArg2()) {
            ScoreTwoTask scoreTwoTask = new ScoreTwoTask(mainActivity, lastExitOccurence, scoresTwo, this);
            scoreTwoTask.executeOnExecutor(MainActivity.executor);
        }
    }

    public void addScoreOne(HashMap<Integer, ArrayList<ScoreOneItem>> scoresOnes) {
        Parameters parameters = mainActivity.getHelper().getParameters();
        for (int i = 0; i < parameters.getOccurencesNumber(); i++) {
            mainActivity.registerDB(scoresOnes.get(i).get(scoresOnes.get(i).size() - 1), 0, 0);
        }
        this.scoresOne = scoresOnes;
        scoreOneReady = true;
        startValorisation();
    }

    public void addScoreTwo(HashMap<Integer, ArrayList<ScoreTwoItem>> scoresTwos) {
        Parameters parameters = mainActivity.getHelper().getParameters();
        for (int i = 0; i < parameters.getOccurencesNumber(); i++) {
            ScoreTwoItem scoreTwoItem = scoresTwos.get(i).get(scoresTwos.get(i).size() - 1);
            lastScoreTwoId++;
            scoreTwoItem.setId(lastScoreTwoId);
            mainActivity.registerDB(scoreTwoItem, DBWorkTask.ADD, 0);
        }
        this.scoresTwo = scoresTwos;
        scoreTwoReady = true;
        startValorisation();
    }

    private void startValorisation() {
        if (scoreOneReady && scoreTwoReady) {
            if (workManager == null) progressDialog.setTitle("Valorisation");

            HashMap<Integer, ScoreOneItem> sc1 = new HashMap<>();
            for (int key : scoresOne.keySet()) {
                sc1.put(key, scoresOne.get(key).get(scoresOne.get(key).size() - 1));
            }

            HashMap<Integer, ScoreTwoItem> sc2 = new HashMap<>();
            for (int key : scoresTwo.keySet()) {
                sc2.put(key, scoresTwo.get(key).get(scoresTwo.get(key).size() - 1));
            }

            ValorisationTask valorisationTask = new ValorisationTask(mainActivity, this, sc1, sc2);
            valorisationTask.executeOnExecutor(MainActivity.executor);
        }
    }

    public void close() {
        if (workManager == null) {
            mainActivity.getProgressDialog().dismiss();
            mainActivity.show(MainActivity.ROAD_RESULT, true);
        } else {
            workManager.addWorkResult();
        }
        init();
    }

    public void init() {
        Tuple<Boolean, Boolean> scores = mainActivity.getHelper().getParameters().getScores();
        if (scores.getArg1()) scoreOneReady = false;
        else scoreOneReady = true;
        if (scores.getArg2()) scoreTwoReady = false;
        else scoreTwoReady = true;
    }

    public void updateTargeting(int tab, int occu, int ind) {
        globalTargeting.get(tab).get(occu).get(ind).setSuccess(true);
    }

    //GS

    public HashMap<Integer, HashMap<Integer, ArrayList<Target>>> getAllTargetsAsHashMap() {
        return allTargetsAsHashMap;
    }

    public void setAllTargetsAsHashMap(HashMap<Integer, HashMap<Integer, ArrayList<Target>>> allTargetsAsHashMap) {
        this.allTargetsAsHashMap = allTargetsAsHashMap;
    }

    public ValorisationItem getValorisations() {
        return valorisations;
    }

    public void setValorisations(ValorisationItem valorisations) {
        this.valorisations = valorisations;
    }

    public boolean isScoreTwoReady() {
        return scoreTwoReady;
    }

    public HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> getGlobalTargeting() {
        return globalTargeting;
    }

    public void setGlobalTargeting(HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> globalTargeting) {
        this.globalTargeting = globalTargeting;
    }

    public HashMap<Integer, ArrayList<BestTargetItem>> getLastExitOccurence() {
        return lastExitOccurence;
    }

    public boolean isScoreOneReady() {
        return scoreOneReady;
    }

    public WorkManager getWorkManager() {
        return workManager;
    }

    public void setWorkManager(WorkManager workManager) {
        this.workManager = workManager;
    }

    public int getLastBestId() {
        return lastBestId;
    }

    public void setLastBestId(int lastBestId) {
        this.lastBestId = lastBestId;
    }

    public int getLastId() {
        return lastId;
    }

    public void setLastId(int lastId) {
        this.lastId = lastId;
    }

    public HashMap<Integer, ArrayList<ScoreOneItem>> getScoresOne() {
        return scoresOne;
    }

    public void setScoresOne(HashMap<Integer, ArrayList<ScoreOneItem>> scoresOne) {
        this.scoresOne = scoresOne;
    }

    public HashMap<Integer, ArrayList<ScoreTwoItem>> getScoresTwo() {
        return scoresTwo;
    }

    public void setScoresTwo(HashMap<Integer, ArrayList<ScoreTwoItem>> scoresTwo) {
        this.scoresTwo = scoresTwo;
    }
}
