package com.example.leuks.roulette.commons.bdd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.leuks.roulette.app.classes.tasks.action.TargetToUpdate;
import com.example.leuks.roulette.commons.entities.InfiniteTuple;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by Leuks on 21/11/2016.
 */

public class DBManager extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "vSDVAERG AE";
    private static final int DATABASE_VERSION = 1;
    private Dao<Parameters, String> daoParameters = null;
    private Dao<Target, String> daoTarget = null;
    private Dao<BestTargetItem, String> daoBestTargetItem = null;
    private Dao<ScoreOneItem, String> daoScoreOne = null;
    private Dao<ScoreTwoItem, String> daoScoreTwo = null;
    private Dao<ValorisationItem, String> daoValorisation = null;
    private Dao<TargetingStatItem, String> daoStatItem = null;
    private Dao<ProfilItem, String> daoProfilItem = null;

    public DBManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, Parameters.class);
            TableUtils.createTableIfNotExists(connectionSource, Target.class);
            TableUtils.createTableIfNotExists(connectionSource, ScoreOneItem.class);
            TableUtils.createTableIfNotExists(connectionSource, ScoreTwoItem.class);
            TableUtils.createTableIfNotExists(connectionSource, BestTargetItem.class);
            TableUtils.createTableIfNotExists(connectionSource, ValorisationItem.class);
            TableUtils.createTableIfNotExists(connectionSource, TargetingStatItem.class);
            TableUtils.createTableIfNotExists(connectionSource, ProfilItem.class);
            Log.i(DBManager.class.getName(), "DBManager onCreate");
        } catch (SQLException e) {
            Log.e(DBManager.class.getName(), "DBManager can't create database", e);
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {

    }

    //GS
    public Parameters getParameters() {
        Parameters p = getCurrentProfilItem().getParameters();
        return p;
    }

    public ProfilItem getCurrentProfilItem() {
        List<ProfilItem> profils = getAllProfilItems();
        for (ProfilItem p : profils) {
            if (p.isActivate())
                return p;
        }
        return null;
    }

    public void changeCurrentProfilItem(String name) {
        List<ProfilItem> profils = getAllProfilItems();
        for (ProfilItem p : profils) {
            if (p.isActivate())
                p.setActivate(false);
            if (p.getName().equals(name))
                p.setActivate(true);


            try {
                daoProfilItem.update(p);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public void removeProfile(ProfilItem item){
        try {
            daoProfilItem.delete(item);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getLastTargetInd() {
        List<Target> targetList = null;
        try {
            targetList = daoTarget.queryBuilder().orderBy("id", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (targetList == null || targetList.isEmpty()) return 0;
        else return targetList.get(targetList.size() - 1).getId();
    }

    public int getLastScoreTwoInd() {
        List<ScoreTwoItem> targetList = null;
        try {
            targetList = daoScoreTwo.queryBuilder().orderBy("id", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (targetList == null || targetList.isEmpty()) return 0;
        else return targetList.get(targetList.size() - 1).getId();
    }


    public int getLastBestTargetItemId() {
        List<BestTargetItem> targetList = null;
        try {
            targetList = daoBestTargetItem.queryBuilder().orderBy("id", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (targetList == null || targetList.isEmpty()) return 0;
        else return targetList.get(targetList.size() - 1).getId();
    }

    public InfiniteTuple getLast3Shots() {
        InfiniteTuple<Integer> shots = new InfiniteTuple<>();
        List<Target> targetList = null;

        try {
            targetList = daoTarget.queryForEq("indoccurence", 0);
        } catch (SQLException e) {
            e.printStackTrace();
        }


        if (targetList != null && (targetList.size() != 0)) {
            for (int i = targetList.size() - 1; i >= 0; i--) {
                Target lastTarget = targetList.get(i);
                if (lastTarget.getIndTab() == 0) {
                    if ((lastTarget.getIndTab() == 0) && (lastTarget.getIndOccurence() == 0) && (lastTarget.getShot() == lastTarget.getMatch())) {
                        shots.add(lastTarget.getShot());
                        if (shots.size() == 3) {
                            break;
                        }
                    }
                }
            }
        }
        return shots;
    }

    public HashMap<Integer, ArrayList<ScoreTwoItem>> getAllScoreTwos() {
        List<ScoreTwoItem> targetList = null;
        HashMap<Integer, ArrayList<ScoreTwoItem>> returnMap = new HashMap<>();
        try {
            targetList = daoScoreTwo.queryBuilder().orderBy("id", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (ScoreTwoItem scoreTwoItem : targetList) {
            int indOccu = scoreTwoItem.getBestTargetItem().getTarget().getIndOccurence();
            if (returnMap.get(indOccu) == null) {
                ArrayList<ScoreTwoItem> arrayToPut = new ArrayList<>();
                arrayToPut.add(scoreTwoItem);
                returnMap.put(indOccu, arrayToPut);
            } else {
                returnMap.get(indOccu).add(scoreTwoItem);
            }
        }
        return returnMap;
    }

    public HashMap<Integer, ArrayList<ScoreOneItem>> getAllScoreOnes() {
        List<ScoreOneItem> targetList = null;
        HashMap<Integer, ArrayList<ScoreOneItem>> returnMap = new HashMap<>();
        try {
            targetList = daoScoreOne.queryBuilder().orderBy("id", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (ScoreOneItem scoreOneItem : targetList) {
            int indOccu = scoreOneItem.getBestTargetItem().getTarget().getIndOccurence();
            if (returnMap.get(indOccu) == null) {
                ArrayList<ScoreOneItem> arrayToPut = new ArrayList<>();
                arrayToPut.add(scoreOneItem);
                returnMap.put(indOccu, arrayToPut);
            } else {
                returnMap.get(indOccu).add(scoreOneItem);
            }
        }
        return returnMap;
    }

    public HashMap<Integer, ArrayList<Target>> getTargetsAsHashMap(int tab) {
        return getAllTargetsAsHashMap().get(tab);
    }

    public HashMap<Integer, HashMap<Integer, ArrayList<Target>>> getAllTargetsAsHashMap() {
        HashMap<Integer, HashMap<Integer, ArrayList<Target>>> tabs = new HashMap<>();
        Parameters parameters = getParameters();
        List<Target> targetList = null;
        try {
            targetList = daoTarget.queryBuilder().orderBy("id", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if ((targetList != null) && (targetList.size() != 0)) {
            for (Target target : targetList) {
                if (target.getIndOccurence() < parameters.getOccurencesNumber() && target.getIndTab() < parameters.getTabsNumber()) {
                    if (tabs.get(target.getIndTab()) == null) {
                        HashMap<Integer, ArrayList<Target>> hashMapToPut = new HashMap<>();
                        tabs.put(target.getIndTab(), hashMapToPut);
                    }

                    if (tabs.get(target.getIndTab()).get(target.getIndOccurence()) == null) {
                        ArrayList<Target> arrayToPut = new ArrayList<>();
                        arrayToPut.add(target);
                        tabs.get(target.getIndTab()).put(target.getIndOccurence(), arrayToPut);
                    } else {
                        ArrayList<Target> arrayToPut = tabs.get(target.getIndTab()).get(target.getIndOccurence());
                        arrayToPut.add(target);
                    }
                }
            }
        }
        return tabs;
    }

    public HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> getAllBestTargetItemsAsHashMap() {
        HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> tabs = new HashMap<>();
        List<BestTargetItem> targetList = null;
        Parameters parameters = getParameters();
        try {
            targetList = daoBestTargetItem.queryBuilder().orderBy("id", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if ((targetList != null) && (targetList.size() != 0)) {
            for (BestTargetItem target : targetList) {
                if (target.getTarget().getIndOccurence() < parameters.getOccurencesNumber() && target.getTarget().getIndTab() < parameters.getTabsNumber()) {
                    if (tabs.get(target.getTarget().getIndTab()) == null) {
                        HashMap<Integer, ArrayList<BestTargetItem>> hashMapToPut = new HashMap<>();
                        tabs.put(target.getTarget().getIndTab(), hashMapToPut);
                    }

                    if (tabs.get(target.getTarget().getIndTab()).get(target.getTarget().getIndOccurence()) == null) {
                        ArrayList<BestTargetItem> arrayToPut = new ArrayList<>();
                        arrayToPut.add(target);
                        tabs.get(target.getTarget().getIndTab()).put(target.getTarget().getIndOccurence(), arrayToPut);
                    } else {
                        ArrayList<BestTargetItem> arrayToPut = tabs.get(target.getTarget().getIndTab()).get(target.getTarget().getIndOccurence());
                        arrayToPut.add(target);
                    }
                }
            }
        }
        return tabs;
    }

    public ArrayList<ValorisationItem> getAllValorisations() {
        List<ValorisationItem> targetList = null;
        try {
            targetList = daoValorisation.queryBuilder().orderBy("id", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>(targetList);
    }

    public HashMap<Integer, ArrayList<BestTargetItem>> getAllStatItem() {
        HashMap<Integer, ArrayList<BestTargetItem>> occs = new HashMap<>();
        List<TargetingStatItem> targetList = null;
        try {
            targetList = daoStatItem.queryBuilder().orderBy("id", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (TargetingStatItem item : targetList) {
            int occu = item.getBestTargetItem().getTarget().getIndOccurence();
            if (occs.get(occu) == null) {
                ArrayList arrayToPut = new ArrayList();
                arrayToPut.add(item.getBestTargetItem());
                occs.put(occu, arrayToPut);
            } else {
                occs.get(occu).add(item.getBestTargetItem());
            }
        }

        return occs;
    }

    public void createNewGlobalTargets(final ArrayList<BestTargetItem> globalTargeting) {
        try {
            daoBestTargetItem.callBatchTasks(new Callable<Void>() {
                public Void call() throws Exception {
                    for (BestTargetItem item : globalTargeting) {
                        daoBestTargetItem.create(item);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createNewTargets(final ArrayList<Target> targeting) {
        try {
            daoTarget.callBatchTasks(new Callable<Void>() {
                public Void call() throws Exception {
                    for (Target item : targeting) {
                        daoTarget.create(item);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateMassTargets(final ArrayList<TargetToUpdate> toUpdate) {
        for (TargetToUpdate item : toUpdate) {
            List<BestTargetItem> targetList = null;
            try {
                targetList = daoBestTargetItem.queryForEq("target", item.getTarget());
            } catch (SQLException e) {
                e.printStackTrace();
            }

            List<ValorisationItem> valorisationList = null;
            try {
                valorisationList = daoValorisation.queryForAll();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            for (BestTargetItem b : targetList) {
                if (b.getTarget().equals(item.getTarget())) {
                    b.setSuccess(true);
                    try {
                        daoBestTargetItem.update(b);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

            for (ValorisationItem v : valorisationList) {
                HashMap<Integer, BestTargetItem> bests = v.getBestTargetItem();
                for (int key : bests.keySet()) {
                    BestTargetItem best = bests.get(key);
                    if (best.getTarget().equals(item.getTarget())) {
                        best.setSuccess(true);
                        try {
                            daoValorisation.update(v);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
    }

    public void updateMassScoreOne(final ArrayList<ScoreOneItem> toUpdate) {
        try {
            daoScoreOne.callBatchTasks(new Callable<Void>() {
                public Void call() throws Exception {
                    for (ScoreOneItem item : toUpdate) {
                        daoScoreOne.create(item);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateMassScoreTwo(final ArrayList<ScoreTwoItem> toUpdate) {
        try {
            daoScoreTwo.callBatchTasks(new Callable<Void>() {
                public Void call() throws Exception {
                    for (ScoreTwoItem item : toUpdate) {
                        daoScoreTwo.create(item);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void createScoreOne(ScoreOneItem target) {
        try {
            daoScoreOne.create(target);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createScoreTwo(ScoreTwoItem scoreTwoItem) {
        try {
            daoScoreTwo.create(scoreTwoItem);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createTarget(Target target) {
        try {
            daoTarget.create(target);
        } catch (SQLException e) {
            Log.e("target", "id  " + target.getId());
            e.printStackTrace();
        }
    }

    public void createBestTargetItem(BestTargetItem target) {
        try {
            daoBestTargetItem.create(target);
        } catch (SQLException e) {
            Log.e("bestSTa", "   id   " + target.getId());
            e.printStackTrace();
        }
    }

    public void createValorisationItem(ValorisationItem target) {
        try {
            daoValorisation.create(target);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createTargetingStatItem(TargetingStatItem target) {
        try {
            daoStatItem.create(target);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateBestTargetItem(Target target, boolean success) {
        List<BestTargetItem> targetList = null;
        try {
            targetList = daoBestTargetItem.queryForEq("target", target);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (BestTargetItem bestTargetItem : targetList) {
            if (bestTargetItem.getTarget() != null && bestTargetItem.getTarget().equals(target)) {
                bestTargetItem.setSuccess(success);
                try {
                    daoBestTargetItem.update(bestTargetItem);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

    }

    public void createProfilItem(ProfilItem item) {
        try {
            daoParameters.create(item.getParameters());
            daoProfilItem.create(item);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void updateScoreTwoItem(ScoreTwoItem scoreTwoItem) {
        try {
            daoScoreTwo.update(scoreTwoItem);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateParameters(Parameters parameters) {
        try {
            daoParameters.update(parameters);
        } catch (SQLException e) {
            Log.e(DBManager.class.getName(), "DBManager can't update parameters", e);
            e.printStackTrace();
        }
    }

    public List<ProfilItem> getAllProfilItems() {
        List<ProfilItem> list = null;
        try {
            list = daoProfilItem.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public void removeMemory() {
        try {
            TableUtils.clearTable(connectionSource, Target.class);
            TableUtils.clearTable(connectionSource, BestTargetItem.class);
            TableUtils.clearTable(connectionSource, ScoreOneItem.class);
            TableUtils.clearTable(connectionSource, ScoreTwoItem.class);
            TableUtils.clearTable(connectionSource, ValorisationItem.class);
            TableUtils.clearTable(connectionSource, TargetingStatItem.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    //DAO
    public void init(String path) {
        if (daoParameters == null) {
            String statement = "PRAGMA temp_store_directory='" + path + "'";
            getWritableDatabase().execSQL(statement);
        }
        getDaoParameters();
        getDaoTarget();
        getDaoBestTargetItem();
        getDaoScoreTwo();
        getDaoScoreOne();
        getDaoValorisation();
        getDaoStatItem();
        getDaoProfilItem();
        insertData();
    }

    private void insertData() {
        try {
            ProfilItem p = new ProfilItem("défaut", new Parameters(), true);
            daoParameters.create(p.getParameters());
            daoProfilItem.create(p);
        } catch (SQLException e) {
            Log.e("insert", "daoParameters");
            e.printStackTrace();
        }
    }

    public Dao<Parameters, String> getDaoParameters() {
        if (daoParameters == null) {
            try {
                daoParameters = DaoManager.createDao(connectionSource, Parameters.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoParameters;
    }

    public Dao<Target, String> getDaoTarget() {
        if (daoTarget == null) {
            try {
                daoTarget = DaoManager.createDao(connectionSource, Target.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoTarget;
    }

    public Dao<BestTargetItem, String> getDaoBestTargetItem() {
        if (daoBestTargetItem == null) {
            try {
                daoBestTargetItem = DaoManager.createDao(connectionSource, BestTargetItem.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoBestTargetItem;
    }

    public Dao<ScoreOneItem, String> getDaoScoreOne() {
        if (daoScoreOne == null) {
            try {
                daoScoreOne = DaoManager.createDao(connectionSource, ScoreOneItem.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoScoreOne;
    }

    public Dao<ScoreTwoItem, String> getDaoScoreTwo() {
        if (daoScoreTwo == null) {
            try {
                daoScoreTwo = DaoManager.createDao(connectionSource, ScoreTwoItem.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoScoreTwo;
    }

    public Dao<ValorisationItem, String> getDaoValorisation() {
        if (daoValorisation == null) {
            try {
                daoValorisation = DaoManager.createDao(connectionSource, ValorisationItem.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoValorisation;
    }

    public Dao<TargetingStatItem, String> getDaoStatItem() {
        if (daoStatItem == null) {
            try {
                daoStatItem = DaoManager.createDao(connectionSource, TargetingStatItem.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoStatItem;
    }

    public Dao<ProfilItem, String> getDaoProfilItem() {
        if (daoProfilItem == null) {
            try {
                daoProfilItem = DaoManager.createDao(connectionSource, ProfilItem.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoProfilItem;
    }
}
