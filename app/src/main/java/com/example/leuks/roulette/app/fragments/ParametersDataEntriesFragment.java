package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Space;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.commons.bdd.Parameters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by Leuks on 12/12/2016.
 */

public class ParametersDataEntriesFragment extends Fragment {
    private MainActivity mainActivity;
    private TableLayout dataTable;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.fragment_parameters_entries_data_layout, container, false);

        //childs
        dataTable = (TableLayout) thisView.findViewById(R.id.table_data);
        Button validateButton = (Button) thisView.findViewById(R.id.button_validate_data_parameters);

        //listeners
        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Parameters parameters = mainActivity.getHelper().getParameters();
                final Parameters oldParameters = mainActivity.getHelper().getParameters();
                for (int i = 1; i < dataTable.getChildCount(); i++) {
                    ArrayList<Integer> matches = parameters.getMatchesOf(i - 1);
                    TableRow indexedRow = (TableRow) dataTable.getChildAt(i);
                    for (int j = 1; j < indexedRow.getChildCount(); j++) {
                        EditText indexedEditText = ((EditText) indexedRow.getChildAt(j));
                        String indexedValue = indexedEditText.getText().toString().trim();
                        if (Pattern.compile("\\d+").matcher(indexedValue).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(indexedValue).find()) {
                            int intIndexedValue = Integer.parseInt(indexedValue);
                            if (matches.get(j - 1) != intIndexedValue)
                                matches.set(j - 1, intIndexedValue);
                        } else {

                            return;
                        }

                    }
                }
                mainActivity.getHelper().updateParameters(parameters);

                Snackbar snackbar = Snackbar
                        .make(mainActivity.findViewById(android.R.id.content), "Paramètres mis à jour!", Snackbar.LENGTH_LONG)
                        .setAction("ANNULER", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mainActivity.getHelper().updateParameters(oldParameters);
                                refreshView();
                                Snackbar snackbar1 = Snackbar.make(mainActivity.findViewById(android.R.id.content), "Paramètres rétablis!", Snackbar.LENGTH_SHORT);
                                snackbar1.show();
                            }
                        });
                snackbar.show();
            }
        });

        return thisView;
    }

    private void refreshView() {
        Parameters parameters = mainActivity.getHelper().getParameters();
        HashMap<Integer, ArrayList<Integer>> matching = parameters.getMatching();

        dataTable.removeAllViews();
        TableRow header = new TableRow(mainActivity);
        header.setGravity(Gravity.CENTER);
        header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

        Space headerSpace = new Space(mainActivity);
        headerSpace.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        header.addView(headerSpace);
        for (int i = 0; i < matching.get(0).size(); i++) {
            TextView cell = new TextView(mainActivity);
            cell.setGravity(Gravity.CENTER);
            cell.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
            cell.setText(String.valueOf(i + 1));
            cell.setBackgroundColor(getResources().getColor(R.color.colorBlue));
            header.addView(cell);
        }

        dataTable.addView(header);


        for (int i = 0; i < matching.size(); i++) {
            ArrayList<Integer> matches = matching.get(i);
            TableRow row = new TableRow(mainActivity);
            TextView indicator = new TextView(mainActivity);

            row.setGravity(Gravity.CENTER);
            row.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

            indicator.setGravity(Gravity.CENTER);
            indicator.setText(String.valueOf(i));
            indicator.setBackgroundColor(getResources().getColor(R.color.colorBlue));
            indicator.setPadding(10, 10, 10, 10);
            row.addView(indicator);

            for (int match : matches) {
                EditText cell = new EditText(mainActivity);
                cell.setGravity(Gravity.CENTER);
                cell.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
                cell.setInputType(InputType.TYPE_CLASS_PHONE);
                cell.setText(String.valueOf(match));


                row.addView(cell);
            }

            dataTable.addView(row);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshView();
    }
}
