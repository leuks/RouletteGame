package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;

/**
 * Created by Leuks on 23/12/2016.
 */

public class EntryChoiceFragment extends Fragment {
    private MainActivity mainActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.fragment_entry_choice, container, false);

        //components
        final Button manualButton = (Button) thisView.findViewById(R.id.button_manual_entry);
        Button autoButton = (Button) thisView.findViewById(R.id.button_auto_entry);

        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        autoButton.setTypeface(fontButton);
        manualButton.setTypeface(fontButton);

        //listeners
        manualButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.show(MainActivity.ROAD_ENTRY_MANUAL, true);
            }
        });

        autoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.show(MainActivity.ROAD_ENTRY_AUTO, true);
            }
        });

        return thisView;
    }
}
