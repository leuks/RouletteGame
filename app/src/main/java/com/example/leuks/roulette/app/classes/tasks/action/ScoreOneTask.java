package com.example.leuks.roulette.app.classes.tasks.action;

import android.os.AsyncTask;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.app.classes.Work;
import com.example.leuks.roulette.commons.bdd.BestTargetItem;
import com.example.leuks.roulette.commons.bdd.Parameters;
import com.example.leuks.roulette.commons.bdd.ScoreOneItem;
import com.example.leuks.roulette.commons.entities.Tuple;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by Leuks on 05/12/2016.
 */

public class ScoreOneTask extends AsyncTask<Void, Void, Void> {
    private MainActivity mainActivity;
    private HashMap<Integer, ArrayList<ScoreOneItem>> bestScoreOnes;
    private Work work;
    private Parameters parameters;
    private HashMap<Integer, ArrayList<BestTargetItem>> bestTargetItems;

    public ScoreOneTask(MainActivity mainActivity, HashMap<Integer, ArrayList<BestTargetItem>> bestTargetItems, HashMap<Integer, ArrayList<ScoreOneItem>> bestScoreOnes, Work work) {
        this.mainActivity = mainActivity;
        this.work = work;
        this.bestScoreOnes = bestScoreOnes;
        this.parameters = mainActivity.getHelper().getParameters();
        this.bestTargetItems = bestTargetItems;
    }

    @Override
    protected Void doInBackground(Void... params) {
        if (isCancelled()) return null;
        for (int i = 0; i < parameters.getOccurencesNumber(); i++) {
            BestTargetItem bestTargetItem = bestTargetItems.get(i).get(bestTargetItems.get(i).size() - 1);

            ScoreOneItem scoreOneItem;
            if (bestScoreOnes.get(i) == null) {
                ArrayList toPut = new ArrayList();
                this.bestScoreOnes.put(i, toPut);
                scoreOneItem = new ScoreOneItem(bestTargetItem, true);
            } else
                scoreOneItem = new ScoreOneItem(bestTargetItem, bestScoreOnes.get(i).size() % 2 == 0);

            this.bestScoreOnes.get(i).add(scoreOneItem);

            HashMap<Integer, Tuple<Integer, Integer>> functions = scoreOneItem.getFunctions();
            ScoreOneItem lastScoreOneItem = null;
            HashMap<Integer, Tuple<Integer, Integer>> lastFunctions = null;
            if (bestScoreOnes.get(i).size() > 1) {
                lastScoreOneItem = bestScoreOnes.get(i).get(bestScoreOnes.get(i).size() - 2);
                lastFunctions = lastScoreOneItem.getFunctions();
            }
            ArrayList<ScoreOneItem> lastsSuccess = getLastsSuccess(i);
            Tuple<Integer, Integer> f1 = new Tuple<>(null, null);
            Tuple<Integer, Integer> f2 = new Tuple<>(null, null);
            Tuple<Integer, Integer> f3 = new Tuple<>(null, null);
            Tuple<Integer, Integer> f4 = new Tuple<>(null, null);

            //function 1
            if (lastScoreOneItem != null) {
                if (lastScoreOneItem.getBestTargetItem().isSuccess()) {
                    f1.setArg1(1);
                } else {
                    f1.setArg1(lastFunctions.get(ScoreOneItem.F1).getArg1() + 1);
                }
            } else {
                f1.setArg1(1);
            }
            //findDenombrement(f1, i);
            functions.put(ScoreOneItem.F1, f1);


            //proximity
            if (!lastsSuccess.isEmpty()) {
                ScoreOneItem lastSuccess = lastsSuccess.get(lastsSuccess.size() - 1);
                scoreOneItem.setProximity(getProximityOf(scoreOneItem, lastSuccess));

                if (isValidFrom(i)) {
                    //function 2
                    ScoreOneItem lastSuccessF2 = lastsSuccess.get(lastsSuccess.size() - 1);
                    if (lastSuccessF2.getProximity() != null) {
                        boolean sameProximity = haveSameProximity(scoreOneItem, lastSuccessF2);
                        if (sameProximity) {
                            ScoreOneItem lastSameValue = findLastValue(ScoreOneItem.F2_1, i);
                            if (lastSameValue == null) {
                                f2.setArg1(1);
                                if (!existsValue(scoreOneItem, f2.getArg1()))
                                    findDenombrement(f2, i);
                            } else {
                                if (lastSameValue.getBestTargetItem().isSuccess()) {
                                    f2.setArg1(1);
                                    if (!existsValue(scoreOneItem, f2.getArg1()))
                                        findDenombrement(f2, i);
                                } else {
                                    f2.setArg1(lastSameValue.getFunctions().get(ScoreOneItem.F2_1).getArg1() + 1);
                                    if (!existsValue(scoreOneItem, f2.getArg1()))
                                        findDenombrement(f2, i);
                                }
                            }
                            functions.put(ScoreOneItem.F2_1, f2);
                        } else {
                            ScoreOneItem lastSameValue = findLastValue(ScoreOneItem.F2_2, i);
                            if (lastSameValue == null) {
                                f2.setArg1(1);
                                if (!existsValue(scoreOneItem, f2.getArg1()))
                                    findDenombrement(f2, i);
                            } else {
                                if (lastSameValue.getBestTargetItem().isSuccess()) {
                                    f2.setArg1(1);
                                    if (!existsValue(scoreOneItem, f2.getArg1()))
                                        findDenombrement(f2, i);
                                } else {
                                    f2.setArg1(lastSameValue.getFunctions().get(ScoreOneItem.F2_2).getArg1() + 1);
                                    if (!existsValue(scoreOneItem, f2.getArg1()))
                                        findDenombrement(f2, i);
                                }
                            }
                            functions.put(ScoreOneItem.F2_2, f2);
                        }
                    }

                    //function 3
                    if (lastsSuccess.size() > 1) {
                        ScoreOneItem lastSuccessF3 = lastsSuccess.get(lastsSuccess.size() - 2);
                        if (lastSuccessF3.getProximity() != null) {
                            boolean sameProximity = haveSameProximity(scoreOneItem, lastSuccessF3);
                            if (sameProximity) {
                                ScoreOneItem lastSameValue = findLastValue(ScoreOneItem.F3_1, i);
                                if (lastSameValue == null) {
                                    f3.setArg1(1);
                                    if (!existsValue(scoreOneItem, f3.getArg1()))
                                        findDenombrement(f3, i);
                                } else {
                                    if (lastSameValue.getBestTargetItem().isSuccess()) {
                                        f3.setArg1(1);
                                        if (!existsValue(scoreOneItem, f3.getArg1()))
                                            findDenombrement(f3, i);
                                    } else {
                                        f3.setArg1(lastSameValue.getFunctions().get(ScoreOneItem.F3_1).getArg1() + 1);
                                        if (!existsValue(scoreOneItem, f3.getArg1()))
                                            findDenombrement(f3, i);
                                    }
                                }
                                functions.put(ScoreOneItem.F3_1, f3);
                            } else {
                                ScoreOneItem lastSameValue = findLastValue(ScoreOneItem.F3_2, i);
                                if (lastSameValue == null) {
                                    f3.setArg1(1);
                                    if (!existsValue(scoreOneItem, f3.getArg1()))
                                        findDenombrement(f3, i);
                                } else {
                                    if (lastSameValue.getBestTargetItem().isSuccess()) {
                                        f3.setArg1(1);
                                        if (!existsValue(scoreOneItem, f3.getArg1()))
                                            findDenombrement(f3, i);
                                    } else {
                                        f3.setArg1(lastSameValue.getFunctions().get(ScoreOneItem.F3_2).getArg1() + 1);
                                        if (!existsValue(scoreOneItem, f3.getArg1()))
                                            findDenombrement(f3, i);
                                    }
                                }
                                functions.put(ScoreOneItem.F3_2, f3);
                            }
                        }
                    }

                    //function 4
                    if (lastsSuccess.size() > 1) {
                        ScoreOneItem lastSuccessF4 = lastsSuccess.get(lastsSuccess.size() - 1);
                        ScoreOneItem lastLastSuccessF4 = lastsSuccess.get(lastsSuccess.size() - 2);
                        if (lastLastSuccessF4.getProximity() != null) {
                            Boolean whereToStart = null;
                            boolean sameProximity = haveSameProximity(lastSuccessF4, lastLastSuccessF4);
                            Tuple<Boolean, Boolean> lastProximity = getLastProximity(i);

                            if (!lastProximity.getArg2()) {
                                whereToStart = !lastProximity.getArg1();
                            } else {
                                if (sameProximity) {
                                    whereToStart = ScoreOneItem.COL1;
                                } else {
                                    whereToStart = ScoreOneItem.COL2;
                                }
                            }

                            if (whereToStart == ScoreOneItem.COL1) {
                                ScoreOneItem lastSameValue = findLastValue(ScoreOneItem.F4_1, i);
                                if (lastSameValue == null) {
                                    f4.setArg1(1);
                                    if (!existsValue(scoreOneItem, f4.getArg1()))
                                        findDenombrement(f4, i);
                                } else {
                                    if (lastSameValue.getBestTargetItem().isSuccess()) {
                                        f4.setArg1(1);
                                        if (!existsValue(scoreOneItem, f4.getArg1()))
                                            findDenombrement(f4, i);
                                    } else {
                                        f4.setArg1(lastSameValue.getFunctions().get(ScoreOneItem.F4_1).getArg1() + 1);
                                        if (!existsValue(scoreOneItem, f4.getArg1()))
                                            findDenombrement(f4, i);
                                    }
                                }
                                functions.put(ScoreOneItem.F4_1, f4);
                            } else {
                                ScoreOneItem lastSameValue = findLastValue(ScoreOneItem.F4_2, i);
                                if (lastSameValue == null) {
                                    f4.setArg1(1);
                                    if (!existsValue(scoreOneItem, f4.getArg1()))
                                        findDenombrement(f4, i);
                                } else {
                                    if (lastSameValue.getBestTargetItem().isSuccess()) {
                                        f4.setArg1(1);
                                        if (!existsValue(scoreOneItem, f4.getArg1()))
                                            findDenombrement(f4, i);
                                    } else {
                                        f4.setArg1(lastSameValue.getFunctions().get(ScoreOneItem.F4_2).getArg1() + 1);
                                        if (!existsValue(scoreOneItem, f4.getArg1()))
                                            findDenombrement(f4, i);
                                    }
                                }
                                functions.put(ScoreOneItem.F4_2, f4);
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    private Tuple<Boolean, Boolean> getLastProximity(int o) {
        ScoreOneItem lastScoreneItem = bestScoreOnes.get(o).get(bestScoreOnes.get(o).size() - 2);
        return new Tuple<>(lastScoreneItem.isColTwo(), lastScoreneItem.getBestTargetItem().isSuccess());
    }

    private ScoreOneItem findLastValue(int colonne, int o) {
        for (int i = bestScoreOnes.get(o).size() - 2; i >= 0; i--) {
            ScoreOneItem indexedScoreOneItem = bestScoreOnes.get(o).get(i);
            if (indexedScoreOneItem.getFunctions().get(colonne) != null) {
                return indexedScoreOneItem;
            }
        }
        return null;
    }

    /**
     * @param actual
     * @param last
     * @return the proximity beetween two scoreOneItem
     */
    private Boolean getProximityOf(ScoreOneItem actual, ScoreOneItem last) {
        return actual.isMatrice() == last.isMatrice();
    }

    /**
     * @param actual
     * @param last
     * @return return true if both ScoreOneitem have the same proximity, else otherwise
     */
    private Boolean haveSameProximity(ScoreOneItem actual, ScoreOneItem last) {
        return actual.getProximity() == last.getProximity();
    }

    /**
     * @param o
     * @return true if the ScoreOneItem for this occurence is valid (cf specifications )
     */
    private boolean isValidFrom(int o) {
        ScoreOneItem scoreOneItem = bestScoreOnes.get(o).get(bestScoreOnes.get(o).size() - 1);
        int count = 0;
        Boolean result = false;
        for (int i = bestScoreOnes.get(o).size() - 2; i >= 0; i--) {
            ScoreOneItem testedScoreOneItem = bestScoreOnes.get(o).get(i);
            if (scoreOneItem.isMatrice() == testedScoreOneItem.isMatrice()) {
                if (testedScoreOneItem.getBestTargetItem().isSuccess()) {
                    result = true;
                    break;
                } else {
                    count++;
                }
                if (count > 2) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * @param scoreOneItem
     * @param value
     * @return true if the value already checked for this scoreOneItem (cf specifications)
     */
    private boolean existsValue(ScoreOneItem scoreOneItem, Integer value) {
        for (int i = ScoreOneItem.F2_1; i < (ScoreOneItem.F4_2 + 1); i++) {
            if (scoreOneItem.getFunctions().get(i) != null) {
                if (value.equals(scoreOneItem.getFunctions().get(i).getArg1())) return true;
            }
        }
        return false;
    }


    /**
     * Set the last denombrement into the tuple for an given occurence (o)
     *
     * @param f
     * @param o
     */
    private void findDenombrement(Tuple<Integer, Integer> f, int o) {
        for (int i = bestScoreOnes.get(o).size() - 2; i >= 0; i--) {
            ScoreOneItem indexedScoreOneItem = bestScoreOnes.get(o).get(i);
            for (int j = ScoreOneItem.F1; j < (ScoreOneItem.F4_2 + 1); j++) {
                if (indexedScoreOneItem.getFunctions().get(j) != null && indexedScoreOneItem.getFunctions().get(j).getArg2() != null) {
                    if (f.getArg1().equals(indexedScoreOneItem.getFunctions().get(j).getArg1())) {
                        if (indexedScoreOneItem.getBestTargetItem().isSuccess()) {
                            f.setArg2(1);
                        } else {
                            f.setArg2(indexedScoreOneItem.getFunctions().get(j).getArg2() + 1);
                        }
                        return;
                    }
                }
            }
        }
        f.setArg2(1);
    }

    /**
     * @param o
     * @return The last success for the given occurence
     */
    private ArrayList<ScoreOneItem> getLastsSuccess(int o) {
        ArrayList<ScoreOneItem> successToReturn = new ArrayList<>();
        for (int i = bestScoreOnes.get(o).size() - 2; i >= 0; i--) {
            ScoreOneItem testedScoreOneItem = bestScoreOnes.get(o).get(i);
            if (testedScoreOneItem.getBestTargetItem().isSuccess()) {
                successToReturn.add(testedScoreOneItem);
                if (successToReturn.size() > 1)
                    break;
            }
        }
        Collections.reverse(successToReturn);
        return successToReturn;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        work.addScoreOne(bestScoreOnes);
        cancel(true);
    }
}
