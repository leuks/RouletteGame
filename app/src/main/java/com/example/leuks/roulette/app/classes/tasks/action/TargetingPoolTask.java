package com.example.leuks.roulette.app.classes.tasks.action;

import android.os.AsyncTask;
import android.os.Message;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.app.classes.UtilObj.AddHandlerObject;
import com.example.leuks.roulette.app.classes.Work;
import com.example.leuks.roulette.commons.bdd.Parameters;
import com.example.leuks.roulette.commons.bdd.Target;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Leuks on 21/12/2016.
 */

public class TargetingPoolTask extends AsyncTask<Void, Void, HashMap<Integer, HashMap<Integer, ArrayList<Target>>>> {
    private MainActivity mainActivity;
    private int shot;
    private Work work;
    private Integer simpleChance;
    private int lastID;
    private int lastBestID;

    public TargetingPoolTask(MainActivity mainActivity, int shot, Integer simpleChance, Work work) {
        this.mainActivity = mainActivity;
        this.shot = shot;
        this.work = work;
        this.simpleChance = simpleChance;
    }

    @Override
    protected HashMap<Integer, HashMap<Integer, ArrayList<Target>>> doInBackground(Void... params) {
        if (isCancelled()) return null;
        if (work.getAllTargetsAsHashMap() == null) {
            work.setLastId(mainActivity.getHelper().getLastTargetInd());
            work.setLastBestId(mainActivity.getHelper().getLastBestTargetItemId());
            lastID = work.getLastId();
            lastBestID = work.getLastBestId();
            return mainActivity.getHelper().getAllTargetsAsHashMap();
        } else {
            //ids setted by task
            lastID = work.getLastId();
            lastBestID = work.getLastBestId();
            return work.getAllTargetsAsHashMap();
        }
    }

    @Override
    protected void onPostExecute(HashMap<Integer, HashMap<Integer, ArrayList<Target>>> allTargets) {
        super.onPostExecute(allTargets);
        Parameters parameters = mainActivity.getHelper().getParameters();
        work.setAllTargetsAsHashMap(allTargets);

        for (int i = 0; i < parameters.getTabsNumber(); i++) {
            HashMap<Integer, ArrayList<Target>> occurence = allTargets.get(i);
            if (occurence == null) {
                occurence = new HashMap<>();
                allTargets.put(i, occurence);
                work.setAllTargetsAsHashMap(allTargets);
            }

            AddHandlerObject obj = new AddHandlerObject(shot, simpleChance, i, occurence, work, lastID, lastBestID);
            Message m = mainActivity.addMatchingTask.tHandler.obtainMessage();
            m.obj = obj;
            mainActivity.addMatchingTask.tHandler.dispatchMessage(m);
        }
        cancel(true);
    }
}
