package com.example.leuks.roulette.commons.bdd;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Leuks on 24/11/2016.
 */

@DatabaseTable(tableName = "Target")
public class Target implements Serializable {
    @DatabaseField(id = true)
    private int id;
    @DatabaseField
    private int shot;
    @DatabaseField
    private int match;
    @DatabaseField(columnName = "indoccurence")
    private int indOccurence;
    @DatabaseField
    private int indTab;
    @DatabaseField
    private int firstChance;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private ArrayList<Integer> occurence;

    public Target() {
    }

    public Target(int shot, int match, int indOccurence, int indTab, int firstChance, ArrayList<Integer> occurence, int id) {
        this.shot = shot;
        this.match = match;
        this.firstChance = firstChance;
        this.indOccurence = indOccurence;
        this.occurence = occurence;
        this.indTab = indTab;
        this.id = id;
    }

    //GS
    public int getShot() {
        return shot;
    }

    public int getMatch() {
        return match;
    }

    public int getIndOccurence() {
        return indOccurence;
    }

    public int getFirstChance() {
        return firstChance;
    }

    public ArrayList<Integer> getOccurence() {
        return occurence;
    }

    @Override
    public boolean equals(Object object) {
        Target target = (Target) object;
        return target.id == this.id;
    }

    public int getIndTab() {
        return indTab;
    }

    public int getId() {
        return id;
    }

}
