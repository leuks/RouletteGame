package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.app.classes.tasks.action.PutDataTask;

/**
 * Created by Leuks on 01/02/2017.
 */

public class HistoryScoreFraqment extends Fragment {
    private MainActivity mainActivity;
    private LinearLayout scoreOneScrollLayout;
    private LinearLayout scoreTwoScrollLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.fragment_history_scores_layout, container, false);

        //components
        TextView scoreOneTextViewShow = (TextView) thisView.findViewById(R.id.textView_score_one_show);
        TextView scoreTwoTextViewShow = (TextView) thisView.findViewById(R.id.textView_score_two_show);
        scoreOneScrollLayout = (LinearLayout) thisView.findViewById(R.id.scroll_score_one_layout);
        scoreTwoScrollLayout = (LinearLayout) thisView.findViewById(R.id.scroll_score_two_layout);

        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        scoreOneTextViewShow.setTypeface(fontButton);
        scoreTwoTextViewShow.setTypeface(fontButton);

        return thisView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        PutDataTask putDataTask = new PutDataTask(mainActivity, scoreOneScrollLayout, PutDataTask.MODE_SCORES, false);
        putDataTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        PutDataTask putDataTask2 = new PutDataTask(mainActivity, scoreTwoScrollLayout, PutDataTask.MODE_SCORES, true);
        putDataTask2.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
