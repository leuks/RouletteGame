package com.example.leuks.roulette.app.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.commons.bdd.ProfilItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leuks on 12/12/2016.
 */

public class ParametersFragment extends Fragment {
    private MainActivity mainActivity;
    private Spinner spinnerProfil;
    private Snackbar snack;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.fragment_parameters_layout, container, false);

        //childs
        Button algorithmButton = (Button) thisView.findViewById(R.id.button_parameters_algorithm);
        Button dataButton = (Button) thisView.findViewById(R.id.button_parameters_data);
        Button profilbutton = (Button) thisView.findViewById(R.id.button_profile);

        //font
        final Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        algorithmButton.setTypeface(fontButton);
        dataButton.setTypeface(fontButton);
        profilbutton.setTypeface(fontButton);

        //listeners
        algorithmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.show(MainActivity.ROAD_PARAMETERS_ALGORITHM, true);
            }
        });

        dataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.show(MainActivity.ROAD_PARAMETERS_CHOICE, true);
            }
        });

        profilbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog mainDialog = new Dialog(mainActivity);
                mainDialog.setContentView(R.layout.dialog_profile_layout);
                ProfilItem p = mainActivity.getHelper().getCurrentProfilItem();

                //components
                TextView tv = (TextView) mainDialog.findViewById(R.id.tv);
                spinnerProfil = (Spinner) mainDialog.findViewById(R.id.spinner_profil);
                Button registerButton = (Button) mainDialog.findViewById(R.id.button_register_profile);
                Button loadButton = (Button) mainDialog.findViewById(R.id.button_load_profile);
                Button removeButton = (Button) mainDialog.findViewById(R.id.button_remove_profile);

                final List<ProfilItem> profilList = mainActivity.getHelper().getAllProfilItems();
                final List<String> profilNames = new ArrayList<>();

                for (ProfilItem pi : profilList) {
                    profilNames.add(pi.getName());
                }

                final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mainActivity,
                        android.R.layout.simple_spinner_item, profilNames);

                spinnerProfil.setAdapter(dataAdapter);
                dataAdapter.notifyDataSetChanged();

                for (int i = 0; i < spinnerProfil.getAdapter().getCount(); i++) {
                    String s = (String) spinnerProfil.getAdapter().getItem(i);
                    if (s.equals(p.getName()))
                        spinnerProfil.setSelection(i);
                }

                //font
                tv.setTypeface(fontButton);
                registerButton.setTypeface(fontButton);
                loadButton.setTypeface(fontButton);
                removeButton.setTypeface(fontButton);

                //listeners
                registerButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                        builder.setTitle("Nom du nouveau profil");
                        final EditText newValueED = new EditText(mainActivity);
                        newValueED.setGravity(Gravity.CENTER);
                        builder.setView(newValueED);
                        builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String newValue = newValueED.getText().toString().trim();
                                profilNames.add(newValue);
                                dataAdapter.notifyDataSetChanged();
                                snack = Snackbar.make(thisView, "Nouveau profil: " + newValue, Snackbar.LENGTH_LONG);
                                mainActivity.getHelper().createProfilItem(new ProfilItem(newValue, mainActivity.getHelper().getParameters(), false));
                            }
                        });
                        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });

                        builder.show();
                    }
                });

                loadButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String name = (String) spinnerProfil.getSelectedItem();
                        mainActivity.getHelper().changeCurrentProfilItem(name);

                        for (int i = 0; i < spinnerProfil.getChildCount(); i++) {
                            String s = (String) spinnerProfil.getAdapter().getItem(i);
                            if (s.equals(name))
                                spinnerProfil.setSelection(i);
                        }

                        snack = Snackbar.make(thisView, "Profil chargé: " + name, Snackbar.LENGTH_LONG);
                        mainDialog.dismiss();
                        snack.show();
                    }
                });

                removeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String name = (String) spinnerProfil.getSelectedItem();

                        final AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                        builder.setTitle("Supprimer le profil '"+name+"' ?");
                        builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        int pos = spinnerProfil.getSelectedItemPosition();

                                        if(spinnerProfil.getAdapter().getCount() == 1){
                                            snack = Snackbar.make(thisView, "Suppresion impossible, reste 1 profil", Snackbar.LENGTH_LONG);
                                            snack.show();
                                        }
                                        else{

                                            String nextName;
                                            if(pos >= spinnerProfil.getAdapter().getCount() -1){
                                                nextName = (String) spinnerProfil.getAdapter().getItem(0);
                                            }
                                            else{
                                                nextName = (String) spinnerProfil.getAdapter().getItem(pos + 1);
                                            }


                                            mainActivity.getHelper().changeCurrentProfilItem(nextName);

                                            for(ProfilItem profil : profilList){
                                                if(profil.getName().equals(name)){
                                                    mainActivity.getHelper().removeProfile(profil);
                                                }
                                            }

                                            snack = Snackbar.make(thisView, "Profil '"+name+"' supprimé, actuel: '"+nextName+"'", Snackbar.LENGTH_LONG);
                                            dialog.dismiss();
                                            mainDialog.dismiss();
                                            snack.show();
                                        }
                                    }
                                });

                        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                return;
                            }
                        });
                        builder.show();
                    }
                });

                mainDialog.show();
            }
        });

        return thisView;
    }
}
