package com.example.leuks.roulette.app.classes;

import com.example.leuks.roulette.app.classes.tasks.action.TargetToUpdate;
import com.example.leuks.roulette.commons.bdd.BestTargetItem;
import com.example.leuks.roulette.commons.bdd.Target;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Leuks on 16/02/2017.
 */


public class UtilObj {
    public static class AddHandlerObject {
        private int shot;
        private Integer simpleChance;
        private int indTab;
        private HashMap<Integer, ArrayList<Target>> occurences;
        private Work work;
        private int lastId;
        private int lastBestId;

        public AddHandlerObject(int shot, Integer simpleChance, int indTab, HashMap<Integer, ArrayList<Target>> occurences, Work work, int lastId, int lastBestId) {
            this.shot = shot;
            this.simpleChance = simpleChance;
            this.indTab = indTab;
            this.occurences = occurences;
            this.work = work;
            this.lastId = lastId;
            this.lastBestId = lastBestId;
        }


        //GS
        public int getShot() {
            return shot;
        }

        public Integer getSimpleChance() {
            return simpleChance;
        }

        public int getIndTab() {
            return indTab;
        }

        public HashMap<Integer, ArrayList<Target>> getOccurences() {
            return occurences;
        }

        public Work getWork() {
            return work;
        }

        public int getLastId() {
            return lastId;
        }

        public int getLastBestId() {
            return lastBestId;
        }
    }

    public static class ReturnHandlerObject {
        private HashMap<Integer, ArrayList<BestTargetItem>> exitOccurence;
        private HashMap<Integer, ArrayList<Target>> occurences;
        private ArrayList<TargetToUpdate> targetsToUpdate;
        private int indTab;


        public ReturnHandlerObject(int indTab) {
            this.indTab = indTab;
            targetsToUpdate = new ArrayList<>();
        }

        //GS
        public HashMap<Integer, ArrayList<BestTargetItem>> getExitOccurence() {
            return exitOccurence;
        }

        public void setExitOccurence(HashMap<Integer, ArrayList<BestTargetItem>> exitOccurence) {
            this.exitOccurence = exitOccurence;
        }

        public HashMap<Integer, ArrayList<Target>> getOccurences() {
            return occurences;
        }

        public void setOccurences(HashMap<Integer, ArrayList<Target>> occurences) {
            this.occurences = occurences;
        }

        public ArrayList<TargetToUpdate> getTArgetsToUpdate() {
            return targetsToUpdate;
        }

        public void setTArgetsToUpdate(ArrayList<TargetToUpdate> argetsToUpdate) {
            this.targetsToUpdate = argetsToUpdate;
        }

        public int getIndTab() {
            return indTab;
        }
    }
}

