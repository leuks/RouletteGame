package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.app.classes.tasks.action.PutDataTask;

/**
 * Created by Leuks on 01/02/2017.
 */

public class HistoryTargetingFragment extends Fragment {
    private MainActivity mainActivity;
    private LinearLayout targetingScrollLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.history_item_layout, container, false);

        //components
        TextView targetingTextViewShow = (TextView) thisView.findViewById(R.id.textview_title);
        targetingScrollLayout = (LinearLayout) thisView.findViewById(R.id.scroll_layout);

        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        targetingTextViewShow.setTypeface(fontButton);

        return thisView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PutDataTask putDataTask = new PutDataTask(mainActivity, targetingScrollLayout, PutDataTask.MODE_TARGETING, false);
        putDataTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
