package com.example.leuks.roulette.commons.bdd;

import com.example.leuks.roulette.commons.entities.Tuple;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.HashMap;

/**
 * Created by Leuks on 22/12/2016.
 */

@DatabaseTable(tableName = "ScoreOneItem")
public class ScoreOneItem {
    public static final int F1 = 0;
    public static final int F2_1 = 1;
    public static final int F2_2 = 2;
    public static final int F3_1 = 3;
    public static final int F3_2 = 4;
    public static final int F4_1 = 5;
    public static final int F4_2 = 6;
    public static final boolean PROCHE = true;
    public static final boolean COL1 = false;
    public static final boolean COL2 = true;

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(foreign = true, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 5)
    private BestTargetItem bestTargetItem;
    @DatabaseField
    private boolean matriceOrBase;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<Integer, Tuple<Integer, Integer>> functions;
    @DatabaseField
    private Boolean proximity;

    public ScoreOneItem(BestTargetItem bestTargetItem, boolean matriceOrBase) {
        this.bestTargetItem = bestTargetItem;
        this.matriceOrBase = matriceOrBase;
        functions = new HashMap<>();
        proximity = null;
    }

    public ScoreOneItem() {
    }

    //GS
    public BestTargetItem getBestTargetItem() {
        return bestTargetItem;
    }

    public boolean isColTwo() {
        if (functions.get(F4_2) != null) {
            if (functions.get(F4_2).getArg1() != null) return true;
            else return false;
        } else return false;
    }

    public boolean isMatrice() {
        return matriceOrBase;
    }

    public HashMap<Integer, Tuple<Integer, Integer>> getFunctions() {
        return functions;
    }

    public Boolean getProximity() {
        return proximity;
    }

    public void setProximity(Boolean proximity) {
        this.proximity = proximity;
    }
}
