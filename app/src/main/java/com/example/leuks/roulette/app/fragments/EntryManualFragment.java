package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.app.classes.Work;
import com.example.leuks.roulette.commons.entities.InfiniteTuple;

/**
 * Created by Leuks on 22/11/2016.
 */

public class EntryManualFragment extends Fragment {

    private MainActivity mainActivity;
    private TextView oldEntries;
    private TextView currentEntry;
    private Button pad0;
    private Button pad1;
    private Button pad2;
    private Button pad3;
    private Button pad4;
    private Button pad5;
    private Button pad6;
    private Button pad7;
    private Button pad8;
    private Button pad9;
    private Button padValid;
    private Button padErase;
    private InfiniteTuple oldEntrys;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        oldEntrys = new InfiniteTuple();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.fragment_entry_manual_layout, container, false);

        //childs
        pad0 = (Button) thisView.findViewById(R.id.pad0);
        pad1 = (Button) thisView.findViewById(R.id.pad1);
        pad2 = (Button) thisView.findViewById(R.id.pad2);
        pad3 = (Button) thisView.findViewById(R.id.pad3);
        pad4 = (Button) thisView.findViewById(R.id.pad4);
        pad5 = (Button) thisView.findViewById(R.id.pad5);
        pad6 = (Button) thisView.findViewById(R.id.pad6);
        pad7 = (Button) thisView.findViewById(R.id.pad7);
        pad8 = (Button) thisView.findViewById(R.id.pad8);
        pad9 = (Button) thisView.findViewById(R.id.pad9);
        padValid = (Button) thisView.findViewById(R.id.pad_valid);
        padErase = (Button) thisView.findViewById(R.id.pad_erase);
        oldEntries = (TextView) thisView.findViewById(R.id.old_entries);
        currentEntry = (TextView) thisView.findViewById(R.id.current_entry);
        Button removeMomoryButton = (Button) thisView.findViewById(R.id.button_remove_memory);

        //listeners
        pad0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                concatEntry("0");
            }
        });

        pad1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                concatEntry("1");
            }
        });

        pad2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                concatEntry("2");
            }
        });

        pad3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                concatEntry("3");
            }
        });

        pad4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                concatEntry("4");
            }
        });

        pad5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                concatEntry("5");
            }
        });

        pad6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                concatEntry("6");
            }
        });

        pad7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                concatEntry("7");
            }
        });

        pad8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                concatEntry("8");
            }
        });

        pad9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                concatEntry("9");
            }
        });

        padErase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentTextEntry = currentEntry.getText().toString();
                if (currentTextEntry.length() > 0) {
                    currentEntry.setText(currentTextEntry.substring(0, currentTextEntry.length() - 1));
                }
            }
        });

        padErase.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                clearEntry();
                return true;
            }
        });

        padValid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int shot = Integer.parseInt(currentEntry.getText().toString());
                oldEntrys.addLast(shot);
                Work actualWork = mainActivity.getWork(null);
                actualWork.start(shot, null);
            }
        });

        currentEntry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                verifEntry();
            }
        });

        removeMomoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.removeMemory();
            }
        });

        return thisView;
    }

    //methods
    private void verifEntry() {
        String stringEntry = currentEntry.getText().toString();
        for (int i = 0; i <= 9; i++) changeEntryState(i, true);
        if (stringEntry.isEmpty()) changeEntryState(-1, false);
        else {
            int intEntry = Integer.parseInt(currentEntry.getText().toString());
            if (intEntry == 3) {
                for (int i = 7; i <= 9; i++) changeEntryState(i, false);
            } else if ((intEntry >= 4) || (intEntry == 0)) {
                for (int i = 0; i <= 9; i++) changeEntryState(i, false);
            }
            changeEntryState(-1, true);
        }
    }

    private void changeEntryState(int toStop, boolean visible) {
        switch (toStop) {
            case 0:
                if (visible) pad0.setVisibility(View.VISIBLE);
                else pad0.setVisibility(View.INVISIBLE);
                break;
            case 1:
                if (visible) pad1.setVisibility(View.VISIBLE);
                else pad1.setVisibility(View.INVISIBLE);
                break;
            case 2:
                if (visible) pad2.setVisibility(View.VISIBLE);
                else pad2.setVisibility(View.INVISIBLE);
                break;
            case 3:
                if (visible) pad3.setVisibility(View.VISIBLE);
                else pad3.setVisibility(View.INVISIBLE);
                break;
            case 4:
                if (visible) pad4.setVisibility(View.VISIBLE);
                else pad4.setVisibility(View.INVISIBLE);
                break;
            case 5:
                if (visible) pad5.setVisibility(View.VISIBLE);
                else pad5.setVisibility(View.INVISIBLE);
                break;
            case 6:
                if (visible) pad6.setVisibility(View.VISIBLE);
                else pad6.setVisibility(View.INVISIBLE);
                break;
            case 7:
                if (visible) pad7.setVisibility(View.VISIBLE);
                else pad7.setVisibility(View.INVISIBLE);
                break;
            case 8:
                if (visible) pad8.setVisibility(View.VISIBLE);
                else pad8.setVisibility(View.INVISIBLE);
                break;
            case 9:
                if (visible) pad9.setVisibility(View.VISIBLE);
                else pad9.setVisibility(View.INVISIBLE);
                break;
            case -1:
                if (visible) {
                    padValid.setVisibility(View.VISIBLE);
                    padErase.setVisibility(View.VISIBLE);
                } else {
                    padValid.setVisibility(View.INVISIBLE);
                    padErase.setVisibility(View.INVISIBLE);
                }
                break;
        }
    }

    private void concatEntry(String toAdd) {
        currentEntry.setText(currentEntry.getText() + toAdd);
    }

    private void clearEntry() {
        currentEntry.setText("");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisible()) {
            clearEntry();
            verifEntry();
            if (oldEntrys.isEmpty()) {
                oldEntrys = mainActivity.getHelper().getLast3Shots();
            }

            if (oldEntrys.isEmpty()) oldEntries.setText("Aucune donnée");
            else oldEntries.setText(String.valueOf(oldEntrys.toString(true)));
        }
    }

    //GS

    public InfiniteTuple getOldEntrys() {
        return oldEntrys;
    }
}
