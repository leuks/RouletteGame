package com.example.leuks.roulette.app.classes;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.app.classes.tasks.action.LoadDataTask;
import com.example.leuks.roulette.commons.bdd.BestTargetItem;
import com.example.leuks.roulette.commons.bdd.ScoreOneItem;
import com.example.leuks.roulette.commons.bdd.ScoreTwoItem;
import com.example.leuks.roulette.commons.bdd.ValorisationItem;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by leuks on 29/12/2016.
 */

public class DataWork {
    private MainActivity mainActivity;
    private HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> globalTargeting;
    private HashMap<Integer, ArrayList<ScoreOneItem>> scoreOnes;
    private HashMap<Integer, ArrayList<ScoreTwoItem>> scoreTwos;
    private ArrayList<ValorisationItem> valorisations;
    private HashMap<Integer, ArrayList<BestTargetItem>> statItems;

    public DataWork(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void start() {
        LoadDataTask dataLoaderTask = new LoadDataTask(mainActivity);
        dataLoaderTask.executeOnExecutor(MainActivity.executor);
    }

    public void close() {
        mainActivity.show(MainActivity.ROAD_DATA_CHOICE, true);
    }

    public HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> getGlobalTargeting() {
        return globalTargeting;
    }

    //GS
    public void setGlobalTargeting(HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> globalTargeting) {
        this.globalTargeting = globalTargeting;
    }

    public HashMap<Integer, ArrayList<ScoreOneItem>> getScoreOnes() {
        return scoreOnes;
    }

    public void setScoreOnes(HashMap<Integer, ArrayList<ScoreOneItem>> scoreOnes) {
        this.scoreOnes = scoreOnes;
    }

    public HashMap<Integer, ArrayList<ScoreTwoItem>> getScoreTwos() {
        return scoreTwos;
    }

    public void setScoreTwos(HashMap<Integer, ArrayList<ScoreTwoItem>> scoreTwos) {
        this.scoreTwos = scoreTwos;
    }

    public ArrayList<ValorisationItem> getValorisations() {
        return valorisations;
    }

    public void setValorisations(ArrayList<ValorisationItem> valorisations) {
        this.valorisations = valorisations;
    }

    public HashMap<Integer, ArrayList<BestTargetItem>> getStatItems() {
        return statItems;
    }

    public void setStatItems(HashMap<Integer, ArrayList<BestTargetItem>> statItems) {
        this.statItems = statItems;
    }
}
