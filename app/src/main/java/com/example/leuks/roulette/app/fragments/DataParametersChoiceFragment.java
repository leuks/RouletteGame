package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;

/**
 * Created by leuks on 30/12/2016.
 */

public class DataParametersChoiceFragment extends Fragment {
    private MainActivity mainActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.fragment_data_parameters_choice, container, false);

        //components
        Button entriesButton = (Button) thisView.findViewById(R.id.button_choice_entries);
        Button outButton = (Button) thisView.findViewById(R.id.button_choice_out);
        Button traverseButton = (Button) thisView.findViewById(R.id.button_choice_traverse);
        Button mScore1Button = (Button) thisView.findViewById(R.id.button_choice_m_1);
        Button mScore2Button = (Button) thisView.findViewById(R.id.button_choice_m_2);

        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        entriesButton.setTypeface(fontButton);
        outButton.setTypeface(fontButton);
        traverseButton.setTypeface(fontButton);
        mScore1Button.setTypeface(fontButton);
        mScore2Button.setTypeface(fontButton);

        //listeners
        entriesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.show(MainActivity.ROAD_PARAMETERS_ENTRIES, true);
            }
        });

        outButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.show(MainActivity.ROAD_PARAMETERS_OUT, true);
            }
        });

        traverseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.show(MainActivity.ROAD_PARAMETERS_TRAVERSE, true);
            }
        });

        mScore1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.show(MainActivity.ROAD_PARAMETERS_SCORE_ONE, true);
            }
        });

        mScore2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.show(MainActivity.ROAD_PARAMETERS_SCORE_TWO, true);
            }
        });


        return thisView;
    }

}
