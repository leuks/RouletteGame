package com.example.leuks.roulette.app.classes;

import java.io.Serializable;

/**
 * Created by Leuks on 04/01/2017.
 */

public class TraverseItem implements Serializable {
    public static final String INF = "<";
    public static final String SUP = ">";
    public static final String EQ = "=";
    public static final String NONE = "---";
    private Integer val1;
    private Integer val2;
    private Boolean checked1;
    private Boolean checked2;
    private String sign;

    public TraverseItem(Integer val1, Integer val2, Boolean checked1, Boolean checked2, String sign) {
        this.val1 = val1;
        this.val2 = val2;
        this.checked1 = checked1;
        this.checked2 = checked2;
        this.sign = sign;
    }

    public TraverseItem(Integer val1, Integer val2, String sign) {
        this.val1 = val1;
        this.val2 = val2;
        this.sign = sign;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TraverseItem) {
            TraverseItem checkTraverseItem = (TraverseItem) obj;
            boolean toReturn = true;
            if (toReturn) {
                if (val1 == null) toReturn = val1 == checkTraverseItem.getVal1();
                else toReturn = val1.equals(checkTraverseItem.getVal1());
            }
            if (toReturn) {
                if (val2 == null) toReturn = val2 == checkTraverseItem.getVal2();
                else toReturn = val2.equals(checkTraverseItem.getVal2());
            }

            if (toReturn) toReturn = sign.equals(checkTraverseItem.getSign());
            return toReturn;
        } else {
            return super.equals(obj);
        }
    }

    //GS
    public Integer getVal1() {
        return val1;
    }

    public void setVal1(Integer val1) {
        this.val1 = val1;
    }

    public Integer getVal2() {
        return val2;
    }

    public void setVal2(Integer val2) {
        this.val2 = val2;
    }

    public Boolean getChecked1() {
        return checked1;
    }

    public void setChecked1(Boolean checked1) {
        this.checked1 = checked1;
    }

    public Boolean getChecked2() {
        return checked2;
    }

    public void setChecked2(Boolean checked2) {
        this.checked2 = checked2;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}