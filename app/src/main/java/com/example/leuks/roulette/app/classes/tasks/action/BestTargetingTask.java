package com.example.leuks.roulette.app.classes.tasks.action;

import android.os.AsyncTask;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.app.classes.Work;
import com.example.leuks.roulette.commons.bdd.BestTargetItem;
import com.example.leuks.roulette.commons.bdd.Parameters;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Leuks on 21/12/2016.
 */

public class BestTargetingTask extends AsyncTask<Void, Void, HashMap<Integer, ArrayList<BestTargetItem>>> {
    private HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> globalOccurences;
    private MainActivity mainActivity;
    private Work work;
    private Parameters parameters;

    public BestTargetingTask(MainActivity mainActivity, HashMap<Integer, HashMap<Integer, ArrayList<BestTargetItem>>> globalOccurences, Work work) {
        this.globalOccurences = globalOccurences;
        this.mainActivity = mainActivity;
        this.work = work;
        this.parameters = mainActivity.getHelper().getParameters();
    }

    @Override
    protected HashMap<Integer, ArrayList<BestTargetItem>> doInBackground(Void... params) {
        if (isCancelled()) return null;
        HashMap<Integer, ArrayList<BestTargetItem>> lastExitOccurence = new HashMap<>();
        for (int i = 0; i < parameters.getOccurencesNumber(); i++) {
            //init array
            lastExitOccurence.put(i, new ArrayList<BestTargetItem>());
            BestTargetItem maxItem = null;
            int max = 0;
            //Find the best denombrement between all tabs
            for (int t = 0; t < parameters.getTabsNumber(); t++) {
                int j = globalOccurences.get(t).get(i).size() - 1;
                HashMap<Integer, ArrayList<BestTargetItem>> exitOccurences = globalOccurences.get(t);
                ArrayList<BestTargetItem> exitOccurence = exitOccurences.get(i);
                BestTargetItem actualItem = exitOccurence.get(j);
                int actualOccurence = actualItem.getDenombrement();
                if (actualOccurence > max) {
                    max = actualOccurence;
                    maxItem = actualItem;
                }
            }
            lastExitOccurence.get(i).add(maxItem);
        }

        return lastExitOccurence;
    }

    @Override
    protected void onPostExecute(HashMap<Integer, ArrayList<BestTargetItem>> lastExitOccurence) {
        super.onPostExecute(lastExitOccurence);
        work.executeScore(lastExitOccurence);
        cancel(true);
    }
}
