package com.example.leuks.roulette.commons.bdd;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.app.classes.TraverseItem;
import com.example.leuks.roulette.commons.entities.Tuple;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Leuks on 21/11/2016.
 */

/**
 * Contains configurables values.
 * These values could be modified by user.
 */
@DatabaseTable(tableName = "Parameters")
public class Parameters {
    public static final boolean RED = true;
    public static final boolean BLACK = false;

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<Integer, ArrayList<Integer>> matching;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<Integer, Boolean> colors;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private Tuple<Boolean, Boolean> scores;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<Integer, TraverseItem> traverse;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<Integer, Integer> inverses;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<String, Integer> mScoreOne;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<String, Integer> mScoreTwo;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<Integer, ArrayList<Double>> exitMatching;
    @DatabaseField
    private int occurencesNumber;
    @DatabaseField
    private int tabsNumber;

    public Parameters() {
        matching = new HashMap<>();
        exitMatching = new HashMap<>();
        colors = new HashMap<>();
        scores = new Tuple<>();
        traverse = new HashMap<>();
        inverses = new HashMap<>();
        mScoreOne = new HashMap<>();
        mScoreTwo = new HashMap<>();
        init();
    }

    public Parameters(Parameters item) {
        matching = item.getMatching();
        exitMatching = item.getExitMatching();
        colors = item.getColors();
        scores = item.getScores();
        traverse = item.getTraverse();
        inverses = item.getInverses();
        mScoreOne = item.getmScoreOne();
        mScoreTwo = item.getmScoreTwo();
        tabsNumber = item.getTabsNumber();
        occurencesNumber = item.getOccurencesNumber();
    }

    public static boolean existsTraverseItemValue(HashMap<Integer, TraverseItem> traverseBis, TraverseItem checkTraverseItem) {
        for (int key : traverseBis.keySet()) {
            TraverseItem traverseItem = traverseBis.get(key);
            if (traverseItem.equals(checkTraverseItem)) return true;
        }
        return false;
    }

    /**
     * Init HashMaps parameters
     * with default values
     */
    public void init() {

        //init correspondences
        Integer[] t0 = {0, 0, 0, 0, 0, 0, 0, 0, 0};
        Integer[] t1 = {1, 12, 19, 19, 1, 19, 29, 29, 12};
        Integer[] t2 = {2, 12, 30, 29, 1, 19, 29, 20, 2};
        Integer[] t3 = {3, 12, 19, 19, 1, 19, 29, 29, 12};
        Integer[] t4 = {4, 12, 30, 29, 2, 29, 11, 12, 11};
        Integer[] t5 = {5, 12, 19, 19, 2, 29, 11, 1, 1};
        Integer[] t6 = {6, 12, 30, 29, 2, 29, 12, 11, 12};
        Integer[] t7 = {7, 12, 29, 12, 2, 29, 12, 11, 30};
        Integer[] t8 = {8, 12, 20, 2, 2, 29, 12, 2, 20};
        Integer[] t9 = {9, 12, 29, 12, 2, 29, 12, 11, 30};
        Integer[] t10 = {10, 11, 20, 2, 11, 30, 12, 30, 29};
        Integer[] t11 = {11, 11, 29, 2, 11, 30, 12, 19, 29};
        Integer[] t12 = {12, 11, 20, 12, 11, 30, 12, 30, 19};
        Integer[] t13 = {13, 11, 29, 2, 1, 12, 2, 29, 2};
        Integer[] t14 = {14, 11, 20, 12, 1, 12, 2, 20, 12};
        Integer[] t15 = {15, 11, 29, 2, 1, 12, 2, 29, 2};
        Integer[] t16 = {16, 11, 2, 30, 12, 2, 11, 12, 1};
        Integer[] t17 = {17, 11, 11, 20, 12, 2, 11, 1, 11};
        Integer[] t18 = {18, 11, 2, 30, 12, 2, 11, 12, 1};
        Integer[] t19 = {19, 20, 11, 30, 30, 11, 1, 11, 30};
        Integer[] t20 = {20, 20, 2, 20, 30, 11, 1, 2, 20};
        Integer[] t21 = {21, 20, 11, 30, 30, 11, 1, 11, 30};
        Integer[] t22 = {22, 20, 2, 20, 29, 1, 19, 30, 29};
        Integer[] t23 = {23, 20, 11, 30, 29, 1, 19, 19, 19};
        Integer[] t24 = {24, 20, 2, 20, 29, 1, 19, 30, 29};
        Integer[] t25 = {25, 20, 1, 1, 29, 1, 20, 29, 12};
        Integer[] t26 = {26, 20, 12, 11, 29, 1, 20, 20, 2};
        Integer[] t27 = {27, 20, 1, 1, 29, 1, 20, 29, 12};
        Integer[] t28 = {28, 19, 12, 11, 20, 2, 20, 12, 11};
        Integer[] t29 = {29, 19, 1, 11, 20, 2, 20, 1, 11};
        Integer[] t30 = {30, 19, 12, 1, 20, 2, 20, 12, 1};
        Integer[] t31 = {31, 19, 1, 11, 30, 20, 30, 11, 20};
        Integer[] t32 = {32, 19, 12, 1, 30, 20, 30, 2, 30};
        Integer[] t33 = {33, 19, 1, 11, 30, 20, 30, 11, 20};
        Integer[] t34 = {34, 19, 30, 19, 19, 30, 19, 30, 19};
        Integer[] t35 = {35, 19, 19, 29, 19, 30, 19, 19, 29};
        Integer[] t36 = {36, 19, 30, 19, 19, 30, 19, 30, 19};
        matching.put(0, new ArrayList<>(Arrays.asList(t0)));
        matching.put(1, new ArrayList<>(Arrays.asList(t1)));
        matching.put(2, new ArrayList<>(Arrays.asList(t2)));
        matching.put(3, new ArrayList<>(Arrays.asList(t3)));
        matching.put(4, new ArrayList<>(Arrays.asList(t4)));
        matching.put(5, new ArrayList<>(Arrays.asList(t5)));
        matching.put(6, new ArrayList<>(Arrays.asList(t6)));
        matching.put(7, new ArrayList<>(Arrays.asList(t7)));
        matching.put(8, new ArrayList<>(Arrays.asList(t8)));
        matching.put(9, new ArrayList<>(Arrays.asList(t9)));
        matching.put(10, new ArrayList<>(Arrays.asList(t10)));
        matching.put(11, new ArrayList<>(Arrays.asList(t11)));
        matching.put(12, new ArrayList<>(Arrays.asList(t12)));
        matching.put(13, new ArrayList<>(Arrays.asList(t13)));
        matching.put(14, new ArrayList<>(Arrays.asList(t14)));
        matching.put(15, new ArrayList<>(Arrays.asList(t15)));
        matching.put(16, new ArrayList<>(Arrays.asList(t16)));
        matching.put(17, new ArrayList<>(Arrays.asList(t17)));
        matching.put(18, new ArrayList<>(Arrays.asList(t18)));
        matching.put(19, new ArrayList<>(Arrays.asList(t19)));
        matching.put(20, new ArrayList<>(Arrays.asList(t20)));
        matching.put(21, new ArrayList<>(Arrays.asList(t21)));
        matching.put(22, new ArrayList<>(Arrays.asList(t22)));
        matching.put(23, new ArrayList<>(Arrays.asList(t23)));
        matching.put(24, new ArrayList<>(Arrays.asList(t24)));
        matching.put(25, new ArrayList<>(Arrays.asList(t25)));
        matching.put(26, new ArrayList<>(Arrays.asList(t26)));
        matching.put(27, new ArrayList<>(Arrays.asList(t27)));
        matching.put(28, new ArrayList<>(Arrays.asList(t28)));
        matching.put(29, new ArrayList<>(Arrays.asList(t29)));
        matching.put(30, new ArrayList<>(Arrays.asList(t30)));
        matching.put(31, new ArrayList<>(Arrays.asList(t31)));
        matching.put(32, new ArrayList<>(Arrays.asList(t32)));
        matching.put(33, new ArrayList<>(Arrays.asList(t33)));
        matching.put(34, new ArrayList<>(Arrays.asList(t34)));
        matching.put(35, new ArrayList<>(Arrays.asList(t35)));
        matching.put(36, new ArrayList<>(Arrays.asList(t36)));

        //init colors
        colors.put(1, RED);
        colors.put(2, BLACK);
        colors.put(3, RED);
        colors.put(4, BLACK);
        colors.put(5, RED);
        colors.put(6, BLACK);
        colors.put(7, RED);
        colors.put(8, BLACK);
        colors.put(9, RED);
        colors.put(10, BLACK);
        colors.put(11, BLACK);
        colors.put(12, RED);
        colors.put(13, BLACK);
        colors.put(14, RED);
        colors.put(15, BLACK);
        colors.put(16, RED);
        colors.put(17, BLACK);
        colors.put(18, RED);
        colors.put(19, RED);
        colors.put(20, BLACK);
        colors.put(21, RED);
        colors.put(22, BLACK);
        colors.put(23, RED);
        colors.put(24, BLACK);
        colors.put(25, RED);
        colors.put(26, BLACK);
        colors.put(27, RED);
        colors.put(28, BLACK);
        colors.put(29, BLACK);
        colors.put(30, RED);
        colors.put(31, BLACK);
        colors.put(32, RED);
        colors.put(33, BLACK);
        colors.put(34, RED);
        colors.put(35, BLACK);
        colors.put(36, RED);

        //traverse
        TraverseItem traverseItem = new TraverseItem(null, null, false, true, TraverseItem.NONE);
        traverse.put(0, traverseItem);

        traverseItem = new TraverseItem(1, 1, true, false, TraverseItem.EQ);
        traverse.put(4, traverseItem);

        traverseItem = new TraverseItem(1, 3, true, false, TraverseItem.SUP);
        traverse.put(1, traverseItem);

        traverseItem = new TraverseItem(2, 3, true, false, TraverseItem.SUP);
        traverse.put(2, traverseItem);

        traverseItem = new TraverseItem(3, null, true, false, TraverseItem.NONE);
        traverse.put(3, traverseItem);


        //inverses
        inverses.put(MainActivity.NR_SIMPLE_CHANCE, MainActivity.RE_SIMPLE_CHANCE);
        inverses.put(MainActivity.RE_SIMPLE_CHANCE, MainActivity.NR_SIMPLE_CHANCE);
        inverses.put(MainActivity.PR_SIMPLE_CHANCE, MainActivity.IR_SIMPLE_CHANCE);
        inverses.put(MainActivity.IR_SIMPLE_CHANCE, MainActivity.PR_SIMPLE_CHANCE);
        inverses.put(MainActivity.ME_SIMPLE_CHANCE, MainActivity.PE_SIMPLE_CHANCE);
        inverses.put(MainActivity.PE_SIMPLE_CHANCE, MainActivity.ME_SIMPLE_CHANCE);

        //mTables
        //ONE
        mScoreOne.put("1.4", 1);
        mScoreOne.put("2.3", 1);
        mScoreOne.put("3.2", 1);
        mScoreOne.put("4.1", 1);
        mScoreOne.put("5.N", 1);

        mScoreOne.put("1.5", 2);
        mScoreOne.put("2.4", 2);
        mScoreOne.put("3.3", 2);
        mScoreOne.put("4.2", 2);
        mScoreOne.put("6.N", 2);

        mScoreOne.put("1.6", 4);
        mScoreOne.put("2.5", 4);
        mScoreOne.put("3.4", 4);
        mScoreOne.put("4.3", 4);
        mScoreOne.put("7.N", 4);

        mScoreOne.put("1.7", 8);
        mScoreOne.put("2.6", 8);
        mScoreOne.put("3.5", 8);
        mScoreOne.put("4.4", 8);
        mScoreOne.put("8.N", 8);

        mScoreOne.put("<", 0);

        mScoreOne.put(">", 1);

        //TWO
        mScoreTwo.put("1.4", 1);
        mScoreTwo.put("2.3", 1);
        mScoreTwo.put("3.2", 1);
        mScoreTwo.put("4.1", 1);

        mScoreTwo.put("1.5", 2);
        mScoreTwo.put("2.4", 2);
        mScoreTwo.put("3.3", 2);
        mScoreTwo.put("4.2", 2);

        mScoreTwo.put("1.6", 4);
        mScoreTwo.put("2.5", 4);
        mScoreTwo.put("3.4", 4);
        mScoreTwo.put("4.3", 4);

        mScoreTwo.put("<", 0);

        mScoreTwo.put(">", 1);

        //exitMatching
        Double[] emNR = {(double) MainActivity.NR_SIMPLE_CHANCE, 12.6, 9.6, (double) MainActivity.NR_SIMPLE_CHANCE, 6.3, 6.2, 3.2, 3.1, (double) MainActivity.NR_SIMPLE_CHANCE};
        Double[] emRE = {(double) MainActivity.RE_SIMPLE_CHANCE, 30.6, 27.6, (double) MainActivity.RE_SIMPLE_CHANCE, 15.3, 12.2, 9.2, 6.1, (double) MainActivity.RE_SIMPLE_CHANCE};
        Double[] emPR = {(double) MainActivity.PR_SIMPLE_CHANCE, 3.3, (double) MainActivity.PR_SIMPLE_CHANCE, 9.6, 6.2, 12.3, 9.3, (double) MainActivity.PR_SIMPLE_CHANCE, 3.1};
        Double[] emIR = {(double) MainActivity.IR_SIMPLE_CHANCE, 12.3, (double) MainActivity.IR_SIMPLE_CHANCE, 27.6, 12.2, 3.3, 18.3, (double) MainActivity.IR_SIMPLE_CHANCE, 6.1};
        Double[] emME = {(double) MainActivity.ME_SIMPLE_CHANCE, (double) MainActivity.ME_SIMPLE_CHANCE, 18.6, 9.3, (double) MainActivity.ME_SIMPLE_CHANCE, 15.6, 6.6, 6.2, 3.2};
        Double[] emPE = {(double) MainActivity.PE_SIMPLE_CHANCE, (double) MainActivity.PE_SIMPLE_CHANCE, 36.6, 18.3, (double) MainActivity.PE_SIMPLE_CHANCE, 33.6, 24.6, 12.2, 9.2};

        exitMatching.put(MainActivity.NR_SIMPLE_CHANCE, new ArrayList<>(Arrays.asList(emNR)));
        exitMatching.put(MainActivity.RE_SIMPLE_CHANCE, new ArrayList<>(Arrays.asList(emRE)));
        exitMatching.put(MainActivity.PR_SIMPLE_CHANCE, new ArrayList<>(Arrays.asList(emPR)));
        exitMatching.put(MainActivity.IR_SIMPLE_CHANCE, new ArrayList<>(Arrays.asList(emIR)));
        exitMatching.put(MainActivity.ME_SIMPLE_CHANCE, new ArrayList<>(Arrays.asList(emME)));
        exitMatching.put(MainActivity.PE_SIMPLE_CHANCE, new ArrayList<>(Arrays.asList(emPE)));

        //others
        occurencesNumber = 9;
        tabsNumber = 1;
        scores.setArg1(true);
        scores.setArg2(true);
    }

    //GS
    public boolean isRed(int shot) {
        if (shot == 0) return false;
        else return colors.get(shot);
    }

    public ArrayList<Integer> getMatchesOf(int shot) {
        return matching.get(shot);
    }

    public int getOccurencesNumber() {
        return occurencesNumber;
    }

    public void setOccurencesNumber(int occurencesNumber) {
        this.occurencesNumber = occurencesNumber;
    }

    public Tuple<Boolean, Boolean> getScores() {
        return scores;
    }

    public HashMap<Integer, ArrayList<Integer>> getMatching() {
        return matching;
    }

    public void setScoreOne(Boolean wanted) {
        this.scores.setArg1(wanted);
    }

    public void setScoreTwo(Boolean wanted) {
        this.scores.setArg2(wanted);
    }

    public int getTabsNumber() {
        return tabsNumber;
    }

    public void setTabsNumber(int tabsNumber) {
        this.tabsNumber = tabsNumber;
    }

    public HashMap<Integer, TraverseItem> getTraverse() {
        return traverse;
    }

    public HashMap<Integer, Integer> getInverses() {
        return inverses;
    }

    public HashMap<String, Integer> getmScoreOne() {
        return mScoreOne;
    }

    public HashMap<String, Integer> getmScoreTwo() {
        return mScoreTwo;
    }

    public HashMap<Integer, ArrayList<Double>> getExitMatching() {
        return exitMatching;
    }

    public Integer getValuemScoreOneOf(String keyWanted) {
        String warg1 = keyWanted.split("\\.")[0];
        String warg2 = null;
        if (keyWanted.length() > 2) warg2 = keyWanted.split("\\.")[1];
        Integer value = mScoreOne.get(keyWanted);

        if (value == null) {

            //i.N case
            ArrayList<String> itemList = new ArrayList<>();
            for (String key : mScoreOne.keySet()) {
                if (!(key.split("\\.")[0].equals("<") || key.split("\\.")[0].equals(">"))) {
                    itemList.add(key);
                }
            }

            for (String key : itemList) {
                String arg1 = key.split("\\.")[0];
                String arg2 = null;
                if (keyWanted.length() > 2) arg2 = key.split("\\.")[1];

                if (warg1.equals(arg1) && arg2.equals("N")) {
                    value = mScoreOne.get(key);
                    break;
                }
            }
        }

        if (value == null) {

            //i,i in case
            ArrayList<String> itemList = new ArrayList<>();
            for (String key : mScoreOne.keySet()) {
                String arg1 = key.split("\\.")[0];

                if (!(key.split("\\.")[0].equals("<") || key.split("\\.")[0].equals(">"))) {
                    String arg2 = key.split("\\.")[1];

                    if (warg1.equals(arg1) && !(arg2.equals("N"))) {
                        itemList.add(key);
                    }
                }

            }

            int min = Integer.MAX_VALUE;
            int max = 0;
            for (String key : itemList) {
                int arg2 = Integer.parseInt(key.split("\\.")[1]);
                if (arg2 > max) max = arg2;
                if (arg2 < min) min = arg2;

            }

            if (Integer.parseInt(warg2) < min) value = mScoreOne.get("<");
            else if (Integer.parseInt(warg2) > max) value = mScoreOne.get(">");
        }

        if (value == null) {

            //i,i out case

            ArrayList<String> itemList = new ArrayList<>();
            for (String key : mScoreOne.keySet()) {
                if (!(key.split("\\.")[0].equals("<") || key.split("\\.")[0].equals(">"))) {
                    itemList.add(key);
                }
            }

            String min = Integer.MAX_VALUE + "." + Integer.MAX_VALUE;
            String max = "0.0";
            for (String key : itemList) {
                int minArg1 = Integer.parseInt(min.split("\\.")[0]);
                int minArg2 = Integer.parseInt(min.split("\\.")[1]);
                int maxArg1 = Integer.parseInt(max.split("\\.")[0]);
                int maxArg2 = Integer.parseInt(max.split("\\.")[1]);
                int arg1 = Integer.parseInt(key.split("\\.")[0]);
                int arg2;
                if ((key.split("\\.")[1]).equals("N")) arg2 = 0;
                else arg2 = Integer.parseInt(key.split("\\.")[1]);


                if (arg1 > maxArg1) max = key;
                else if (arg1 == maxArg1 && arg2 > maxArg2) max = key;
                if (arg1 < minArg1) min = key;
                else if (arg1 == minArg1 && arg2 < minArg2) min = key;

            }

            if (Double.parseDouble(keyWanted) < Double.parseDouble(min)) value = mScoreOne.get("<");
            else if (Integer.parseInt(warg2) > Double.parseDouble(max)) value = mScoreOne.get(">");
        }

        return value;
    }

    public Integer getKeyFromValue(Integer ar1, Integer arg2, String sign) {
        TraverseItem itemToTest = new TraverseItem(ar1, arg2, sign);
        for (int i : traverse.keySet()) {
            TraverseItem traverseItem = traverse.get(i);
            if (traverseItem.equals(itemToTest)) {
                return i;
            }
        }
        return null;
    }

    public int getMaxKeyOfTraverse() {
        int max = 0;
        for (int i : traverse.keySet()) {
            TraverseItem traverseItem = traverse.get(i);
            if (i > max) max = i;
        }
        return max + 1;
    }

    public Integer getValuemScoreTwoOf(String keyWanted) {

        String warg1 = keyWanted.split("\\.")[0];
        String warg2 = null;
        if (keyWanted.length() > 2) warg2 = keyWanted.split("\\.")[1];
        Integer value = mScoreTwo.get(keyWanted);

        if (value == null) {

            //i.N case
            ArrayList<String> itemList = new ArrayList<>();
            for (String key : mScoreTwo.keySet()) {
                if (!(key.split("\\.")[0].equals("<") || key.split("\\.")[0].equals(">"))) {
                    itemList.add(key);
                }
            }

            for (String key : itemList) {
                String arg1 = key.split("\\.")[0];
                String arg2 = null;
                if (keyWanted.length() > 2) arg2 = key.split("\\.")[1];

                if (warg1.equals(arg1) && arg2.equals("N")) {
                    value = mScoreTwo.get(key);
                    break;
                }
            }
        }

        if (value == null) {

            //i,i in case
            ArrayList<String> itemList = new ArrayList<>();
            for (String key : mScoreTwo.keySet()) {
                String arg1 = key.split("\\.")[0];

                if (!(arg1.equals("<") || arg1.equals(">"))) {
                    String arg2 = key.split("\\.")[1];

                    if (warg1.equals(arg1) && !(arg2.equals("N"))) {
                        itemList.add(key);
                    }
                }

            }

            int min = Integer.MAX_VALUE;
            int max = 0;
            for (String key : itemList) {
                int arg2 = Integer.parseInt(key.split("\\.")[1]);
                if (arg2 > max) max = arg2;
                if (arg2 < min) min = arg2;

            }

            if (Integer.parseInt(warg2) < min) value = mScoreTwo.get("<");
            else if (Integer.parseInt(warg2) > max) value = mScoreTwo.get(">");
        }

        if (value == null) {

            //i,i out case

            ArrayList<String> itemList = new ArrayList<>();
            for (String key : mScoreTwo.keySet()) {
                if (!(key.split("\\.")[0].equals("<") || key.split("\\.")[0].equals(">"))) {
                    itemList.add(key);
                }
            }

            String min = Integer.MAX_VALUE + "." + Integer.MAX_VALUE;
            String max = "0.0";
            for (String key : itemList) {
                int minArg1 = Integer.parseInt(min.split("\\.")[0]);
                int minArg2 = Integer.parseInt(min.split("\\.")[1]);
                int maxArg1 = Integer.parseInt(max.split("\\.")[0]);
                int maxArg2 = Integer.parseInt(max.split("\\.")[1]);
                int arg1 = Integer.parseInt(key.split("\\.")[0]);
                int arg2;
                if ((key.split("\\.")[1]).equals("N")) arg2 = 0;
                else arg2 = Integer.parseInt(key.split("\\.")[1]);


                if (arg1 > maxArg1) max = key;
                else if (arg1 == maxArg1 && arg2 > maxArg2) max = key;
                if (arg1 < minArg1) min = key;
                else if (arg1 == minArg1 && arg2 < minArg2) min = key;

            }

            if (Double.parseDouble(keyWanted) < Double.parseDouble(min)) value = mScoreTwo.get("<");
            else if (Integer.parseInt(warg2) > Double.parseDouble(max)) value = mScoreTwo.get(">");
        }

        return value;
    }

    public boolean existsMScoreOneKey(String keyWanted) {
        return mScoreOne.keySet().contains(keyWanted);
    }

    public boolean existsMScoreTwoKey(String keyWanted) {
        return mScoreTwo.keySet().contains(keyWanted);
    }

    public HashMap<Integer, Boolean> getColors() {
        return colors;
    }

    public int getId() {
        return id;
    }

    public int setId(int id) {
        this.id = id;
        return id;
    }
}