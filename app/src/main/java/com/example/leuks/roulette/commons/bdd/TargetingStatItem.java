package com.example.leuks.roulette.commons.bdd;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Leuks on 11/02/2017.
 */

@DatabaseTable(tableName = "TargetingStatItem")
public class TargetingStatItem {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(foreign = true, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 5)
    private BestTargetItem bestTargetItem;

    public TargetingStatItem(BestTargetItem b) {
        this.bestTargetItem = b;
    }

    public TargetingStatItem() {
    }

    //GS
    public BestTargetItem getBestTargetItem() {
        return bestTargetItem;
    }
}
