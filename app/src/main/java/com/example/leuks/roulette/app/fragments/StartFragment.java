package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.app.classes.DataWork;

/**
 * Created by Leuks on 22/11/2016.
 */

public class StartFragment extends Fragment {
    private MainActivity mainActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.fragment_start_layout, container, false);

        //childs
        Button entryButton = (Button) thisView.findViewById(R.id.button_entry);
        Button parametersButton = (Button) thisView.findViewById(R.id.button_parameters);
        final Button dataButton = (Button) thisView.findViewById(R.id.button_data);

        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        entryButton.setTypeface(fontButton);
        parametersButton.setTypeface(fontButton);
        dataButton.setTypeface(fontButton);

        //listeners
        entryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.show(MainActivity.ROAD_ENTRY_CHOICE, true);
            }
        });

        parametersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.show(MainActivity.ROAD_PARAMETERS, true);
            }
        });

        dataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mainActivity.DBAvalaible()) {
                    Toast.makeText(mainActivity, "Ajouts BD en cours, reste: " + (MainActivity.executorDB.getTaskCount() + MainActivity.sPoolWorkQueueDB.size() - MainActivity.executorDB.getCompletedTaskCount()) + " tâches", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    DataWork dataWork = mainActivity.getDataWork(true);
                    dataWork.start();
                }
            }
        });


        return thisView;
    }
}
