package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;

/**
 * Created by Leuks on 31/01/2017.
 */

public class HistoryChoiceFragment extends Fragment {
    private MainActivity mainActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.fragment_history_choice_layout, container, false);

        //components
        Button targetingButton = (Button) thisView.findViewById(R.id.button_targeting);
        Button scoresButton = (Button) thisView.findViewById(R.id.button_scores);
        Button valorisationButtons = (Button) thisView.findViewById(R.id.button_valorisation);

        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        targetingButton.setTypeface(fontButton);
        scoresButton.setTypeface(fontButton);
        valorisationButtons.setTypeface(fontButton);

        //listeners
        targetingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.show(MainActivity.ROAD_HISTORY_TARGETING, true);
            }
        });

        scoresButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.show(MainActivity.ROAD_HISTORY_SCORES, true);
            }
        });

        valorisationButtons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.show(MainActivity.ROAD_HISTORY_VALORISATION, true);
            }
        });

        return thisView;
    }

}
