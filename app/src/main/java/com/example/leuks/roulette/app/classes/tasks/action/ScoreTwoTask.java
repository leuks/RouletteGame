package com.example.leuks.roulette.app.classes.tasks.action;

import android.os.AsyncTask;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.app.classes.TraverseItem;
import com.example.leuks.roulette.app.classes.Work;
import com.example.leuks.roulette.commons.bdd.BestTargetItem;
import com.example.leuks.roulette.commons.bdd.Parameters;
import com.example.leuks.roulette.commons.bdd.ScoreTwoItem;
import com.example.leuks.roulette.commons.entities.Tuple;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Leuks on 23/12/2016.
 */

public class ScoreTwoTask extends AsyncTask<Void, Void, Void> {
    private HashMap<Integer, ArrayList<ScoreTwoItem>> bestScoreTwos;
    private Work work;
    private MainActivity mainActivity;
    private Parameters parameters;
    private HashMap<Integer, ArrayList<BestTargetItem>> bestTargetItems;

    public ScoreTwoTask(MainActivity mainActivity, HashMap<Integer, ArrayList<BestTargetItem>> bestTargetItems, HashMap<Integer, ArrayList<ScoreTwoItem>> bestScoreTwos, Work work) {
        this.work = work;
        this.mainActivity = mainActivity;
        parameters = mainActivity.getHelper().getParameters();
        this.bestScoreTwos = bestScoreTwos;
        this.bestTargetItems = bestTargetItems;

    }

    @Override
    protected Void doInBackground(Void... params) {
        if (isCancelled()) return null;
        for (int z = 0; z < parameters.getOccurencesNumber(); z++) {
            BestTargetItem bestTargetItem = bestTargetItems.get(z).get(bestTargetItems.get(z).size() - 1);

            if (bestScoreTwos.get(z) == null) {
                ArrayList toPut = new ArrayList();
                this.bestScoreTwos.put(z, toPut);
            }
            ScoreTwoItem scoreTwoItem = new ScoreTwoItem(bestTargetItem);
            this.bestScoreTwos.get(z).add(scoreTwoItem);

            HashMap<Integer, Tuple> functions = scoreTwoItem.getFunctions();
            Tuple<Integer, Integer> a = new Tuple(null, null);
            Tuple<Integer, Integer> b = new Tuple(null, null);
            Tuple<Integer, Integer> c = new Tuple(null, null);
            Tuple<Integer, Integer> d = new Tuple(null, null);
            Integer e = null;
            Tuple<Boolean, Tuple<Integer, Integer>> f = new Tuple(false, new Tuple(1, null));

            //A

            a.setArg1(1);
            a.setArg2(1);
            for (int i = bestScoreTwos.get(z).size() - 2; i >= 0; i--) {
                ScoreTwoItem indexedScoretwoItem = bestScoreTwos.get(z).get(i);
                if (indexedScoretwoItem.getBestTargetItem().getDenombrement() == scoreTwoItem.getBestTargetItem().getDenombrement()) {
                    if (indexedScoretwoItem.getBestTargetItem().isSuccess()) {
                        a.setArg1(1);
                    } else {
                        a.setArg1(((Integer) indexedScoretwoItem.getFunctions().get(ScoreTwoItem.A).getArg1()) + 1);
                    }
                    break;
                }
            }
            findDenombrement(a, ScoreTwoItem.A, z);
            functions.put(ScoreTwoItem.A, a);

            //B
            b.setArg1(1);
            b.setArg2(1);
            c.setArg1(1);
            c.setArg2(1);
            d.setArg1(1);
            d.setArg2(1);
            if (bestScoreTwos.get(z).size() > 1) {
                ScoreTwoItem lastScoreTwoItem = bestScoreTwos.get(z).get(bestScoreTwos.get(z).size() - 2);

                if (lastScoreTwoItem.getBestTargetItem().isSuccess()) {
                    b.setArg1(1);
                } else {
                    int denombrement = (int) lastScoreTwoItem.getFunctions().get(ScoreTwoItem.B).getArg1();
                    denombrement++;
                    b.setArg1(denombrement);
                }
                findDenombrement(b, ScoreTwoItem.B, z);

                //C
                for (int i = bestScoreTwos.get(z).size() - 2; i >= 0; i--) {
                    ScoreTwoItem indexedScoretwoItem = bestScoreTwos.get(z).get(i);
                    if ((int) indexedScoretwoItem.getFunctions().get(ScoreTwoItem.B).getArg1() == b.getArg1()) {
                        if (indexedScoretwoItem.getBestTargetItem().isSuccess()) {
                            c.setArg1(1);
                        } else {
                            c.setArg1(((Integer) indexedScoretwoItem.getFunctions().get(ScoreTwoItem.C).getArg1()) + 1);
                        }
                        break;
                    }
                }
                findDenombrement(c, ScoreTwoItem.C, z);

                //reindexing
                int valA = a.getArg1();
                int valB = b.getArg1();
                int valC = c.getArg1();
                int indA = a.getArg2();
                int indB = b.getArg2();
                int indC = c.getArg2();

                if ((indA == 1) && (indB == 1) && (indC == 1)) {
                    if ((valA == 1) && (valB == 1) && (valC == 1)) {
                    } else {
                        int max = Math.max(Math.max(valA, valB), valC);
                        if (valA != max) {
                            a.setArg2(0);
                        }
                        if (valB != max) {
                            b.setArg2(0);
                        }
                        if (valC != max) {
                            c.setArg2(0);
                        }
                    }
                } else {
                    if (indA == 1) {
                        a.setArg2(0);
                    }
                    if (indB == 1) {
                        b.setArg2(0);
                    }
                    if (indC == 1) {
                        c.setArg2(0);
                    }
                }

                //D
                valA = a.getArg1();
                valB = b.getArg1();
                valC = c.getArg1();
                indA = a.getArg2();
                indB = b.getArg2();
                indC = c.getArg2();

                d.setArg1(b.getArg1());
                if ((indA == 1) && (indB == 1) && (indC == 1)) {
                    if ((valA == 1) && (valB == 1) && (valC == 1)) {
                        d.setArg2(1);
                    } else d.setArg2(indA + indB + indC);
                } else d.setArg2(indA + indB + indC);
            }
            functions.put(ScoreTwoItem.B, b);
            functions.put(ScoreTwoItem.C, c);
            functions.put(ScoreTwoItem.D, d);

            //E
            Boolean inverse = null;
            HashMap<Integer, TraverseItem> traverse = parameters.getTraverse();

            for (int t : traverse.keySet()) {
                //autre
                TraverseItem traverseItem = traverse.get(t);

                if (traverseItem.getVal1() != null) {
                    if (traverseItem.getVal2() != null) {
                        if (traverseItem.getSign().equals(TraverseItem.INF)) {
                            if (traverseItem.getVal1().equals(d.getArg1()) && d.getArg2() < traverseItem.getVal2()) {
                                if (traverseItem.getChecked1()) inverse = false;
                                else inverse = true;
                                break;
                            }
                        } else if (traverseItem.getVal1().equals(d.getArg1()) && traverseItem.getSign().equals(TraverseItem.SUP)) {
                            if (d.getArg2() > traverseItem.getVal2()) {
                                if (traverseItem.getChecked1()) inverse = false;
                                else inverse = true;
                                break;
                            }
                        } else if (traverseItem.getVal1().equals(d.getArg1()) && traverseItem.getSign().equals(TraverseItem.EQ)) {
                            if (d.getArg2().equals(traverseItem.getVal2())) {
                                if (traverseItem.getChecked1()) inverse = false;
                                else inverse = true;
                                break;
                            }
                        }
                    } else {
                        if (traverseItem.getVal1().equals(d.getArg1())) {
                            if (traverseItem.getChecked1()) inverse = false;
                            else inverse = true;
                        }
                    }
                } else {
                    if (traverseItem.getChecked1()) inverse = false;
                    else inverse = true;
                }
            }

            if (inverse == null) e = scoreTwoItem.getBestTargetItem().getSimpleChance();
            else if (inverse) {
                e = parameters.getInverses().get(scoreTwoItem.getBestTargetItem().getSimpleChance());
            } else {
                e = scoreTwoItem.getBestTargetItem().getSimpleChance();
            }

            scoreTwoItem.setE(e);

            //F
            ScoreTwoItem lastScoreTwo = null;
            if ((bestScoreTwos.get(z).size() > 1) && (bestScoreTwos.get(z).size() - 1 > 0)) {
                lastScoreTwo = bestScoreTwos.get(z).get(bestScoreTwos.get(z).size() - 2);
                int match = scoreTwoItem.getBestTargetItem().getTarget().getMatch();
                int lastE = lastScoreTwo.getE();
                Tuple<Boolean, Tuple<Integer, Integer>> lastF = lastScoreTwo.getFunctions().get(ScoreTwoItem.F);
                boolean wasSuccess = false;
                if (match == 0) wasSuccess = false;
                else {
                    switch (lastE) {
                        case MainActivity.NR_SIMPLE_CHANCE:
                            if (!parameters.isRed(match)) wasSuccess = true;
                            break;
                        case MainActivity.RE_SIMPLE_CHANCE:
                            if (parameters.isRed(match)) wasSuccess = true;
                            break;
                        case MainActivity.PR_SIMPLE_CHANCE:
                            if (match % 2 == 0) wasSuccess = true;
                            break;
                        case MainActivity.IR_SIMPLE_CHANCE:
                            if (!(match % 2 == 0)) wasSuccess = true;
                            break;
                        case MainActivity.ME_SIMPLE_CHANCE:
                            if ((match > 0) && (match <= 18)) wasSuccess = true;
                            break;
                        case MainActivity.PE_SIMPLE_CHANCE:
                            if ((match > 18) && (match <= 36)) wasSuccess = true;
                            break;
                    }
                }

                if (wasSuccess) {
                    lastF.setArg1(true);
                    f.getArg2().setArg1(1);
                } else {
                    int denombrement = lastF.getArg2().getArg1();
                    denombrement++;
                    f.getArg2().setArg1(denombrement);
                }

                findFDenombrement(f.getArg2(), z);


            } else {
                f.getArg2().setArg1(1);
                f.getArg2().setArg2(1);
            }
            functions.put(ScoreTwoItem.F, f);

            mainActivity.registerDB(lastScoreTwo, DBWorkTask.UPDATE, 0);
        }
        return null;
    }

    private void findDenombrement(Tuple<Integer, Integer> f, int col, int z) {
        for (int i = bestScoreTwos.get(z).size() - 2; i >= 0; i--) {
            ScoreTwoItem indexedScoreOneItem = bestScoreTwos.get(z).get(i);

            if (indexedScoreOneItem.getFunctions().get(col).getArg2() != null) {
                if (f.getArg1().equals(indexedScoreOneItem.getFunctions().get(col).getArg1())) {
                    if (indexedScoreOneItem.getBestTargetItem().isSuccess()) {
                        f.setArg2(1);
                    } else {
                        f.setArg2((Integer) indexedScoreOneItem.getFunctions().get(col).getArg2() + 1);
                    }
                    return;
                }
            }
        }
        f.setArg2(1);
    }

    private void findFDenombrement(Tuple<Integer, Integer> f, int z) {
        int col = ScoreTwoItem.F;
        for (int i = bestScoreTwos.get(z).size() - 2; i >= 0; i--) {
            ScoreTwoItem indexedScoreOneItem = bestScoreTwos.get(z).get(i);

            if (indexedScoreOneItem.getFunctions().get(col).getArg2() != null) {
                if (f.getArg1().equals(((Tuple<Boolean, Tuple<Integer, Integer>>) indexedScoreOneItem.getFunctions().get(col)).getArg2().getArg1())) {
                    if ((boolean) indexedScoreOneItem.getFunctions().get(col).getArg1()) {
                        f.setArg2(1);
                    } else {
                        f.setArg2(((Tuple<Boolean, Tuple<Integer, Integer>>) indexedScoreOneItem.getFunctions().get(col)).getArg2().getArg2() + 1);
                    }
                    return;
                }
            }
        }
        f.setArg2(1);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        work.addScoreTwo(bestScoreTwos);
        cancel(true);
    }
}
