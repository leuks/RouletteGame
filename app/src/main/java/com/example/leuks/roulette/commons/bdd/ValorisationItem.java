package com.example.leuks.roulette.commons.bdd;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by leuks on 28/12/2016.
 */

@DatabaseTable(tableName = "ValorisationItem")
public class ValorisationItem {
    public final static boolean SCORE1 = false;
    public final static boolean SCORE2 = true;

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<Integer, BestTargetItem> bestTargetItem;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<Integer, String> finalExitTarget;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<Integer, ArrayList<String>> valuesScoreOne;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<Integer, ArrayList<String>> valuesScoreTwo;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private HashMap<Integer, Integer> valorisation;

    public ValorisationItem() {
        finalExitTarget = new HashMap<>();
        valuesScoreOne = new HashMap<>();
        valuesScoreTwo = new HashMap<>();
        bestTargetItem = new HashMap<>();
        valorisation = new HashMap<>();
    }

    public void addValue(boolean score, int occurence, String valueDenom, int value) {
        if (score == SCORE1) {
            if (valuesScoreOne.get(occurence) == null) {
                ArrayList<String> arrayToPut = new ArrayList<>();
                arrayToPut.add(valueDenom);
                valuesScoreOne.put(occurence, arrayToPut);
            } else {
                valuesScoreOne.get(occurence).add(valueDenom);
            }
        } else {
            if (valuesScoreTwo.get(occurence) == null) {
                ArrayList<String> arrayToPut = new ArrayList<>();
                arrayToPut.add(valueDenom);
                valuesScoreTwo.put(occurence, arrayToPut);
            } else {
                valuesScoreTwo.get(occurence).add(valueDenom);
            }
        }

        if (valorisation.get(occurence) == null) {
            valorisation.put(occurence, value);
        } else {
            valorisation.put(occurence, valorisation.get(occurence) + value);
        }
    }

    public String getStringValueScoreOne(int occurence) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < valuesScoreOne.get(occurence).size(); i++) {
            sb.append(valuesScoreOne.get(occurence).get(i));
            if (i < valuesScoreOne.get(occurence).size() - 1) sb.append(" / ");
        }
        return sb.toString();
    }

    public String getStringValueScoreTwo(int occurence) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < valuesScoreTwo.get(occurence).size(); i++) {
            sb.append(valuesScoreTwo.get(occurence).get(i));
            if (i < valuesScoreTwo.get(occurence).size() - 1) sb.append(" / ");
        }
        return sb.toString();
    }

    public void addBestTargetItem(int occurence, BestTargetItem bestTargetItem) {
        this.bestTargetItem.put(occurence, bestTargetItem);
    }

    public void addFinalTarget(int occurence, String finalTarget) {
        this.finalExitTarget.put(occurence, finalTarget);
    }

    //GS

    public HashMap<Integer, Integer> getValorisation() {
        return valorisation;
    }

    public void setValorisation(HashMap<Integer, Integer> valorisation) {
        this.valorisation = valorisation;
    }

    public HashMap<Integer, BestTargetItem> getBestTargetItem() {
        return bestTargetItem;
    }

    public HashMap<Integer, String> getFinalExitTarget() {
        return finalExitTarget;
    }

    public HashMap<Integer, ArrayList<String>> getValuesScoreOne() {
        return valuesScoreOne;
    }

    public void setValuesScoreOne(HashMap<Integer, ArrayList<String>> valuesScoreOne) {
        this.valuesScoreOne = valuesScoreOne;
    }

    public HashMap<Integer, ArrayList<String>> getValuesScoreTwo() {
        return valuesScoreTwo;
    }

    public void setValuesScoreTwo(HashMap<Integer, ArrayList<String>> valuesScoreTwo) {
        this.valuesScoreTwo = valuesScoreTwo;
    }
}
