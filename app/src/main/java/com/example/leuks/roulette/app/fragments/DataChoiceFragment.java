package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;

/**
 * Created by leuks on 29/12/2016.
 */

public class DataChoiceFragment extends Fragment {
    private MainActivity mainActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.fragment_data_choice, container, false);

        //components
        Button historyButton = (Button) thisView.findViewById(R.id.button_history);
        Button statisticsButton = (Button) thisView.findViewById(R.id.button_stats);

        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        historyButton.setTypeface(fontButton);
        statisticsButton.setTypeface(fontButton);


        //listeners
        historyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.show(MainActivity.ROAD_HISTORY_CHOICE, true);
            }
        });

        statisticsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.show(MainActivity.ROAD_STATISTICS, true);
            }
        });

        return thisView;
    }
}
