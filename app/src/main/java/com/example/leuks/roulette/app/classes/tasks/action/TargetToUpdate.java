package com.example.leuks.roulette.app.classes.tasks.action;

import com.example.leuks.roulette.commons.bdd.Target;

/**
 * Created by Leuks on 18/02/2017.
 */

public class TargetToUpdate {
    private Target target;
    private int indOccu;
    private int indTab;

    public TargetToUpdate(Target target, int indOccu, int indTab) {
        this.target = target;
        this.indOccu = indOccu;
        this.indTab = indTab;
    }

    //GS
    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public int getIndOccu() {
        return indOccu;
    }

    public void setIndOccu(int indOccu) {
        this.indOccu = indOccu;
    }

    public int getIndTab() {
        return indTab;
    }

    public void setIndTab(int indTab) {
        this.indTab = indTab;
    }
}
