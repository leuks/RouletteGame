package com.example.leuks.roulette.app.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.app.classes.WorkManager;

/**
 * Created by Leuks on 23/12/2016.
 */

public class EntryAutoFragment extends Fragment {
    private MainActivity mainActivity;
    private TextView entryCountTextView;
    private SeekBar entryCountSeekBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.fragment_entry_auto, container, false);

        //components
        final TextView entryCountShowTextView = (TextView) thisView.findViewById(R.id.textView_entry_count_show);
        entryCountTextView = (TextView) thisView.findViewById(R.id.textView_entry_count);
        entryCountSeekBar = (SeekBar) thisView.findViewById(R.id.seekBar_entry_count);
        Button validateEntryAutoButton = (Button) thisView.findViewById(R.id.button_validate_entry_auto);

        //font
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");
        entryCountShowTextView.setTypeface(fontButton);
        entryCountTextView.setTypeface(fontButton);

        //listeners
        entryCountSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                entryCountTextView.setText(String.valueOf(progress + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        validateEntryAutoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WorkManager workManager = new WorkManager(mainActivity, Integer.parseInt(entryCountTextView.getText().toString()) - 1, null);
                workManager.start();
            }
        });

        //data
        entryCountTextView.setText(String.valueOf(entryCountSeekBar.getProgress()));

        return thisView;
    }
}
