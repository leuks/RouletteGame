package com.example.leuks.roulette.app.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leuks.roulette.MainActivity;
import com.example.leuks.roulette.R;
import com.example.leuks.roulette.app.classes.TraverseItem;
import com.example.leuks.roulette.commons.bdd.Parameters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by leuks on 31/12/2016.
 */

public class ParametersDataTraverseFragment extends Fragment {
    private MainActivity mainActivity;
    private TableLayout dataTable;
    private HashMap<Integer, TraverseItem> denombrement;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = (MainActivity) getActivity();
        this.denombrement = new HashMap<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.fragment_parameters_traverse_data_layout, container, false);
        final Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");

        //childs
        dataTable = (TableLayout) thisView.findViewById(R.id.table_data);
        Button validateButton = (Button) thisView.findViewById(R.id.button_validate_data_parameters);
        Button addButton = (Button) thisView.findViewById(R.id.button_more);

        //listeners
        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Parameters parameters = mainActivity.getHelper().getParameters();
                final Parameters oldParameters = mainActivity.getHelper().getParameters();
                HashMap<Integer, TraverseItem> traverse = parameters.getTraverse();

                for (int i = 1; i < dataTable.getChildCount(); i++) {
                    TableRow tableRow = (TableRow) dataTable.getChildAt(i);

                    LinearLayout layoutD = (LinearLayout) tableRow.getChildAt(0);
                    LinearLayout layoutIC = (LinearLayout) tableRow.getChildAt(1);
                    LinearLayout layoutICn = (LinearLayout) tableRow.getChildAt(2);
                    String tw1StringValue = ((EditText) layoutD.getChildAt(0)).getText().toString().trim();
                    String tw2StringValue = ((EditText) layoutD.getChildAt(2)).getText().toString().trim();
                    String tw3StringValue = ((TextView) layoutD.getChildAt(1)).getText().toString().trim();
                    boolean isCB1Checked = ((CheckBox) layoutIC.getChildAt(1)).isChecked();
                    boolean isCB2Checked = ((CheckBox) layoutICn.getChildAt(1)).isChecked();


                    String sign = null;
                    if (tw3StringValue.contains("<")) sign = TraverseItem.INF;
                    else if (tw3StringValue.contains(">")) sign = TraverseItem.SUP;
                    else if (tw3StringValue.contains("=")) sign = TraverseItem.EQ;
                    else sign = TraverseItem.NONE;

                    TraverseItem traverseItem;
                    TraverseItem dnbTraverseItem;
                    if (sign.equals(TraverseItem.NONE)) {
                        if (!tw1StringValue.isEmpty()) {
                            int tw1IntValue = Integer.parseInt(tw1StringValue);
                            traverseItem = traverse.get(parameters.getKeyFromValue(tw1IntValue, null, sign));
                            dnbTraverseItem = new TraverseItem(tw1IntValue, null, isCB1Checked, isCB2Checked, sign);
                        } else {
                            dnbTraverseItem = new TraverseItem(null, null, isCB1Checked, isCB2Checked, sign);
                            traverseItem = traverse.get(parameters.getKeyFromValue(null, null, sign));
                        }
                    } else {
                        if ((Pattern.compile("\\d+").matcher(tw1StringValue).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(tw1StringValue).find())) {
                            if ((Pattern.compile("\\d+").matcher(tw2StringValue).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(tw2StringValue).find())) {
                                int tw1IntValue = Integer.parseInt(tw1StringValue);
                                int tw2IntValue = Integer.parseInt(tw2StringValue);
                                dnbTraverseItem = new TraverseItem(tw1IntValue, tw2IntValue, isCB1Checked, isCB2Checked, sign);
                                traverseItem = traverse.get(parameters.getKeyFromValue(tw1IntValue, tw2IntValue, sign));
                            } else {
                                Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        } else {
                            Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    //modify
                    if (parameters.existsTraverseItemValue(traverse, dnbTraverseItem)) {
                        if (!(dnbTraverseItem.getChecked1().equals(traverseItem.getChecked1()))) {
                            traverseItem.setChecked1(dnbTraverseItem.getChecked1());
                            traverseItem.setChecked2(dnbTraverseItem.getChecked2());
                        }
                    }
                    //add
                    else {
                        traverse.put(parameters.getMaxKeyOfTraverse(), dnbTraverseItem);
                    }
                }

                //remove
                ArrayList<Integer> toRemove = new ArrayList<Integer>();
                for (int key : traverse.keySet()) {
                    TraverseItem traverseItem = traverse.get(key);
                    //remove
                    if (!(parameters.existsTraverseItemValue(denombrement, traverseItem))) {
                        toRemove.add(key);
                    }
                }
                for (int key : toRemove) {
                    traverse.remove(key);
                }

                mainActivity.getHelper().updateParameters(parameters);

                Snackbar snackbar = Snackbar
                        .make(mainActivity.findViewById(android.R.id.content), "Paramètres mis à jour!", Snackbar.LENGTH_LONG)
                        .setAction("ANNULER", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mainActivity.getHelper().updateParameters(oldParameters);
                                refreshView();
                                Snackbar snackbar1 = Snackbar.make(mainActivity.findViewById(android.R.id.content), "Paramètres rétablis!", Snackbar.LENGTH_SHORT);
                                snackbar1.show();
                            }
                        });
                snackbar.show();

            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(mainActivity);
                dialog.setTitle("Ajouter une ligne");
                dialog.setContentView(R.layout.dialog_new_traverse_layout);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;

                //components
                final EditText firstED = (EditText) dialog.findViewById(R.id.editText_one);
                final EditText secondED = (EditText) dialog.findViewById(R.id.editText_two);
                final Spinner choiceSpinner = (Spinner) dialog.findViewById(R.id.spinner_choice);
                final CheckBox firstCB = (CheckBox) dialog.findViewById(R.id.checkBox_one);
                final CheckBox secondCB = (CheckBox) dialog.findViewById(R.id.checkBox_two);
                Button validateButton3 = (Button) dialog.findViewById(R.id.button_validate);
                TextView twDValue = (TextView) dialog.findViewById(R.id.textView_d_value_show);
                TextView twCol = (TextView) dialog.findViewById(R.id.textView_col_value_show);

                //font
                firstED.setGravity(Gravity.CENTER);
                secondED.setGravity(Gravity.CENTER);
                firstED.setInputType(InputType.TYPE_CLASS_PHONE);
                secondED.setInputType(InputType.TYPE_CLASS_PHONE);
                firstED.setTypeface(fontButton);
                secondED.setTypeface(fontButton);
                twDValue.setTypeface(fontButton);
                twCol.setTypeface(fontButton);

                //data
                firstCB.setChecked(true);
                final ArrayList<String> options = new ArrayList<>();
                options.add("=");
                options.add("<");
                options.add(">");
                options.add("");
                options.add("Autre");
                ArrayAdapter<String> adapter = new ArrayAdapter<>(mainActivity, android.R.layout.simple_spinner_item, options);
                choiceSpinner.setAdapter(adapter);

                validateButton3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Parameters parameters = mainActivity.getHelper().getParameters();
                        String textFirstEd = firstED.getText().toString().trim();
                        String textSecondEd = secondED.getText().toString().trim();
                        if ((choiceSpinner.getSelectedItem()).equals("=")) {
                            if ((Pattern.compile("\\d+").matcher(textFirstEd).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textFirstEd).find())) {
                                if ((Pattern.compile("\\d+").matcher(textSecondEd).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textSecondEd).find())) {
                                    TraverseItem traverseItem = new TraverseItem(Integer.parseInt(textFirstEd), Integer.parseInt(textSecondEd), firstCB.isChecked(), secondCB.isChecked(), TraverseItem.EQ);
                                    if (!parameters.existsTraverseItemValue(denombrement, traverseItem))
                                        denombrement.put(getMaxKeyOfDenombrement(), traverseItem);
                                    else {
                                        Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                } else {
                                    Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            } else {
                                Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        } else if ((choiceSpinner.getSelectedItem()).equals("<")) {
                            if ((Pattern.compile("\\d+").matcher(textFirstEd).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textFirstEd).find())) {
                                if ((Pattern.compile("\\d+").matcher(textSecondEd).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textSecondEd).find())) {
                                    TraverseItem traverseItem = new TraverseItem(Integer.parseInt(textFirstEd), Integer.parseInt(textSecondEd), firstCB.isChecked(), secondCB.isChecked(), TraverseItem.INF);
                                    if (!parameters.existsTraverseItemValue(denombrement, traverseItem))
                                        denombrement.put(getMaxKeyOfDenombrement(), traverseItem);
                                    else {
                                        Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                } else {
                                    Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            } else {
                                Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        } else if ((choiceSpinner.getSelectedItem()).equals(">")) {
                            if ((Pattern.compile("\\d+").matcher(textFirstEd).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textFirstEd).find())) {
                                if ((Pattern.compile("\\d+").matcher(textSecondEd).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textSecondEd).find())) {
                                    TraverseItem traverseItem = new TraverseItem(Integer.parseInt(textFirstEd), Integer.parseInt(textSecondEd), firstCB.isChecked(), secondCB.isChecked(), TraverseItem.SUP);
                                    if (!parameters.existsTraverseItemValue(denombrement, traverseItem))
                                        denombrement.put(getMaxKeyOfDenombrement(), traverseItem);
                                    else {
                                        Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                } else {
                                    Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            } else {
                                Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        } else {
                            TraverseItem traverseItem;
                            if (!textFirstEd.isEmpty()) {
                                traverseItem = new TraverseItem(Integer.parseInt(textFirstEd), null, firstCB.isChecked(), secondCB.isChecked(), TraverseItem.NONE);
                                if (!parameters.existsTraverseItemValue(denombrement, traverseItem))
                                    denombrement.put(getMaxKeyOfDenombrement(), traverseItem);
                                else {
                                    Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            } else {
                                traverseItem = new TraverseItem(null, null, firstCB.isChecked(), secondCB.isChecked(), TraverseItem.NONE);
                                if (!parameters.existsTraverseItemValue(denombrement, traverseItem))
                                    denombrement.put(getMaxKeyOfDenombrement(), traverseItem);
                                else {
                                    Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            }
                        }
                        dialog.dismiss();

                        refreshViewFromDenombrement();
                    }
                });
                dialog.getWindow().setAttributes(lp);
                dialog.show();

                //listeners
                firstCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (!b) secondCB.setChecked(true);
                        else secondCB.setChecked(false);
                    }
                });

                secondCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (!b) firstCB.setChecked(true);
                        else firstCB.setChecked(false);
                    }
                });

                choiceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (options.get(position).isEmpty()) {
                            secondED.setEnabled(false);
                            secondED.setText("");
                        } else if (options.get(position).equals("Autre")) {
                            firstED.setEnabled(false);
                            firstED.setText("");
                            secondED.setText("");
                            secondED.setEnabled(false);
                        } else {
                            firstED.setEnabled(true);
                            secondED.setEnabled(true);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });

        return thisView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshView();
    }

    private void refreshView() {
        HashMap<Integer, TraverseItem> map = mainActivity.getHelper().getParameters().getTraverse();
        denombrement.putAll(map);
        refreshViewFromDenombrement();
    }

    private void refreshViewFromDenombrement() {
        Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");

        dataTable.removeAllViews();

        //header
        TableRow header = new TableRow(mainActivity);
        header.setGravity(Gravity.CENTER);
        header.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

        TextView headerTwD = new TextView(mainActivity);
        headerTwD.setGravity(Gravity.CENTER);
        headerTwD.setPadding(10, 10, 10, 10);
        headerTwD.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        headerTwD.setText("Valeur D");
        headerTwD.setTypeface(fontButton);

        TextView headerTwITarget = new TextView(mainActivity);
        headerTwITarget.setPadding(10, 10, 10, 10);
        headerTwITarget.setGravity(Gravity.CENTER);
        headerTwITarget.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        headerTwITarget.setText("Cible initiale");
        headerTwITarget.setTypeface(fontButton);

        TextView headerTwIChance = new TextView(mainActivity);
        headerTwIChance.setGravity(Gravity.CENTER);
        headerTwIChance.setPadding(10, 10, 10, 10);
        headerTwIChance.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        headerTwIChance.setText("Chance Inverse");
        headerTwIChance.setTypeface(fontButton);

        header.addView(headerTwD);
        header.addView(headerTwITarget);
        header.addView(headerTwIChance);

        dataTable.addView(header);

        for (int key : denombrement.keySet()) {
            final int finalI = key;
            TraverseItem traverseItem = denombrement.get(key);
            TableRow row = new TableRow(mainActivity);
            row.setGravity(Gravity.CENTER);
            row.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));

            LinearLayout layoutD = new LinearLayout(mainActivity);
            final EditText firstED = new EditText(mainActivity);
            firstED.setInputType(InputType.TYPE_CLASS_PHONE);
            final TextView firstTW = new TextView(mainActivity);
            final EditText secondED = new EditText(mainActivity);
            layoutD.setGravity(Gravity.CENTER);
            layoutD.setOrientation(LinearLayout.HORIZONTAL);
            layoutD.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));
            firstED.setGravity(Gravity.CENTER);
            firstED.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            firstTW.setGravity(Gravity.CENTER);
            firstTW.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            secondED.setGravity(Gravity.CENTER);
            secondED.setInputType(InputType.TYPE_CLASS_PHONE);
            secondED.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            firstTW.setTypeface(fontButton);
            secondED.setTypeface(fontButton);
            firstED.setTypeface(fontButton);
            firstED.setEnabled(false);
            secondED.setEnabled(false);

            Space space1 = new Space(mainActivity);
            space1.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            Space space2 = new Space(mainActivity);
            space2.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            Space space3 = new Space(mainActivity);
            space3.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            Space space4 = new Space(mainActivity);
            space4.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));


            LinearLayout layoutCI = new LinearLayout(mainActivity);
            layoutCI.setGravity(Gravity.CENTER);
            layoutCI.setOrientation(LinearLayout.HORIZONTAL);
            layoutCI.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));
            final CheckBox firstCheckBox = new CheckBox(mainActivity);
            firstCheckBox.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            layoutCI.addView(space1);
            layoutCI.addView(firstCheckBox);
            layoutCI.addView(space2);
            if (traverseItem.getChecked1()) firstCheckBox.setChecked(true);

            LinearLayout layoutCIn = new LinearLayout(mainActivity);
            layoutCIn.setGravity(Gravity.CENTER);
            layoutCIn.setOrientation(LinearLayout.HORIZONTAL);
            layoutCIn.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3f));
            final CheckBox secondCheckBox = new CheckBox(mainActivity);
            secondCheckBox.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
            layoutCIn.addView(space3);
            layoutCIn.addView(secondCheckBox);
            layoutCIn.addView(space4);
            if (traverseItem.getChecked2()) secondCheckBox.setChecked(true);

            firstCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (!b) secondCheckBox.setChecked(true);
                    else secondCheckBox.setChecked(false);
                }
            });

            secondCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (!b) firstCheckBox.setChecked(true);
                    else firstCheckBox.setChecked(false);
                }
            });

            firstED.setInputType(InputType.TYPE_CLASS_PHONE);
            secondED.setInputType(InputType.TYPE_CLASS_PHONE);


            if (traverseItem.getSign().equals(TraverseItem.EQ)) {
                firstTW.setText(",n : n" + TraverseItem.EQ);
                firstED.setText(String.valueOf(traverseItem.getVal1()));
                secondED.setText(String.valueOf(traverseItem.getVal2()));
            } else if (traverseItem.getSign().equals(TraverseItem.INF)) {
                firstTW.setText(",n : n" + TraverseItem.INF);
                firstED.setText(String.valueOf(traverseItem.getVal1()));
                secondED.setText(String.valueOf(traverseItem.getVal2()));
            } else if (traverseItem.getSign().equals(TraverseItem.SUP)) {
                firstTW.setText(",n : n" + TraverseItem.SUP);
                firstED.setText(String.valueOf(traverseItem.getVal1()));
                secondED.setText(String.valueOf(traverseItem.getVal2()));
            } else if (traverseItem.getSign().equals(TraverseItem.NONE)) {
                if (traverseItem.getVal1() == null && traverseItem.getVal2() == null) {
                    firstTW.setText("Autre");
                    firstED.setEnabled(false);
                } else {
                    firstTW.setText(",n");
                    firstED.setText(String.valueOf(traverseItem.getVal1()));
                }
                secondED.setEnabled(false);
            }

            layoutD.addView(firstED);
            layoutD.addView(firstTW);
            layoutD.addView(secondED);

            row.setOnClickListener(new View.OnClickListener() {
                final Typeface fontButton = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/Roboto-Light.ttf");

                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                    builder.setTitle("Action : " + firstED.getText().toString() + "" + firstTW.getText().toString() + "" + secondED.getText().toString());
                    builder.setPositiveButton("Modifier", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i1) {
                            final Dialog dialog = new Dialog(mainActivity);
                            dialog.setTitle("Modification : " + firstED.getText().toString() + "" + firstTW.getText().toString() + "" + secondED.getText().toString());
                            dialog.setContentView(R.layout.dialog_new_traverse_layout);

                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            lp.copyFrom(dialog.getWindow().getAttributes());
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.MATCH_PARENT;

                            //components
                            final EditText firstED2 = (EditText) dialog.findViewById(R.id.editText_one);
                            final EditText secondED2 = (EditText) dialog.findViewById(R.id.editText_two);
                            final Spinner choiceSpinner2 = (Spinner) dialog.findViewById(R.id.spinner_choice);
                            final CheckBox firstCB2 = (CheckBox) dialog.findViewById(R.id.checkBox_one);
                            final CheckBox secondCB2 = (CheckBox) dialog.findViewById(R.id.checkBox_two);
                            Button validateButton2 = (Button) dialog.findViewById(R.id.button_validate);
                            TextView twDValue2 = (TextView) dialog.findViewById(R.id.textView_d_value_show);
                            TextView twCol2 = (TextView) dialog.findViewById(R.id.textView_col_value_show);
                            firstED.setInputType(InputType.TYPE_CLASS_PHONE);
                            firstED2.setInputType(InputType.TYPE_CLASS_PHONE);

                            //font
                            firstED2.setGravity(Gravity.CENTER);
                            secondED2.setGravity(Gravity.CENTER);
                            firstED2.setTypeface(fontButton);
                            secondED2.setTypeface(fontButton);
                            twDValue2.setTypeface(fontButton);
                            twCol2.setTypeface(fontButton);

                            //data
                            firstCB2.setChecked(true);
                            final ArrayList<String> options = new ArrayList<>();
                            options.add("=");
                            options.add("<");
                            options.add(">");
                            options.add("");
                            options.add("Autre");
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(mainActivity, android.R.layout.simple_spinner_item, options);
                            choiceSpinner2.setAdapter(adapter);

                            firstED2.setText(firstED.getText());
                            secondED2.setText(secondED.getText());
                            firstCB2.setChecked(firstCheckBox.isChecked());
                            secondCB2.setChecked(secondCheckBox.isChecked());
                            if (firstTW.getText().toString().contains("=")) {
                                choiceSpinner2.setSelection(0);
                            } else if (firstTW.getText().toString().contains(">")) {
                                choiceSpinner2.setSelection(2);
                            } else if (firstTW.getText().toString().contains("<")) {
                                choiceSpinner2.setSelection(1);
                            } else {
                                if (!firstED.getText().toString().isEmpty()) {
                                    choiceSpinner2.setSelection(3);
                                } else {
                                    choiceSpinner2.setSelection(4);
                                }
                            }

                            choiceSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    if (options.get(position).isEmpty()) {
                                        secondED2.setEnabled(false);
                                        secondED2.setText("");
                                    } else if (options.get(position).equals("Autre")) {
                                        firstED2.setEnabled(false);
                                        firstED2.setText("");
                                        secondED2.setText("");
                                        secondED2.setEnabled(false);
                                    } else {
                                        firstED2.setEnabled(true);
                                        secondED2.setEnabled(true);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                            validateButton2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    denombrement.remove((finalI));
                                    Parameters parameters = mainActivity.getHelper().getParameters();
                                    String textFirstEd = firstED2.getText().toString().trim();
                                    String textSecondEd = secondED2.getText().toString().trim();
                                    if ((choiceSpinner2.getSelectedItem()).equals("=")) {
                                        if ((Pattern.compile("\\d+").matcher(textFirstEd).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textFirstEd).find())) {
                                            if ((Pattern.compile("\\d+").matcher(textSecondEd).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textSecondEd).find())) {
                                                TraverseItem traverseItem = new TraverseItem(Integer.parseInt(textFirstEd), Integer.parseInt(textSecondEd), firstCB2.isChecked(), secondCB2.isChecked(), TraverseItem.EQ);
                                                if (!parameters.existsTraverseItemValue(denombrement, traverseItem))
                                                    denombrement.put(getMaxKeyOfDenombrement(), traverseItem);
                                                else {
                                                    Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                                    return;

                                                }
                                            } else {
                                                Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                        } else {
                                            Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    } else if ((choiceSpinner2.getSelectedItem()).equals("<")) {
                                        if ((Pattern.compile("\\d+").matcher(textFirstEd).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textFirstEd).find())) {
                                            if ((Pattern.compile("\\d+").matcher(textSecondEd).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textSecondEd).find())) {
                                                TraverseItem traverseItem = new TraverseItem(Integer.parseInt(textFirstEd), Integer.parseInt(textSecondEd), firstCB2.isChecked(), secondCB2.isChecked(), TraverseItem.INF);
                                                if (!parameters.existsTraverseItemValue(denombrement, traverseItem))
                                                    denombrement.put(getMaxKeyOfDenombrement(), traverseItem);
                                                else {
                                                    Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                                    return;
                                                }
                                            } else {
                                                Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                        } else {
                                            Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    } else if ((choiceSpinner2.getSelectedItem()).equals(">")) {
                                        if ((Pattern.compile("\\d+").matcher(textFirstEd).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textFirstEd).find())) {
                                            if ((Pattern.compile("\\d+").matcher(textSecondEd).find() && !Pattern.compile("^[0-9]+[.][0-9]+?$").matcher(textSecondEd).find())) {
                                                TraverseItem traverseItem = new TraverseItem(Integer.parseInt(textFirstEd), Integer.parseInt(textSecondEd), firstCB2.isChecked(), secondCB2.isChecked(), TraverseItem.SUP);
                                                if (!parameters.existsTraverseItemValue(denombrement, traverseItem))
                                                    denombrement.put(getMaxKeyOfDenombrement(), traverseItem);
                                                else {
                                                    Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                                    return;
                                                }
                                            } else {
                                                Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                        } else {
                                            Toast.makeText(mainActivity, "Entier uniquement", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    } else {
                                        TraverseItem traverseItem;
                                        if (!textFirstEd.isEmpty()) {
                                            secondED2.setEnabled(false);
                                            traverseItem = new TraverseItem(Integer.parseInt(textFirstEd), null, firstCB2.isChecked(), secondCB2.isChecked(), TraverseItem.NONE);
                                            if (!parameters.existsTraverseItemValue(denombrement, traverseItem))
                                                denombrement.put(getMaxKeyOfDenombrement(), traverseItem);
                                            else {
                                                Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                        } else {
                                            firstED2.setEnabled(false);
                                            secondED2.setEnabled(false);
                                            traverseItem = new TraverseItem(null, null, firstCB2.isChecked(), secondCB2.isChecked(), TraverseItem.NONE);
                                            if (!parameters.existsTraverseItemValue(denombrement, traverseItem))
                                                denombrement.put(getMaxKeyOfDenombrement(), traverseItem);
                                            else {
                                                Toast.makeText(mainActivity, "Valeur existante", Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                        }
                                    }
                                    dialog.dismiss();
                                    refreshViewFromDenombrement();
                                }
                            });

                            dialog.getWindow().setAttributes(lp);
                            dialog.show();
                        }
                    });
                    builder.setNegativeButton("Supprimer", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            denombrement.remove(finalI);
                            refreshViewFromDenombrement();
                        }
                    });
                    builder.show();
                }
            });

            row.addView(layoutD);
            row.addView(layoutCI);
            row.addView(layoutCIn);
            dataTable.addView(row);
        }
    }

    public int getMaxKeyOfDenombrement() {
        int max = 0;
        for (int i : denombrement.keySet()) {
            TraverseItem traverseItem = denombrement.get(i);
            if (i > max) max = i;
        }
        return max + 1;
    }
}
